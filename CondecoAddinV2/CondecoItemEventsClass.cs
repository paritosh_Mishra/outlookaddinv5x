using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;

using System.Data;

using Exception = System.Exception;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.Constants;
using System.Web;
namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Item Events Class
    /// </summary>
    public class CondecoItemEventsClass : AddinExpress.MSO.ADXOutlookItemEvents
    {
        private AddinModule CurrentModule = null;
        private bool isSelectedChanged = false;
        private static BookingHelper bookingHelper = new BookingHelper();
        private static AppointmentHelper appHelper = new AppointmentHelper();
        private static SyncManager syncManager = new SyncManager();
        private DateTime origStart;
        private bool isAppointmentMoved;
        private DateTime origSeriesStart;
        private DateTime origSeriesEnd;
        private bool IsDatePastMsgShow = false;
        //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
        private bool isOLK2007RecCancelExp = false;

        //PRB0041438-Added by Ravi Goyal to capture Yes, No cancel for user option
        private bool IsOccurenceChangesDiscard = false;

        //Anand : Added this property to identify that process open completed
        public bool IsEnableWriteEventCheck { get; set; }

        //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
        public bool IsOLK2007RecCancelExp
        {
            get { return isOLK2007RecCancelExp; }
            set { isOLK2007RecCancelExp = value; }
        }
        // end 
        public DateTime OrigStart
        {
            get { return origStart; }
            set { origStart = value; }
        }
        private DateTime origEnd;

        public DateTime OrigEnd
        {
            get { return origEnd; }
            set { origEnd = value; }
        }
        private DateTime deletionDate;

        public DateTime DeletionDate
        {
            get { return deletionDate; }
            set { deletionDate = value; }
        }
        private string origSubject;

        public string OrigSubject
        {
            get { return origSubject; }
            set { origSubject = value; }
        }
        private bool isNormalBooking;
        public bool IsNormalBooking
        {
            get { return isNormalBooking; }
            set { isNormalBooking = value; }
        }
        public bool IsAppointmentMoved
        {
            get { return isAppointmentMoved; }
            set { isAppointmentMoved = value; }
        }

        private bool isSubjectChanged;
        private bool isLocationChanged;
        private bool isStartDateChanged;
        private bool isEndDateChanged;
        private bool isAlertDisplayed;
        private bool isCondecoBookingSaved;
        private bool isSeriesDeleted;
        private bool isRecurrenceRemoved;
        public bool IsSubjectChanged
        {
            get { return isSubjectChanged; }
            set { isSubjectChanged = value; }
        }
        public bool IsLocationChanged
        {
            get { return isLocationChanged; }
            set { isLocationChanged = value; }
        }
        public bool IsStartDateChanged
        {
            get { return isStartDateChanged; }

            set { isStartDateChanged = value; }
        }
        public bool IsEndDateChanged
        {
            get { return isEndDateChanged; }
            set { isEndDateChanged = value; }
        }
        public bool IsAlertDisplayed
        {
            get { return isAlertDisplayed; }
            set { isAlertDisplayed = value; }
        }
        public bool IsCondecoBookingSaved
        {
            get { return isCondecoBookingSaved; }
            set { isCondecoBookingSaved = value; }
        }
        public bool IsSeriesDeleted
        {
            get { return isSeriesDeleted; }
            set { isSeriesDeleted = value; }
        }
        public bool IsRecurrenceRemoved
        {
            get { return isRecurrenceRemoved; }
            set { isRecurrenceRemoved = value; }
        }
        public CondecoItemEventsClass(AddinExpress.MSO.ADXAddinModule module, bool isSelectedChanged)
            : base(module)
        {
            this.isSelectedChanged = isSelectedChanged;
            if (CurrentModule == null)
                CurrentModule = module as AddinModule;
        }



        public override void ProcessAttachmentAdd(object attachment)
        {
            // TODO: Add some code
        }

        public override void ProcessAttachmentRead(object attachment)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentSave(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeCheckNames(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessClose(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:*********Started********");
            try
            {
                Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
                if (currApp == null)
                {
                    e.Cancel = false;
                    if (!isSelectedChanged)
                    {
                        this.Dispose();
                    }
                    return;
                }
                if (!UtilityManager.QuickResponse())
                {
                    UtilityManager.LogMessage("CondecoItemEventsClass: ProcessClose():-Timeout detected-Condeco host is not responding, booking will not be sync with Condeco----");
                    UtilityManager.LogMessage("CondecoItemEventsClass: ProcessClose():-Appointment closed without sync with Condeco----");
                    return;
                }
                
                    try
                    {
                        UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:CurrApp Location is fixed in catch() using active inspector as it was released during open event");
                        Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                        if (ins != null)
                        {
                            object item = ins.CurrentItem;
                            if (item != null)
                            {
                                if (item is Outlook.AppointmentItem)
                                {
                                    currApp = item as Outlook.AppointmentItem;
                                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                                            if ((Module as AddinModule).exclusionoccurencesinfo != null)
                                                if (currApp.Start != (Module as AddinModule).exclusionoccurencesinfo.Item1)
                                                    IsStartDateChanged = true;
                                        if (currApp.End != (Module as AddinModule).exclusionoccurencesinfo.Item2)
                                            IsEndDateChanged = true;
                                        if (currApp.Subject != (Module as AddinModule).exclusionoccurencesinfo.Item3)
                                            IsSubjectChanged = true;
                                }
                            }
                        }
                    }
                    catch { }
                
                if (currApp == null)
                {
                    e.Cancel = false;
                    if (!isSelectedChanged)
                    {
                        CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                    }
                    return;
                }
                try
                {
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                    {
                        if (!isSelectedChanged)
                        {
                            CurrentModule.itemEvents.Remove(this);
                            this.Dispose();
                        }
                        return;
                    }

                }
                catch
                {
                    e.Cancel = false;
                    return;
                }
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:Check Booking is In Sync With Condeco");
                // TODO: Add some code
                if (!CheckBookingIsInSyncWithCondeco())
                {
                    IsAlertDisplayed = false;
                    e.Cancel = true;
                    //if (!isSelectedChanged)
                    //{
                    //    CurrentModule.itemEvents.Remove(this);
                    //    this.Dispose();
                    //}
                    return;
                }
               

                //Added by Anand on 27-Feb-2013 for TP 11197
                if (currApp.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
                {
                    bool isCondecoBooking = bookingHelper.IsCondecoBooking(currApp);

                    // Below origEnd check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                    // if (isCondecoBooking && origStart < DateTime.Now && !currApp.Location.ToLower().Contains("cancelled"))
                    if (isCondecoBooking && origEnd < DateTime.Now && !currApp.Location.ToLower().Contains("cancelled"))
                    {
                        //proceed without event removal
                        e.Cancel = false;
                        return;
                    }
                }

                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:Series booking so checking for individually edited item sync");
                    //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
                    if (!IsOLK2007RecCancelExp)
                    {
                        syncManager.SyncIndiviudallyEditedItems(currApp,CurrentModule,false,false);
                        IsOLK2007RecCancelExp = false;
                    }

                }
                if (!isSelectedChanged)
                {
                    CurrentModule.itemEvents.Remove(this);
                    this.Dispose();
                }
            }
            catch
            { }
            finally
            {

            }
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessClose:*********Finished********");
        }

        public override void ProcessCustomAction(object action, object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessCustomPropertyChange(string name)
        {
            // TODO: Add some code
        }

        public override void ProcessForward(object forward, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessOpen(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:*********Started********");
            try
            {
                Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
                if (currApp == null) return;
                if (!UtilityManager.QuickResponse())
                {
                    UtilityManager.LogMessage("CondecoItemEventsClass: ProcessOpen():-Timeout detected-Condeco host is not responding, booking will not be sync with Condeco----");
                    UtilityManager.LogMessage("CondecoItemEventsClass: ProcessOpen():-Appointment open without sync with Condeco----");
                    return;
                }
                IsEnableWriteEventCheck = false;
                bool isTimechanged = (Module as AddinModule).isNewTimeProposed;
                //By Anand on 8-Feb-2013 for TP issue 10614, 10611
                string postId = appHelper.GetAppointmentPostID(currApp);
                bool havingPermission = true;

                if (!string.IsNullOrEmpty(postId))
                {
                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                    {
                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                        {

                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                        }
                        //UtilityManager.IsSSOTimeOutMsgShow = false;
                        return;
                    }
                    havingPermission = bookingHelper.PermissionCheck(postId, UtilityManager.GetCurrentUserForPost());
                }
                UtilityManager.LogMessage("processopen:permission returned is: " + havingPermission);

                int bookingId = bookingHelper.GetBookingID(postId, currApp);

                AppointmentDataInfo.AddAppointmentInCollection(currApp, havingPermission, bookingId);
                //end
                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled ||
                    currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived)
                {

                    return;
                }
                if (currApp.IsRecurring)
                {
                    IsNormalBooking = false;
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                    {
                        AppointmentDataInfo.SetIsItemNonMeeting(currApp, true);
                    }
                }
                else
                {
                    IsNormalBooking = true;
                }
                ////Removing setting condeco reference from here and moving to click of room booking
                //bookingHelper.AddCondecoReference(currApp);
                if(UtilityManager.IsSharedCalendarItem(currApp) && currApp.IsRecurring)
                {
                    AppointmentDataInfo.SetAppItemCondecoBookingOpen(currApp, true);
                }
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:Going to Sync the appointment");
                if (!isTimechanged)
                {
                    syncManager.SyncAppointmentWithCondeco(currApp);
                }
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:Sync completed");
                OrigStart = currApp.Start;
                OrigEnd = currApp.End;
                //OrigSubject = HttpUtility.UrlEncode(currApp.Subject);
                //Added by Ravi Goyal to fix the Issue-PRB0041438 of Space was coverting to + sing in (5.5.4 QA testing reported bug)
                origSubject = UtilityManager.CheckHtmlDecode(currApp.Subject);
                //added by anand for TP 9497 issue below variables are newly added variables
                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                {
                    Outlook.RecurrencePattern pattern = currApp.GetRecurrencePattern();
                    origSeriesStart = new DateTime(pattern.PatternStartDate.Date.Ticks + pattern.StartTime.TimeOfDay.Ticks);
                    origSeriesEnd = new DateTime(pattern.PatternEndDate.Date.Ticks + pattern.EndTime.TimeOfDay.Ticks);
                    UtilityManager.FreeCOMObject(pattern);
                }
                // Added by Ravi Goyal - if item is olApptException, release the com object.
                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                {
                    try
                    {
                        UtilityManager.LogMessage("processopen:Item RecurrenceState is olApptException, releasing com the object");
                        // Added by Ravi Goyal - if item is olApptException, save its start, end and Subject info to tuple list in addin module.
                        (Module as AddinModule).exclusionoccurencesinfo = new Tuple<DateTime, DateTime, string>(currApp.Start, currApp.End, currApp.Subject);
                        UtilityManager.FreeCOMObject(currApp);
                        UtilityManager.LogMessage("processopen:com object released successfully");
                    }
                    catch
                    {

                    }
                }
                IsStartDateChanged = false;
                IsEndDateChanged = false;
                IsSubjectChanged = false;
                IsAlertDisplayed = false;
                IsRecurrenceRemoved = false;
                IsEnableWriteEventCheck = true;
                //changed by Anand on 13-Feb-2012. Moved here as need to set the properties as well
                if (isTimechanged)
                {
                    IsCondecoBookingSaved = false;
                    IsStartDateChanged = true;
                    IsEndDateChanged = true;
                    (Module as AddinModule).isNewTimeProposed = false;
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in Process Open" + ex.Message + " " + ex.StackTrace);
            }
            finally 
            { 
                (Module as AddinModule).IsSendPressed = false;
            }
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessOpen:*********Finished********");
        }

        public override void ProcessPropertyChange(string name)
        {
            UtilityManager.LogMessage("CondecoItemEventsClass: *******Started**************** For Name : " + name);
            // if (this.ItemObj is Outlook.AppointmentItem)
            // {
            try
            {
                if (name.Equals("Subject"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsSubjectChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetSubjectChanged(cItem, true);
                    appHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("Start"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsStartDateChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetStartDateChanged(cItem, true);
                    appHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("End"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    IsEndDateChanged = true;
                    IsCondecoBookingSaved = false;
                    AppointmentDataInfo.SetEndDateChanged(cItem, true);
                    appHelper.UpdateBookingClickedSettings(cItem, false);
                }
                else if (name.Equals("IsRecurring"))
                {
                    Outlook.AppointmentItem cItem = this.ItemObj as Outlook.AppointmentItem;
                    if (!isNormalBooking && !cItem.IsRecurring)
                    {
                        IsRecurrenceRemoved = true;
                    }
                    IsCondecoBookingSaved = false;
                    appHelper.UpdateBookingClickedSettings(cItem, false);
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in ProcessPropertyChange for prop:" + name + " " + ex.Message + " " + ex.StackTrace);
            }
            //}
            UtilityManager.LogMessage("CondecoItemEventsClass: *******Finished**************** ");

        }

        public override void ProcessRead()
        {
            // TODO: Add some code
        }

        public override void ProcessReply(object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessReplyAll(object response, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessSend(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:*********Started********");
            try
            {
                Outlook.AppointmentItem currApp = null;
                CurrentModule.IsSendPressed = true;
                try
                {
                    currApp = this.ItemObj as Outlook.AppointmentItem;
                }
                catch
                {
                    // Added by Ravi Goyal - if item is olApptException, com object was released,recreate it from inspector object and match the changes with tuple list.
                    try
                    {
                        UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:CurrApp Location is fixed in catch() using active inspector");
                        Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                        if (ins != null)
                        {
                            object item = ins.CurrentItem;
                            if (item != null)
                            {
                                if (item is Outlook.AppointmentItem)
                                {
                                    currApp = item as Outlook.AppointmentItem;
                                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                                    if ((Module as AddinModule).exclusionoccurencesinfo != null)
                                    if (currApp.Start != (Module as AddinModule).exclusionoccurencesinfo.Item1)
                                    IsStartDateChanged = true;
                                    if (currApp.End != (Module as AddinModule).exclusionoccurencesinfo.Item2)
                                    IsEndDateChanged = true;
                                    if (currApp.Subject != (Module as AddinModule).exclusionoccurencesinfo.Item3)
                                    IsSubjectChanged = true;
                                }
                            }
                        }
                    }
                    catch { }
                }
                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                {
                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currApp.Location.ToLower().Contains("cancelled"))
                    {
                        // bookingHelper.DeleteSingleOccurrenceFromCondeco();
                        if (!isSelectedChanged)
                        {
                            CurrentModule.itemEvents.Remove(this);
                            this.Dispose();
                        }
                        return;
                    }
                    else if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        string MasterLocation = string.Empty;
                        if (CurrentModule.AppMasterLoc != null)
                        {
                            if (CurrentModule.AppMasterLoc.Count > 0)
                            {
                                for (int i = 0; i < 1; i++)
                                {
                                    MasterLocation = CurrentModule.AppMasterLoc[0].ToString();
                                }
                            }
                            else
                            {
                                MasterLocation = currApp.Location;
                            }
                        }
                        else
                        {
                            MasterLocation = currApp.Location;
                        }
                        appHelper.SetAppointmentLocation(currApp, MasterLocation);
                        CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                        return;
                    }
                    else
                    {
                        bookingHelper.DeleteCondecoBooking(currApp, CurrentModule as AddinModule);
                    }
                    CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, true);
                    //bookingHelper.GetBookingLocation(postID,delAppItem)
                    //currApp.Location = curMeeting.MainLocation;
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:Uupdating the location");
                    appHelper.SetAppointmentLocation(currApp, curMeeting.MainLocation);
                    e.Cancel = false;
                    if (!isSelectedChanged)
                    {
                        CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                    }
                    return;
                }
                //Added by Ravi Goyal Check if Meeting was deleted from Delete booking button, if true then set the meeting cancel here
                else if (CurrentModule.AppMasterLoc != null)
                {
                    string MasterLocation = string.Empty;
                    if (CurrentModule.AppMasterLoc.Count > 0)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            MasterLocation = CurrentModule.AppMasterLoc[0].ToString();
                        }
                        currApp.MeetingStatus = Outlook.OlMeetingStatus.olMeetingCanceled;
                        appHelper.SetAppointmentLocation(currApp, MasterLocation);
                        CurrentModule.itemEvents.Remove(this);
                        this.Dispose();
                        return;
                    }

                }
                if (!IsAppointmentMoved)
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:checking Appointment is in sync with Condeco");
                    if (!CheckBookingIsInSyncWithCondeco())
                    {
                        IsAlertDisplayed = false;
                        e.Cancel = true;
                        //Commented by Anand on 21_feb_13 for TP issue 10829
                        //if (!isSelectedChanged)
                        //{
                        //    CurrentModule.itemEvents.Remove(this);
                        //    this.Dispose();
                        //}
                        return;
                    }
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:Syncing Finished");
                }
                else
                {
                    IsAppointmentMoved = false;
                }

                if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !currApp.Location.ToLower().Contains("cancelled"))
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:In case of series, checking syncing of individually edited items");
                    syncManager.SyncIndiviudallyEditedItems(currApp, CurrentModule, true, false);
                }
                if (IsOccurenceChangesDiscard)
                {
                    e.Cancel = true;
                    IsAlertDisplayed = false;
                    IsOccurenceChangesDiscard = false;
                    return;
                }
                if (!isSelectedChanged)
                {
                    CurrentModule.itemEvents.Remove(this);
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in ProcessSend :" + ex.Message + " " + ex.StackTrace);
            }
            
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessSend:*********Finished********");
        }

        public override void ProcessWrite(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:****Started****");
            try
            {
                if (!IsEnableWriteEventCheck) return;
                Outlook.AppointmentItem appItem = null;
                try
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:****trying to get appItem from ItemObj****");
                    appItem = ItemObj as Outlook.AppointmentItem;
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:****trying to get appItem from ItemObj Successful****");
                }
                catch
                {
                    try
                    {
                        UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:CurrApp was Exception and was released in ProcessOpen, fixing in catch() using active inspector");
                        Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                        if (ins != null)
                        {
                            object item = ins.CurrentItem;
                            if (item != null)
                            {
                                if (item is Outlook.AppointmentItem)
                                {
                                    appItem = item as Outlook.AppointmentItem;
                                    // Added by Ravi Goyal - if item is olApptException, com object was released,recreate it from inspector object.
                                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:CurrApp is fixed in catch() using active inspector");
                                }
                            }
                        }
                    }
                    catch { }
                }

                //changed by Anand on 27-Feb-2013 for TP11197 to handle past occurance and exception
                if (appItem != null)
                {
                    //By Anand on 11-March-2013 Moved here as above it might get appItem as null.
                    if (AppointmentDataInfo.GetSkipWriteCheck(appItem)) return;

                    string postId = appHelper.GetAppointmentPostID(appItem);
                    if (!string.IsNullOrEmpty(postId))
                    {
                        if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            if (!CheckBookingIsInSyncWithCondeco(false))
                            {
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            //not cancelled as having post id
                            if (appItem.Start.CompareTo(appItem.End) == 0)
                            {
                                isAppointmentMoved = false;
                                UtilityManager.ShowErrorMessage(CondecoResources.MeetingDate_Start_End_Same);
                                e.Cancel = true;
                            }
                            else if (origStart < DateTime.Now && !appItem.Location.ToLower().Contains("cancelled"))
                            {
                                //show message that updated is not allowed
                                if (origStart == DateTime.MinValue)
                                {
                                    UtilityManager.ShowErrorMessage(CondecoResources.ObjectIssue_NeedToReopen);
                                    e.Cancel = true;
                                }
                                // Below End date  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                                else if (origEnd < DateTime.Now)
                                {
                                    if (!(Module as AddinModule).IsDatePastUpdateMsgShow)
                                    {
                                        if ((Module as AddinModule).HostMajorVersion > 12)
                                        {
                                            UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                                        }
                                        else
                                        {
                                            appItem.Start = OrigStart;
                                            appItem.End = OrigEnd;
                                            appItem.Subject = OrigSubject;
                                            appItem.Save();
                                        }

                                        // isAppointmentMoved = false;
                                        // UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                                        // IsDatePastMsgShow = true;
                                        e.Cancel = true;
                                    }

                                }
                            }

                            //having start and end delimiter
                        }
                    }
                }
                //if (!CheckBookingIsInSyncWithCondeco())
                //{

                //    e.Cancel = true;
                //}
                /* else if (IsSubjectChanged && bookingHelper.IsCondecoBooking(currApp))
               {
                   MessageBox.Show("The recent changes in the item subject may result in your Calendar and the Room Booking Application being out of synchronisation." + Environment.NewLine + "Please make sure you update your changes using the Book Room Tab and then click on Save and Close (or Send Update if the meeting has attendees).", "Condeco Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   e.Cancel = true;
               }
             */
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in ProcessWrite :" + ex.Message + " " + ex.StackTrace);
            }
            finally { UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessWrite:Finally Block()****Finished****"); }
        }

        public override void ProcessBeforeDelete(object item, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            //Added by Anand on 27-Feb-2013 for TP 11992
            Outlook.AppointmentItem appItem = item as Outlook.AppointmentItem;
            BookingHelper bookingHelper = new BookingHelper();
            if (appItem != null)
            {
                //Changed by Anand : Returning as received mails should not cancel the booking. TP12345
                UtilityManager.LogMessage("1-Checking meeting is not olMeetingReceived");
                if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived) return;
                //End Change
                string postId = appHelper.GetAppointmentPostID(appItem);
                if (!string.IsNullOrEmpty(postId))
                {
                    // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh
                    //!appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster is added for past booking delete 
                    if ((appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster) && appItem.Start < DateTime.Now && appItem.Location.Contains(UtilityManager.GetLocationStartDelimiter())
                      && appItem.End < DateTime.Now && appItem.Location.Contains(UtilityManager.GetLocationEndDelimiter()) && !appItem.Location.ToLower().Contains("cancelled"))
                    {
                        //show message that updated is not allowed
                        UtilityManager.LogMessage("process before delete  past booking appItem.RecurrenceState " + appItem.RecurrenceState);
                        // this below message commented .this msg pop up in olk 2010 when cancel past meeting instance  as well as series
                        //UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                        // e.Cancel = true;
                        return;
                    }
                    //chandra for #18802 IsVCWithin1hourCheck in case of reccurance booking 27-Oct-13
                    if (appItem.IsRecurring && appItem.Start.Subtract(DateTime.Now).TotalMinutes < 60)
                    {
                        if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                        {
                            if (!UtilityManager.IsSSOTimeOutMsgShow)
                            {

                                UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                            }
                            //UtilityManager.IsSSOTimeOutMsgShow = false;
                            e.Cancel = true;
                            return;
                        }
                        bookingHelper.IsVCWithin1hourCheck(bookingHelper.GetBookingID(postId, appItem), UtilityManager.GetCurrentUserForPost());
                        if (bookingHelper.IsVCWithin1hour)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.VcNotDeletedWithinAnHour);
                            e.Cancel = true;
                            return;
                        }
                    }
                    //End- chandra for #18802 IsVCWithin1hourCheck in case of reccurance booking 27-Oct-13
                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                    {
                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                        {

                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                        }
                        //UtilityManager.IsSSOTimeOutMsgShow = false;
                        e.Cancel = true;
                        return;
                    }
                    bool permcheck = bookingHelper.PermissionCheck(postId, UtilityManager.GetCurrentUserForPost());
                    // Added Meeting status condition by Paritosh Mishra , to fix CRI2 6507 & CRI2 -6554
                    // if (!(permcheck) )
                    if (!(permcheck) && !((appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled) || (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceivedAndCanceled)))
                    {
                        UtilityManager.ShowWarningMessage(CondecoResources.Check_Booking_Permission);
                        e.Cancel = true;
                    }

                    try
                    {
                        if (UtilityManager.OutlookVersionList.Count == 0)
                        {
                            return;
                        }


                        bool isSharedCalendar = false;

                        appItem = (this.ItemObj as Outlook.AppointmentItem);
                        Outlook.NameSpace ns = appItem.Session as Outlook.NameSpace;
                        Outlook.Recipient recp = ns.CurrentUser;
                        if (!appItem.Organizer.Equals(recp.Name))
                        {
                            isSharedCalendar = true;
                        }

                        UtilityManager.FreeCOMObject(recp);
                        UtilityManager.FreeCOMObject(ns);

                        if (!isSharedCalendar || !UtilityManager.OutlookVersionList.Contains(appItem.OutlookVersion))
                        {
                            return;

                        }
                        bool deleteSeries = false;
                        if ((appItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting) && (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException))
                        {

                            DialogResult response = MessageBox.Show("The meeting you are removing is linked to a recurring room booking in the Room Booking system.  " + Environment.NewLine + " Click 'Yes' to cancel all instances of the room booking. " + Environment.NewLine + " Click 'No' if you wish to just remove just this instance of the room booking. " + Environment.NewLine + " Alternatively click 'Cancel' to cancel this operation. ", CondecoResources.Condeco_Error_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                            Outlook.AppointmentItem itemToBeDeleted = null;
                            if (response == DialogResult.Yes)
                            {
                                deleteSeries = true;

                                try
                                {
                                    itemToBeDeleted = appItem.Parent as Outlook.AppointmentItem;
                                }
                                catch
                                {
                                    itemToBeDeleted = appItem;
                                }




                            }
                            else if (response == DialogResult.Cancel)
                            {
                                e.Cancel = true;
                                return;
                            }
                            else if (response == DialogResult.No)
                            {
                                itemToBeDeleted = appItem;
                            }
                            bookingHelper.DeleteCondecoBooking(itemToBeDeleted, CurrentModule);
                            // else
                            //{  Check 27/10/15

                            // bookingHelper.DeleteCondecoBooking(itemToBeDeleted, deleteSeries);
                            //}
                        }

                    }
                    catch (Exception)
                    {

                    }
                    bookingHelper = null;

                }
            }
            //end

            //MessageBox.Show("Item Before Delete Fires");



        }

        public override void ProcessAttachmentRemove(object attachment)
        {
            ////Below code are added by Paritosh to resolve CRD-6520
            //Outlook.AppointmentItem currApp = this.ItemObj as Outlook.AppointmentItem;
            //if (bookingHelper.IsCondecoBooking(currApp))
            //{
            //    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            //    {
            //        if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
            //        {
            //            try
            //            {
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove before remove attachment");
            //                currApp.Attachments.Remove(1);
            //                currApp.Save();
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove after remove attachment");
            //            }
            //            catch
            //            {
            //                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.ProcessAttachmentRemove catch");
            //            }
            //        }
            //    }
            //}
            //// end // 
        }

        public override void ProcessBeforeAttachmentAdd(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentPreview(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentRead(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAttachmentWriteToTempFile(object attachment, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessUnload()
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeAutoSave(AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeRead()
        {
            // TODO: Add some code
        }

        public override void ProcessAfterWrite()
        {
            // TODO: Add some code
        }

        private bool CheckBookingIsInSyncWithCondeco()
        {
            return CheckBookingIsInSyncWithCondeco(true);
        }

        private bool CheckBookingIsInSyncWithCondeco(bool returnTrueForCancel)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco:*********Started********");

            bool result = true;

            Outlook.AppointmentItem currApp = null;
            try
            {
                currApp = this.ItemObj as Outlook.AppointmentItem;
            }
            catch
            {
                // Added by Ravi Goyal - if item is olApptException, com object was released,recreate it from inspector object
                Outlook.Inspector ins = CurrentModule.OutlookApp.ActiveInspector();
                if (ins != null)
                {
                    object item = ins.CurrentItem;
                    if (item != null)
                    {
                        if (item is Outlook.AppointmentItem)
                        {
                            currApp = item as Outlook.AppointmentItem;
                        }
                    }
                }
            }
            //Outlook.UserProperties uProps = currApp.UserProperties;
            //bool bookRoomClicked = false;
            //if (uProps != null)
            //{
            //    Outlook.UserProperty propBookRoom = uProps.Find("BookRoomClicked", true);

            //    if (propBookRoom != null)
            //    {
            //        bookRoomClicked = Convert.ToBoolean(propBookRoom.Value.ToString());
            //        UtilityManager.FreeCOMObject(propBookRoom);
            //    }

            //}
            //Outlook.UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
            bool bookRoomClicked = false;
            string namedValue = "";
            namedValue = UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked);

            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue :" + namedValue);
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - AddinModule.IsBookRoomClicked :" + AddinModule.IsBookRoomClicked);

            if (!String.IsNullOrEmpty(namedValue))
            {
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue is not null");
                bookRoomClicked = Convert.ToBoolean(namedValue.ToString());
            }
            else//TP#21109 Added by Vineet Yadav on 15 Feb 2014
            {
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco - UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.BookRoomClicked) NamedValue is NullOrEmpty");
                bookRoomClicked = Convert.ToBoolean(AddinModule.IsBookRoomClicked);
            }

            bool isCondecoBooking = bookingHelper.IsCondecoBooking(currApp);

            if (isCondecoBooking)
            {
                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    return true;
                }
            }
            bool appointmentCancelled = false;
            //if (prop != null)
            //{
            //    appointmentCancelled = true;u
            //    UtilityManager.FreeCOMObject(prop);
            //}
            //UtilityManager.FreeCOMObject(uProps);
            string namedValue1 = "";
            namedValue1 = UserPropertiesExtension.GetNamedPropertyRecord(currApp, CustomProperty.AppointmentLocationSynced);
            if (!String.IsNullOrEmpty(namedValue1))
            {
                appointmentCancelled = true;
            }

            if (!isCondecoBooking || appointmentCancelled) return true;
            if (isCondecoBooking && IsRecurrenceRemoved)
            {
                // bookingHelper.DeleteCondecoBooking(currApp);
                bookingHelper.RemoveSeriesFromCondeco(currApp);
                return true;
            }
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, true);

            if (bookRoomClicked)
                IsCondecoBookingSaved = bookingHelper.IsCondecoBookingSaved(currApp);

            if (bookRoomClicked && isCondecoBooking && IsCondecoBookingSaved)
            {
                // int i = 0;
            }
            else
            {
                if (currMeeting.BookingID > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    // || IsSubjectChanged
                    bool subjectReallyChanged = true;
                    bool startDateReallyChanged = true;
                    bool endDateReallyChanged = true;
                    if ((IsSubjectChanged && !IsCondecoBookingSaved))
                    {
                        //Added by Anand On 1-Feb-2013 if setting subject empty its coming null and giving error below.
                        if (currApp.Subject == null) currApp.Subject = " ";
                        //if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        // {
                        if (currApp.Subject.ToLower().Equals(currMeeting.MeetingTitle.ToLower()))
                        {
                            subjectReallyChanged = false;
                        }
                        // }
                        if (subjectReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_Subject);
                        // IsSubjectChanged = false;
                    }
                    //|| IsStartDateChanged
                    //bool seriesChanged = false;
                    long currentTicks;
                    Outlook.RecurrencePattern rpItem = null;
                    if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        rpItem = currApp.GetRecurrencePattern();
                        //if (!bookRoomClicked && !IsCondecoBookingSaved && (IsStartDateChanged || IsEndDateChanged))
                        // {
                        //     seriesChanged = true;
                        // }
                    }

                    if ((IsStartDateChanged && !IsCondecoBookingSaved))
                    {

                        DateTime condecoStart = DateTime.MinValue;
                        DateTime itemStart = DateTime.MinValue;
                        if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            rpItem = currApp.GetRecurrencePattern();
                            currentTicks = rpItem.PatternStartDate.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                            itemStart = new DateTime(currentTicks);
                            itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID, currMeeting.OriginalTZ);
                            //Changed by Anand for Issue 5 for Case TC10 & TC17
                            //currentTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //changed again currentTicks = OrigStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            currentTicks = origSeriesStart.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            //Changed end
                            condecoStart = new DateTime(currentTicks);


                        }
                        else
                        {
                            currentTicks = currApp.Start.Date.Ticks + currApp.Start.TimeOfDay.Ticks;
                            itemStart = new DateTime(currentTicks);
                            itemStart = UtilityManager.ConvertDateToTZ(itemStart, currMeeting.LocationID);
                            currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                            condecoStart = new DateTime(currentTicks);
                        }

                        if (itemStart.CompareTo(condecoStart) == 0)
                        {
                            startDateReallyChanged = false;
                        }


                        if (startDateReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_StartDate);
                        // IsStartDateChanged = false;
                    }
                    // || IsEndDateChanged

                    if ((IsEndDateChanged && !IsCondecoBookingSaved))
                    {
                        DateTime condecoEnd = DateTime.MinValue;
                        DateTime itemEnd = DateTime.MinValue;
                        if (currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            rpItem = currApp.GetRecurrencePattern();
                            currentTicks = rpItem.PatternEndDate.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
                            itemEnd = new DateTime(currentTicks);
                            itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID, currMeeting.OriginalTZ);
                            //Changed by Anand for Issue 5 for Case TC10 & TC17
                            //currentTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //changed again currentTicks = OrigEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            currentTicks = origSeriesEnd.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            //Changed end
                            condecoEnd = new DateTime(currentTicks);


                        }
                        else
                        {
                            currentTicks = currApp.End.Date.Ticks + currApp.End.TimeOfDay.Ticks;
                            itemEnd = new DateTime(currentTicks);
                            itemEnd = UtilityManager.ConvertDateToTZ(itemEnd, currMeeting.LocationID);
                            currentTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                            condecoEnd = new DateTime(currentTicks);
                        }
                        if (rpItem != null)
                            UtilityManager.FreeCOMObject(rpItem);

                        if (itemEnd.CompareTo(condecoEnd) == 0)
                        {
                            endDateReallyChanged = false;
                        }

                        if (endDateReallyChanged)
                            sb.AppendLine(CondecoResources.Message_starter + CondecoResources.Appointment_EndDate);
                        // IsEndDateChanged = false;
                    }



                    if (!string.IsNullOrEmpty(sb.ToString()))
                    {

                        //Please make sure you update your changes using the Book Room Tab and then click on Save and Close (or Send Update if the meeting has attendees)
                        if (!IsAlertDisplayed)
                        {
                            if (currApp.IsRecurring && currApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                            {
                                if (startDateReallyChanged || endDateReallyChanged)
                                    (Module as AddinModule).isItemNotInSync = true;
                                result = HandleSeriesRequest2(sb.ToString(), returnTrueForCancel);
                            }
                            else
                            {
                                if (startDateReallyChanged || endDateReallyChanged)
                                    (Module as AddinModule).isItemNotInSync = true;

                                if (!IsDatePastMsgShow)
                                {
                                    result = HandleNormalRequest(currMeeting, currApp, sb.ToString());

                                }
                                else
                                {
                                    IsDatePastMsgShow = false;
                                }
                            }
                            if (result) IsAlertDisplayed = true;
                        }

                    }
                }
                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.CheckBookingIsInSyncWithCondeco:*********Finished********");
            }
            return result;
        }

        private bool HandleNormalRequest(CondecoMeeting currMeeting, Outlook.AppointmentItem currApp, string currentMessage)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:*********Started********");
            bool result = true;
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.OutofSync_Message_Start);
            message.AppendLine(currentMessage);
            message.AppendLine(CondecoResources.OutofSync_Message_Options);
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:SyncMessage:" + message.ToString());
            (Module as AddinModule).IsDatePastUpdateMsgShow = true;
            DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption,
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            if (response == DialogResult.Yes)
            {
                if (!bookingHelper.PermissionCheck(currMeeting.PostID, UtilityManager.GetCurrentUserForPost()))
                {
                    UtilityManager.ShowErrorMessage(CondecoResources.Check_Booking_Permission);
                    return result = false;
                }

                // Below  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                if (currApp.Start.CompareTo(currApp.End) == 0)
                {
                    MessageBox.Show(CondecoResources.MeetingDate_Start_End_Same, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }


                //if (DateTime.Now.CompareTo(currApp.Start) >= 0 || DateTime.Now.CompareTo(currApp.End) >= 0)
                //{
                //    MessageBox.Show(CondecoResources.Booking_Moved_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                //    return result = false;
                //}
                long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                DateTime meetingStartDateTime = new DateTime(meetingTicks);
                meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                DateTime meetingEndDateTime = new DateTime(meetingTicks);
                DateTime currentDateTime = UtilityManager.ConvertDateToTZ(DateTime.Now, currMeeting.LocationID);
                // Below replace || to && check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                if (currentDateTime.CompareTo(meetingStartDateTime) >= 0 && currentDateTime.CompareTo(meetingEndDateTime) > 0)
                {
                    MessageBox.Show(CondecoResources.Booking_Update_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }
                if (DateTime.Now.CompareTo(currApp.Start) >= 0 && DateTime.Now.CompareTo(currApp.End) > 0)
                {
                    MessageBox.Show(CondecoResources.Booking_Moved_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }
                //fix the CRRI2 6550 ,CRI26043,TP-19968 by Paritosh
                //if (meetingStartDateTime.CompareTo(DateTime.Now) < 0)
                //{
                //    if (meetingStartDateTime.CompareTo(currApp.Start) != 0)
                //    {
                //        MessageBox.Show(CondecoResources.Booking_Update_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        MessageBox.Show("Dont change start date");
                //        return result = false;
                //    }
                //}
                //
                //24/jan/2004
                //  if (meetingStartDateTime.CompareTo(DateTime.MinValue) != 0 && currApp.Start.CompareTo(meetingStartDateTime) != 0 &&  (meetingStartDateTime.CompareTo(DateTime.Now) < 0))
                if (meetingStartDateTime.CompareTo(DateTime.MinValue) != 0 && currApp.Start.CompareTo(meetingStartDateTime) != 0 && (meetingStartDateTime.CompareTo(DateTime.Now) < 0) && (meetingStartDateTime.CompareTo(DateTime.Now) < 0) && (currApp.Start.CompareTo(meetingStartDateTime) < 0))
                {
                    // MessageBox.Show("date change to past");
                    MessageBox.Show(CondecoResources.Booking_Moved_Past, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;

                }

                if (currApp.AllDayEvent)
                {
                    MessageBox.Show(CondecoResources.AllDayEvent_NotSupported, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return result = false;
                }

                //Below line is commented by paritosh for fixing issue TP 22791.While updating cross country dates booking from appointment window, 'You cannot proceed with the booking as dates are crossing two different days.'
                // if (!bookingHelper.CheckCrossDateBookingDueToTimeZoneDifference(currMeeting, currApp, true)) return false;

                UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Going to Uupdate in condeco:" + currApp.Start + ":subject:" + currApp.Subject);

                //Added by Anand on 29 Jan 2013 for TP issue 9947 in one scenario when save failed but updadatecondecobooking passed
                //Save the appointment before sending to update so no inconsistency if further save failed
                //Added by Ravi Goyal on 30-July-2015, When updating a single occurrence of a recurring meeting to a new room, then updating the same occurrence in time, Outlook is crashing.
                if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                {
                    currApp.Save();
                }
                //end change

                //Added by Anand on 1-Feb-2013 for TP10850
                //Validation code to prevent the invalid data update on direct save and close
                result = ValidationManager.ValidateAppointment(currApp, true);
                if (!result) return false;
                //End change
                string messageToDisplay = string.Empty;
                int updateResponse = bookingHelper.UpdateCondecoBooking(currMeeting, currApp);
                if (updateResponse == 1)
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:update is successful in condeco");
                    //bool dataFound = false;
                    DataSet condecoDataSet = bookingHelper.GetMeetingFromCondeco(appHelper.GetAppointmentPostID(currApp));
                    if (condecoDataSet.Tables.Count < 7)
                    {
                        //dataFound = false;
                    }
                    else
                    {
                        if (condecoDataSet.Tables[6].Rows.Count > 0)
                        {
                            appHelper.SaveAppointmentRecord(currApp, condecoDataSet.GetXml());
                            //dataFound = true;
                        }
                    }
                    CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(currApp, false);
                    string newLocSimple = curMeeting.MainLocation;
                    string newLoc = appHelper.FormatLocationInCondecoStyle(newLocSimple);
                    if (currApp.Location == null)
                        currApp.Location = " ";
                    if (!String.IsNullOrEmpty(newLoc) && !String.IsNullOrEmpty(currApp.Location))
                    {
                        if (!currApp.Location.ToLower().Contains(newLoc.ToLower()))
                        {
                            //    currApp.Location = newLoc;
                            appHelper.SetAppointmentLocation(currApp, newLocSimple);

                        }
                    }
                    //Added by Ravi Goyal on 30-July-2015, When updating a single occurrence of a recurring meeting to a new room, then updating the same occurrence in time, Outlook is crashing.
                    if (currApp.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)
                    {
                        if (!(AppointmentDataInfo.GetAppItemCondecoBookingOpen(currApp)) && !UtilityManager.IsSharedCalendarItem(currApp))
                            currApp.Save();
                    }
                    result = true;
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:appointment is updated");
                }
                else if (updateResponse == -97)
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Update failed");
                    MessageBox.Show(CondecoResources.Condeco_Update_Failed_VCBooking, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    result = false;
                }
                else if (updateResponse == -81)
                    messageToDisplay = CondecoResources.Group_SingleDayBookingOnly;
                else if (updateResponse == -82)
                    messageToDisplay = CondecoResources.Group_BusinessHourOverLap;
                else if (updateResponse == -83)
                    messageToDisplay = CondecoResources.Group_AdvancedBookingPeriod;
                else if (updateResponse == -84)
                    messageToDisplay = CondecoResources.Group_NoticeRequired;
                else if (updateResponse == -85)
                {

                    messageToDisplay = CondecoResources.BookingIsInProgress;
                }
                else if (updateResponse == -86)
                {
                    messageToDisplay = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (updateResponse == -999) //added by Anand for permission verification
                    messageToDisplay = CondecoResources.Check_Booking_Permission;
                else
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Update failed");
                    MessageBox.Show(CondecoResources.Condeco_Update_Failed, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    result = false;
                }
                if (!string.IsNullOrEmpty(messageToDisplay))
                {
                    MessageBox.Show(messageToDisplay, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    result = false;
                }

            }
            //Changed by Anand on 14-Feb-2013 and add the condition for Cancel
            //else if (response == DialogResult.No)
            else if (response == DialogResult.No || response == DialogResult.Cancel)
            {
                try
                {
                    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:Reverting the changes");
                    IsEnableWriteEventCheck = false;
                    if (origStart != DateTime.MinValue)
                    {
                        currApp.Start = OrigStart;
                        currApp.End = OrigEnd;
                        currApp.Subject = OrigSubject;
                        IsOccurenceChangesDiscard = true;
                        currApp.Save();
                    }
                }
                catch
                {
                }
                finally
                {
                    IsEnableWriteEventCheck = true;
                }
                result = true;
            }
            else
            {
                result = false;
            }
            (Module as AddinModule).IsDatePastUpdateMsgShow = false;
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleNormalRequest:*********Finished********");
            return result;
        }

        private bool HandleSeriesRequest2(string currentMessage)
        {
            return HandleSeriesRequest2(currentMessage, true);
        }

        private bool HandleSeriesRequest2(string currentMessage, bool returnTrueForCancel)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Started********");
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.OutofSync_Message_Start);
            message.AppendLine(currentMessage);
            message.AppendLine(CondecoResources.Room_Booking_Tab_Instructions);


            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:message" + message);
            //Changed by Anand : Used OkCancel message box and based on cancel its allowing user to close the inspector but prompt message that need to open again to get sync
            MessageBoxButtons options = MessageBoxButtons.OK;
            int version = CurrentModule.HostMajorVersion;
            //Below line changed version check from 12 to 11  by paritosh  to fix CRI2-5814
            // if (version > 11)
            //{
            options = MessageBoxButtons.OKCancel;
            message.AppendLine(CondecoResources.Recurrence_Series_Discard_Instruction);
            //}
            DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption, options, MessageBoxIcon.Information);
            IsOLK2007RecCancelExp = false;
            if (response == DialogResult.Cancel)
            {
                //CRD 6754 Addded flag  isOLK2007RecCancelExp only for OLK 2007 by paritosh
                if (version == 12)
                {
                    IsOLK2007RecCancelExp = true;
                }
                //MessageBox.Show(Resources.Room_Booking_Tab_InstructionsForCancel, Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);              
                return returnTrueForCancel;

            }
            return false;
        }
        private bool HandleSeriesRequest(CondecoMeeting currMeeting, Outlook.AppointmentItem currApp, string currentMessage)
        {
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Started********");
            bool result = true;
            StringBuilder message = new StringBuilder();
            message.AppendLine(CondecoResources.OutofSync_Message_Start);
            message.AppendLine(currentMessage);
            message.AppendLine(CondecoResources.Room_Booking_Tab_Instructions);
            UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:message" + message);
            DialogResult response = MessageBox.Show(message.ToString(), CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (response == DialogResult.Cancel)
            {
                MessageBox.Show(CondecoResources.Room_Booking_Tab_InstructionsForCancel, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;

            }
            /*if (currApp.Start.CompareTo(DateTime.Now) >= 0 || UtilityManager.IsSeriesSaveAllowed())
            {
                MessageBox.Show(message.ToString(), Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                //The recent times changes are discarded as the series time can not be updated as some of the instances are in past.
                DialogResult response = MessageBox.Show(Resources.Recurrence_Series_PastSave, Resources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (response == DialogResult.OK)
                {

                    try
                    {
                        SyncManager sManager = new SyncManager();
                        //sManager.SyncAppointmentWithCondeco(currApp);
                        sManager.SyncSeriesBooking(currApp, false);
                        sManager.SyncIndiviudallyEditedItems(currApp);
                    }
                    catch
                    {
                        UtilityManager.LogMessage("Error Occurred while syncing the series appointment." + message);
                    }
                    currApp.Close(Outlook.OlInspectorClose.olDiscard);
                 
                 result = false;
                }
            }*/
            result = false;
            //    if (response == DialogResult.OK)
            //     {
            //         UtilityManager.ShowErrorMessage(Resources.Room_Booking_Tab_Instructions);
            //         UtilityManager.ShowErrorMessage("Your appointment is not in sync with the room booking application. Please open it again to synchronize.");
            //         currApp.Close(Outlook.OlInspectorClose.olDiscard);
            //         result = false;
            //     }
            //     else if (response == DialogResult.Cancel)
            //     {
            //         try
            //         {
            //             /*Outlook.RecurrencePattern recPattern = currApp.GetRecurrencePattern();
            //             if (recPattern != null)
            //                 recPattern.PatternStartDate = OrigStart.Date;
            //                 recPattern.StartTime = OrigStart;
            //                 recPattern.PatternEndDate = OrigEnd.Date;
            //                 recPattern.EndTime = OrigEnd;

            //             //currApp.Start = OrigStart;
            //             //currApp.End = OrigEnd;
            //                 currApp.Subject = OrigSubject;
            //                 currApp.Save();
            //                 UtilityManager.FreeCOMObject(recPattern);
            //            */


            //         }
            //         catch
            //         {
            //         }
            //         result = true;
            //     }
            //    UtilityManager.LogMessage("CondecoMeetingItemEventsClass.HandleSeriesRequest:*********Finished********");
            return result;
        }
    }
}

