﻿using System;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Items Events Class
    /// </summary>
    public class DeletedItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents
    {
        //static AppointmentHelper appHelper = new AppointmentHelper();
        static BookingHelper bookingHelper = new BookingHelper();
        private AddinModule CurrentModule = null;
        public DeletedItemsEventsClass(AddinExpress.MSO.ADXAddinModule module)
            : base(module)
        {
            if (CurrentModule == null)
                CurrentModule = module as AddinModule;
        }

        public override void ProcessItemAdd(object item)
        {

            if (item is Outlook.AppointmentItem)
            {
                Outlook.AppointmentItem cItem = item as Outlook.AppointmentItem;
                if (!bookingHelper.IsCondecoBooking(cItem))
                {

                    return;
                }
                //if (cItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || cItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                // {
                try
                {
                    bookingHelper.DeleteCondecoBooking(cItem, CurrentModule as AddinModule);
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while deleting using Deleted Folder" + ex.Message);
                    UtilityManager.LogMessage("Error Occurred while deleting using Deleted Folder" + ex.StackTrace);
                }
                // }

            }
        }

        public override void ProcessItemChange(object item)
        {
            // TODO: Add some code
        }

        public override void ProcessItemRemove()
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeFolderMove(object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeItemMove(object item, object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }
    }
}

