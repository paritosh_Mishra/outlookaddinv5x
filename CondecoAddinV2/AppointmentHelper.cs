using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CondecoAddinV2.Constants;
using Microsoft.Win32;
using System.Configuration;
using System.Text;
using CondecoAddinV2.App_Resources;
using Office = Microsoft.Office.Core; //Print 
using Outlook = Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook; //Print

namespace CondecoAddinV2
{
    class AppointmentHelper
    {

        private static string locationStart;
        private static string locationEnd;
        private static int dragnDrop = -1;
        private static bool resetBookingLink = false;
       // private static BookingHelper bookingHelper = new BookingHelper();
        public bool IsCancelAppInvitees = false; 
        public static string LocationStart
        {
            get
            {
                if (string.IsNullOrEmpty(locationStart))
                {
                    string startLoc = ConfigurationManager.AppSettings["LocationStartDelimiter"].ToString();
                    if (!string.IsNullOrEmpty(startLoc))
                    {

                        locationStart = startLoc;
                    }
                }
                return locationStart;



            }
            set { locationStart = value; }
        }

        public static string LocationEnd
        {
            get
            {
                if (string.IsNullOrEmpty(locationEnd))
                {
                    string endLoc = ConfigurationManager.AppSettings["LocationEndDelimiter"].ToString();
                    if (!string.IsNullOrEmpty(endLoc))
                    {

                        locationEnd = endLoc;
                    }
                }
                return locationEnd;



            }
            set { locationStart = value; }
        }

        public static int DragnDrop
        {
            get
            {
                if (dragnDrop == -1)
                {
                    dragnDrop = 1;//Convert.ToInt32(ConfigurationManager.AppSettings["DragnDrop"].ToString());
                    
                }
                return dragnDrop;



            }
            set { dragnDrop = value; }
        }
        public static bool ResetBookingLink
        {
            get
            {
                resetBookingLink = !ConfigurationManager.AppSettings["ResetAfterRoomCancellation"].ToString().Equals("0");            
                return resetBookingLink;
            }
            set
            {
                resetBookingLink = value; 
            }
        }

        public AppointmentHelper()
        {
         
     
     
        }

        public Outlook.AppointmentItem GetCurrentAppointment(object objOutlook)
        {
            UtilityManager.LogMessage("AppointmentHelper.GetCurrentAppointment:*********Started********");
            Outlook.AppointmentItem cItem = null;
            Outlook.Inspector currentInspector = null;
            try
            {
                Outlook.Application currentApp = objOutlook as Outlook.Application;
                currentInspector = currentApp.ActiveInspector();
            
                //object nItem = currentInspector.CurrentItem;
                if (currentInspector.CurrentItem is Outlook.AppointmentItem)
                {
                    UtilityManager.LogMessage("AppointmentHelper.GetCurrentAppointment:Item is appointment type so return that");
                    cItem = currentInspector.CurrentItem as Outlook.AppointmentItem;
                }
                else
                {
                    UtilityManager.FreeCOMObject((object)currentInspector.CurrentItem);
                }

            }
            catch
            {
            }
            finally
            {
             
                UtilityManager.LogMessage("AppointmentHelper.GetCurrentAppointment:Releasing the Current Inspector");
            }
            UtilityManager.LogMessage("AppointmentHelper.GetCurrentAppointment:*********Finished********");
            return cItem;
        }

        public bool IsNewAppointment(AppointmentItem appItem)
        {
            bool result = true;
            if (appItem == null) return result;
            string currentID = this.GetAppointmentPostID(appItem);            
            if (!string.IsNullOrEmpty(currentID))
                result = false;
            return result;

        }

        public string GetAppointmentLegacyPostID(AppointmentItem appItem)
        {
            //if (appItem == null) return string.Empty;
            //if (appItem.UserProperties == null) return string.Empty;
            //Outlook.UserProperties uProps = null;
            //Outlook.UserProperty uProp = null;
            //string iID = "";

            //try
            //{
            //    uProps = appItem.UserProperties;
            //    uProp = uProps.Find("CondecoBookingID", true);
            //    if (uProp == null) return "";

                
           
            //    iID = uProp.Value.ToString();
            //}
            //catch (System.Exception ex)
            //{
            //}
            //finally
            //{

            //    UtilityManager.FreeCOMObject(uProp);
            //    UtilityManager.FreeCOMObject(uProps);
            //}

            
            //return iID;
            if (appItem == null) return string.Empty;
            string iID = "";

            try
            {
                iID = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.CondecoBookingID);

            }
            catch
            {
            }

            return iID;
        }
        
        public string GetAppointmentAttendees(AppointmentItem appItem)
        {
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentAttendees:*********Started********");
            string currAttendees = "";
            try
            {
                if (appItem != null)
                {
                    string reqAttendees = appItem.RequiredAttendees;
                    string optionalAttendees = appItem.OptionalAttendees;

                    if (!string.IsNullOrEmpty(reqAttendees))
                    {

                        currAttendees = reqAttendees;
                    }
                    if (!string.IsNullOrEmpty(optionalAttendees))
                    {
                        if (string.IsNullOrEmpty(currAttendees))
                            currAttendees = optionalAttendees;
                        else
                            currAttendees = currAttendees + ";" + optionalAttendees;
                    }
                    if (currAttendees == null)
                        currAttendees = string.Empty;
                    else
                    {
                        UtilityManager.LogMessage("AppointmentHelper.GetAppointmentAttendees:Filtering started");
                        currAttendees = FilterAttendees(currAttendees, appItem);
                        UtilityManager.LogMessage("AppointmentHelper.GetAppointmentAttendees:Filtering completed");
                    }
                }
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetAppointmentAttendees "+ex.StackTrace);
            }
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentAttendees:*********Finished********");
            return currAttendees;

        }

        public string FilterAttendees(string attendeesList,AppointmentItem appItem)
        {
            List<string> attList = new List<string>(attendeesList.Split(';'));
            string filteredAttendees = "";
            
            foreach (string attendee in attList)
            {
                
                bool checkAttendee = false;
                NameSpace ns = appItem.Session;
                Recipient currentRecip = ns.CurrentUser;
                if (!string.IsNullOrEmpty(attendee) && !attendee.Trim().Equals(currentRecip.Name))
                {
                    checkAttendee = true;
                }
                UtilityManager.FreeCOMObject(currentRecip);
                UtilityManager.FreeCOMObject(ns);

               
                if (!checkAttendee) continue;
                string curAttendee = CheckName(attendee.Trim());


                if (UtilityManager.IsLastNameSwitchOn()) 
                    curAttendee = ConvertName(curAttendee);

                if(curAttendee.Contains("@"))
                {
                    curAttendee = curAttendee + " " + "N/A";
                }
                if(curAttendee.Contains("(") && curAttendee.Contains(")"))
                {
                    int strStart = curAttendee.IndexOf('(');
                    int strEnd = curAttendee.IndexOf(')');
                    string tobeReplaced = curAttendee.Substring(strStart,(strEnd - strStart)+1);
                    curAttendee = curAttendee.Replace(tobeReplaced,string.Empty);
                }
                if (string.IsNullOrEmpty(filteredAttendees))
                    filteredAttendees = curAttendee;
                else
                    filteredAttendees = filteredAttendees + ";" + curAttendee;


            }
            return filteredAttendees;
        }

        public string CheckName(string curAttendee)
        {
            string rtnAttendee = curAttendee;
            if (rtnAttendee.Contains("@"))
            {
                if(rtnAttendee.Contains("("))
                {
                    rtnAttendee  = rtnAttendee.Substring(0,rtnAttendee.IndexOf('(')-1);
                }
            }
            return rtnAttendee;
        }

        public string ConvertName(string curAttendee)
        {
            string convertedName = curAttendee;
            try
            {
                string newAttendee = curAttendee.Trim().Replace(",", "");
                if (!string.IsNullOrEmpty(newAttendee) && newAttendee.Contains(" "))
                {
                    string lastName = newAttendee.Substring(0, curAttendee.IndexOf(" "));
                    int strStart = newAttendee.IndexOf(" ")+1;
                    int strEnd = (newAttendee.Length ) - strStart;

                    string firstName = newAttendee.Substring(strStart, strEnd);
                    convertedName = firstName.Trim() + " " + lastName.Trim();
                }
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in ConvertName " + curAttendee + " " + ex.StackTrace);
            }
            return convertedName;
        }
    
        public string GetAppointmentRecurrenceDates(AppointmentItem appItem)
        {
            if (appItem == null) return string.Empty;
            if (!appItem.IsRecurring) return string.Empty;
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentRecurrenceDates:*********Started********");
            Outlook.RecurrencePattern recPattern = null;
            string recDates = "";
            try
            {
                recPattern = appItem.GetRecurrencePattern();
                if (recPattern == null) return string.Empty;
                string dateFormat = UtilityManager.CondecoDateFormat;

                //BookingHelper bookingHelper = new BookingHelper();
                //DateTime rDTStartDateToTZ;
                //DateTime rDTEndDateToTZ;
                //CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                //if (currMeeting.BookingID > 0)
                //{
                //    long meetingTicks = recPattern.PatternStartDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                //    DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                //    rDTStartDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesStart, currMeeting.LocationID,currMeeting.OriginalTZ);

                //    meetingTicks = recPattern.PatternEndDate.Date.Ticks + recPattern.EndTime.TimeOfDay.Ticks;
                //    DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                //    rDTEndDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesEnd, currMeeting.LocationID, currMeeting.OriginalTZ);//UtilityManager.ConvertDateToTZ(recPattern.PatternEndDate, currMeeting.LocationID);
                //}
                //else
                //{
                //    rDTStartDateToTZ = recPattern.PatternStartDate;
                //    rDTEndDateToTZ = recPattern.PatternEndDate ;
                //}
                
                //bookingHelper = null;

                UtilityManager.LogMessage("AppointmentHelper.GetAppointmentRecurrenceDates:RecString creation started");
                StringBuilder recString = new StringBuilder();
                recString.Append("rRecurrenceType=" + (int)recPattern.RecurrenceType).ToString();
                recString.Append("&rInterval=" + recPattern.Interval);
                recString.Append("&rInstance=" + recPattern.Instance);
                recString.Append("&rOccurrences=" + recPattern.Occurrences);
                recString.Append("&rStartDate=" + recPattern.PatternStartDate.ToString(dateFormat));
                recString.Append("&rEndDate=" + recPattern.PatternEndDate.ToString(dateFormat));
                recString.Append("&rMonthDay=" + recPattern.DayOfMonth);
                recString.Append("&rMonthYear=" + recPattern.MonthOfYear);
                recString.Append("&rWeekMask=" + (int)recPattern.DayOfWeekMask).ToString();
                recString.Append("&iSwitch=8");
                //By anand for TP10181 issue  as already free object in finally
                //UtilityManager.FreeCOMObject(recPattern);                
                string dataToPost = recString.ToString();
                UtilityManager.LogMessage("AppointmentHelper.GetAppointmentRecurrenceDates:Posting Data To Server to get recurrence dataes:"+dataToPost);
                string returnedDates = UtilityManager.PostDataToServer(dataToPost);
                UtilityManager.LogMessage("AppointmentHelper.GetAppointmentRecurrenceDates:Received Recurrence Dates:" + returnedDates);
                if (string.IsNullOrEmpty(returnedDates))
                    recDates = string.Empty;
                else
                    recDates = returnedDates;
            }            
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetAppointmentRecurrenceDates " + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(recPattern);
            }
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentRecurrenceDates:*********Finished********");
            return recDates;

        }

        public string GetAppointmentBookingType(AppointmentItem appItem)
        {

            if (appItem == null) return string.Empty;
            string bookingType = "";
            if (appItem.IsRecurring)
            {
                if (appItem.RecurrenceState == OlRecurrenceState.olApptMaster)
                    bookingType = "1"; // series booking
                else if (appItem.RecurrenceState == OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == OlRecurrenceState.olApptException)
                    bookingType = "0"; // Single Occurrence
            }
            else
            {
                bookingType = "";
            }
            return bookingType;
        }

        public string GetAppointmentItemID(AppointmentItem appItem,int locationID)
        {
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemID:*********Started********");
            if (appItem == null) return string.Empty;
            
            //DatePart("yyyy",tmp_startDate) & fn_addLeadingZeros(datepart("m",tmp_startDate), 2) & fn_addLeadingZeros(datepart("d",tmp_startDate), 2) & "T" & fn_addLeadingZeros(datepart("h",tmp_startTime),2) & fn_addLeadingZeros(datepart("n",tmp_startTime), 2)
            string currentItemID = "";
            try
            {
                DateTime meetingDate = UtilityManager.ConvertDateToTZ(appItem.Start, locationID);
                if (meetingDate != null)
                    currentItemID = meetingDate.Year + meetingDate.Month.ToString("00") + meetingDate.Day.ToString("00") + "T" + meetingDate.Hour.ToString("00") + meetingDate.Minute.ToString("00");
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetAppointmentItemID " + ex.Message + " " + ex.StackTrace);
            }
            //appItem.
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemID: ItemID:"+currentItemID);
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemID:*********Finished********");
            return currentItemID;
        }
        public DateTime GetAppointmentItemDate(AppointmentItem appItem, int locationID)
        {
            if (appItem == null) return DateTime.MinValue;
            DateTime meetingDate = DateTime.MinValue;
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemDate:*********Started********");
            try
            {
                meetingDate = UtilityManager.ConvertDateToTZ(appItem.Start, locationID);
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetAppointmentItemDate " + ex.Message + " " + ex.StackTrace);
            }
            //DatePart("yyyy",tmp_startDate) & fn_addLeadingZeros(datepart("m",tmp_startDate), 2) & fn_addLeadingZeros(datepart("d",tmp_startDate), 2) & "T" & fn_addLeadingZeros(datepart("h",tmp_startTime),2) & fn_addLeadingZeros(datepart("n",tmp_startTime), 2)
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemDate: ItemDate:" + meetingDate);
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentItemDate:*********Finished********");
            return meetingDate;
        }

        public void UpdateBookingClickedSettings(Outlook.AppointmentItem appItem,bool data)
        {
            UtilityManager.LogMessage("AppointmentHelper.UpdateBookingClickedSettings:*********Started******** and data :" + data);
            if (appItem == null) return;

            try
            {
                UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.BookRoomClicked, Convert.ToString(data));
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Catch AppointmentHelper.UpdateBookingClickedSettings Ex.Message : " + ex.Message + " " + ex.StackTrace);
            }
            UtilityManager.LogMessage("AppointmentHelper.UpdateBookingClickedSettings:*********Finished******** and data :" + data);
        }

        public void SaveAppointmentPostID(Outlook.AppointmentItem appItem, string postID)
        {
            if (appItem == null) return;

            if (!string.IsNullOrEmpty(postID))
            {
                int result=0;
                Int32.TryParse(postID,out result);
                if (result > 0)
                {
                    appItem.Companies = postID; 
                }
            }

        }

        public string GetAppointmentPostID(Outlook.AppointmentItem appItem)
        {
            string appPostID = "";
            if (appItem == null) return string.Empty;
            if(!string.IsNullOrEmpty(appItem.Companies))
                appPostID = appItem.Companies;
            return appPostID;
            

        }

        public bool SaveAppointmentRecord(AppointmentItem appItem, string recordSet)
        {
        
            if (appItem == null) throw new ArgumentNullException("Appointment item is null");
            try
            {
                if (!string.IsNullOrEmpty(recordSet))
                {

                    if (AddinModule.CurrentInstance.HostMajorVersion > 12)
                    {

                        bool rtnVal = UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.CondecoAppointmentInformation, Convert.ToString(recordSet));
                        return rtnVal;
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder("");
                        int SplitterIndex = 3000;
                        if (recordSet.Length < SplitterIndex)
                        {
                            UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.NoOfPropCondecoAppInfo, Convert.ToString(0));
                            appItem.PropertyAccessor.SetProperty(CustomProperty.CondecoAppointmentInformation + "0", sb.ToString());
                            return true;
                        }
                        int icount = recordSet.Length / SplitterIndex;
                        bool rtnVal = UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.NoOfPropCondecoAppInfo, Convert.ToString(icount));
                        for (int i = 0; i < icount + 1; i++)
                        {
                            string porpName = CustomProperty.CondecoAppointmentInformation + i.ToString();
                            if (i == 0)
                            {
                                sb = new StringBuilder(recordSet, 0, SplitterIndex, 100);
                            }
                            else
                            {
                                int k = recordSet.Length - (i * SplitterIndex);
                                if (k >= SplitterIndex)
                                {
                                    sb = new StringBuilder(recordSet, i * SplitterIndex, SplitterIndex, 100);
                                }
                                else
                                {
                                    sb = new StringBuilder(recordSet, (i * SplitterIndex), (recordSet.Trim().Length - (i * SplitterIndex)), 100);
                                }
                            }
                            appItem.PropertyAccessor.SetProperty(porpName, sb.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("-- SaveAppointmentRecord(AppointmentItem appItem, string recordSet)-- recordSet="+recordSet);
                UtilityManager.LogMessage("-- SaveAppointmentRecord(AppointmentItem appItem, string recordSet)-- Exception Source:" + ex.Source + "--Message:--"  + ex.Message.ToString() +" --Stack--:"+ ex.StackTrace.ToString());
                return false;
            }
        }

        public string GetAppointmentRecord(AppointmentItem appItem)
        {
            if (appItem == null) return string.Empty;
            
            try
            {
                if (AddinModule.CurrentInstance.HostMajorVersion > 12)
                {
                    //object obj1 = appItem.PropertyAccessor.GetProperty(CustomProperty.CondecoAppointmentInformation);
                    //rtnVal = obj1.ToString();
                    //return rtnVal;

                    string obj1 = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.CondecoAppointmentInformation);
                    return obj1;
                }
                else
                {
                    string strNoOfappInfo = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.NoOfPropCondecoAppInfo);
                    if (strNoOfappInfo == "") return string.Empty;

                    StringBuilder sb = new StringBuilder();
                    int noOfAppRecordProp = Convert.ToInt32(strNoOfappInfo);

                    if (noOfAppRecordProp > 0)
                    {
                        for (int i = 0; i < noOfAppRecordProp + 1; i++)
                        {
                            string p = CustomProperty.CondecoAppointmentInformation + i.ToString();
                            object obj = appItem.PropertyAccessor.GetProperty(p);
                            sb.Append(obj.ToString());
                            obj = null;
                        }

                    }
                    return sb.ToString();
                   
                }
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("-- GetAppointmentRecord(AppointmentItem appItem)-- " );
                UtilityManager.LogMessage("-- GetAppointmentRecord(AppointmentItem appItem)-- " + ex.Source + "--Message:--" + ex.Message.ToString() + " --Stack--:" + ex.StackTrace.ToString());
                return string.Empty; 
            }
        }

        public void ToggleRecurrencebutton(Outlook.Inspector cInsp,bool showButton)
        {

            //Outlook.Inspector cInsp = null;
            
            Office.CommandBarButton newButton = null;
            Office.CommandBars cbars = null;



            //cInsp =  as Outlook.Inspector;
            //MessageBox.Show("Active Explorer Found");
            //Outlook.NameSpace ns = null;




            try
            {
                if (cInsp != null)
                {
                    // folderObject = cExplorer.CurrentFolder;

                    // if (folderObject.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                    // {

                    // MessageBox.Show("Calendar Found");
                   cbars = cInsp.CommandBars;

                    // Office.CommandBar activeBar = cbars.ActiveMenuBar;
                    // activeBar.Reset();
                    
                    newButton = (Office.CommandBarButton)
                    cbars.FindControl(Office.MsoControlType.msoControlButton, 1977, Type.Missing, Type.Missing);

                    if (newButton != null)
                    {
                        //   MessageBox.Show("New button executing");
                        // chkTime = true;
                        // newButton.Execute();
                        newButton.Enabled = showButton;
                        newButton.Visible = showButton;
                        
                        cbars.ReleaseFocus();
                    }

                    cbars.ReleaseFocus();

                    // }
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("--ToggleRecurrencebutton(Outlook.Inspector cInsp,bool showButton) catch" + ex.Message);
            }
            finally
            {

                UtilityManager.FreeCOMObject(newButton);
                UtilityManager.FreeCOMObject(cbars);
            }

        }

        //public bool IsMessageClassValid(string messageClass)
        //{
        //    bool result = true;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(messageClass))
        //        {
        //            List<string> excludedClasses = UtilityManager.MessageClassList;
        //            if (excludedClasses == null) return result;
        //            if (excludedClasses.Count == 0) return result;

        //            string matchedClass = excludedClasses.Find(delegate(string s) { return s.ToLower().Equals(messageClass.ToLower()); });
        //            if (!string.IsNullOrEmpty(matchedClass))
        //                result = false;
        //        }
        //    }
        //    catch(System.Exception ex)
        //    {
        //        UtilityManager.LogMessage("Error has occurred in IsMessageClassValid " + ex.Message + " " + ex.StackTrace);
        //    }
        //    return result;
        //}

        public bool ClearAppointmentData(Outlook.AppointmentItem appItem)
        {
            bool result = true;
            if (appItem == null) return result;
            try
            {
                if (ResetBookingLink)
                {
                    if (appItem.RecurrenceState == OlRecurrenceState.olApptException || appItem.RecurrenceState == OlRecurrenceState.olApptOccurrence)
                        return result;
                }
                else
                {
                    if (appItem.RecurrenceState != OlRecurrenceState.olApptMaster )
                        return result;
                }
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in ClearAppointmentData " + ex.Message + " " + ex.StackTrace);
            }
            if (appItem.UserProperties == null)
            {
                UtilityManager.FreeCOMObject(appItem.UserProperties);
                return result;
            }
           
            //string iID = "";
            string NamedValueBookRoomClick = "";
            try
            {
                appItem.Companies = ""; // Condeco Record Set

                if (IsCancelAppInvitees)
                {
                    appItem.RequiredAttendees = "";
                    appItem.OptionalAttendees = "";
                    IsCancelAppInvitees = false;
                }
                NamedValueBookRoomClick = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.BookRoomClicked);

                if (!String.IsNullOrEmpty(NamedValueBookRoomClick))
                    UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.BookRoomClicked);

                if (AddinModule.CurrentInstance.HostMajorVersion > 12)
                {
                    UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.CondecoAppointmentInformation);
                }
                else
                {
                    string strNoOfappInfo = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.NoOfPropCondecoAppInfo);
                    if (strNoOfappInfo != "")
                    {
                        StringBuilder sb = new StringBuilder();
                        int noOfAppRecordProp = Convert.ToInt32(strNoOfappInfo);
                        if (noOfAppRecordProp > 0)
                        {
                            for (int i = 0; i < noOfAppRecordProp + 1; i++)
                            {
                                string p = CustomProperty.CondecoAppointmentInformation + i.ToString();
                                UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, p);
                            }
                            UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.NoOfPropCondecoAppInfo);
                        }
                    }
                }
                
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in ClearAppointmentData " + ex.Message + " " + ex.StackTrace);
                result = false;
            }
            finally
            {

                //UtilityManager.FreeCOMObject(uProp);
                //UtilityManager.FreeCOMObject(uProp1);
                //UtilityManager.FreeCOMObject(uProps);
            }


            return result;
        }

        public string GetAppointmentLocation(AppointmentItem appItem)
        {
            try{
            
            if (appItem == null) return string.Empty;
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentLocation ***Start*** ");
            if(appItem.Location == null) return string.Empty;
            UtilityManager.LogMessage("AppointmentHelper.GetAppointmentLocation appItem.Location="+ appItem.Location);
            return appItem.Location;
            }
            catch(System.Exception ex)
            {
                UtilityManager.LogMessage("AppointmentHelper.GetAppointmentLocation Catch " + ex.Message );
                return "";
            }
        }

        //Changed by Anand returning bool value if location set for appointmentitem
        public bool SetAppointmentLocation(AppointmentItem appItem, string condecoLocation)
        {
            bool rtnVal = false;
            condecoLocation = UtilityManager.DecodeSpecialCharacter(condecoLocation);
            UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:*********Started********");
            try
            {
                string currentLocation = string.Empty;
                if (appItem == null) return rtnVal;
                UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation: condecoLocation==" + condecoLocation);
                if (string.IsNullOrEmpty(condecoLocation))
                    return rtnVal;
                UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:Updating Location to:" + condecoLocation);
                if (appItem.Location == null || (string.IsNullOrEmpty(LocationStart) && string.IsNullOrEmpty(LocationEnd)))
                {
                    if (!string.IsNullOrEmpty(condecoLocation))
                    {
                        currentLocation = FormatLocationInCondecoStyle(condecoLocation);
                        UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:  currentLocation==" + currentLocation);
                        appItem.Location = currentLocation;
                        rtnVal = true;
                    }
                    return rtnVal;
                }
                UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:rtnVal=" + rtnVal);
                bool locationSet = false;
                StringBuilder newLoc = new StringBuilder();

                if (appItem.Location.Contains(LocationStart))
                {
                    if (appItem.Location.Contains(LocationEnd))
                    {
                        UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:Location Already contain start and end" );
                        currentLocation = FormatLocationInCondecoStyle(condecoLocation);
                        string remPart = GetLocationRemainingPart(appItem.Location);
                        string startPart = GetLocationStartingPart(appItem.Location);
                        if (!string.IsNullOrEmpty(startPart))
                            newLoc.Append(startPart);
                        if (!string.IsNullOrEmpty(currentLocation))
                            newLoc.Append(currentLocation);
                        if (!string.IsNullOrEmpty(remPart))
                            newLoc.Append(remPart);
                        UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:Updating locaion with new location:" + newLoc);
                        locationSet = true;
                    }

                }

                if (!locationSet)
                {
                    UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:Location is not already set:" );
                    currentLocation = FormatLocationInCondecoStyle(condecoLocation);
                    newLoc.Append(currentLocation);

                    //if (!appItem.Location.Equals(CondecoResources.Appointment_Initial_Location))
                    //Commented above line and added below line by Paritosh To fix issue TP #16785.Now only primary VC room is seen in location with "-Cancelled" string , not all VC end.
                    if (!appItem.Location.Equals(CondecoResources.Appointment_Initial_Location) && (appItem.Location.Contains(LocationEnd)))
                            newLoc.Append(appItem.Location);
                    
                    UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:seting new lcoation"+newLoc);
                }
                string newLocation = newLoc.ToString();
                //Changed by Anand : Added condition to check for same location before updating location
                if (String.Compare(appItem.Location, newLocation, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:appItem.Location::newLoc=" + newLoc.ToString());
                    appItem.Location = newLoc.ToString();
                    rtnVal = true;
                }
                //End change
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:Error has occurred while setting the lcoation" + ex.Message);
            }
            UtilityManager.LogMessage("AppointmentHelper.SetAppointmentLocation:*********Finished********");

            return rtnVal;
        }
        public string FormatLocationInCondecoStyle(string currentLocation)
        {
            UtilityManager.LogMessage("AppointmentHelper.FormatLocationInCondecoStyle:*********Started********");
            UtilityManager.LogMessage("AppointmentHelper.FormatLocationInCondecoStyle:Location Before Fomrating:" + currentLocation);
            string cLocation = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(currentLocation))
                {
                    //cLocation = LocationStart + currentLocation.Trim() + LocationEnd;
                    //Changed by Vineet Yadav on 01/15/2014 - Issue number : 20709
                    cLocation = LocationStart + currentLocation.Trim().Replace("&amp;", "&") + LocationEnd;
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("AppointmentHelper.FormatLocationInCondecoStyle:Error has occurred while formating the lcoation" + ex.Message);
            }
            UtilityManager.LogMessage("AppointmentHelper.FormatLocationInCondecoStyle:Location After Fomrating:"+cLocation);
            UtilityManager.LogMessage("AppointmentHelper.FormatLocationInCondecoStyle:*********Finished********");
            return cLocation;
        }

        public string GetLocationRemainingPart(string currentLocation)
        {
            string result = string.Empty;

            
                try
                {
                    if (currentLocation.Contains(locationEnd))
                    {
                        char[] delim = LocationEnd.ToCharArray();
                        result = currentLocation.Split(delim)[2];
                    }
                }
                catch(System.Exception ex)
                {
                    UtilityManager.LogMessage("AppointmentHelper.GetLocationRemainingPart:Error has occurred while GetLocationRemainingPart" + ex.Message);
                }
            
            
            
            return result;
        }
        public string GetLocationStartingPart(string currentLocation)
        {
           string result = string.Empty;


            try
            {
                if (currentLocation.Contains(locationStart))
                {
                    char[] delim = LocationStart.ToCharArray();
                    result = currentLocation.Split(delim)[0];
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("AppointmentHelper.GetLocationStartingPart:Error has occurred while GetLocationRemainingPart" + ex.Message);
            }



            return result;
        }
       

        
        public bool IsOldOccurrenceItem(Outlook.AppointmentItem currAppItem)
        {
            bool result = false;
            if (currAppItem.RecurrenceState == OlRecurrenceState.olApptException || currAppItem.RecurrenceState == OlRecurrenceState.olApptOccurrence)
            {
                string postId = this.GetAppointmentPostID(currAppItem);
                string legacyPostID = GetAppointmentLegacyPostID(currAppItem);
                if (string.IsNullOrEmpty(postId) && !string.IsNullOrEmpty(legacyPostID))
                {
                    result = true;
                }
            }
            return result;
        }


    }
}
