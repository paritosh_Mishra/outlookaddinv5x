using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Reflection;
using System.Globalization;
using System.Web;
using System.Diagnostics;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using System.Collections.Specialized;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Timers;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
namespace CondecoAddinV2
{
    public sealed class UtilityManager
    {

        private UtilityManager()
        {
        }
        //private static string pin;
        //private static string menuPosition;
        private static string hostName;
        private static string hostURL;
        private static string hostScheme;
        private static string iconpath;
        private static string username;
        private static string password;
        private static string currentPassword = string.Empty;
        private static string condecoDateFormat = "dd'/'MM'/'yyyy";
        private static bool currentUserCheck = false;
        private static bool iscurrentUserValid = false;
        private static string userLanguage;
        //Added by Ravi Goyal-to enable reading config file only once at addin load time
        private static string IsLoggingAllowed = string.Empty;
        private static string ssoUserName = string.Empty;
        private static List<string> messageClassList;
        private static List<string> sharedCalendarVersionList;
        ////public static List<string> MessageClassList
        ////{
        ////    get
        ////    {
        ////        if (messageClassList == null)
        ////        {
        ////            string currentClasses = ConfigurationManager.AppSettings["ExcludedMessageClasses"].ToString();
        ////            if (!string.IsNullOrEmpty(currentClasses))
        ////            {
        ////                string[] classes = currentClasses.Split(',');
        ////                messageClassList = new List<string>(classes);
        ////            }
        ////        }
        ////        return messageClassList;



        ////    }
        ////    set { UtilityManager.messageClassList = value; }
        ////}

        public static List<string> OutlookVersionList
        {
            get
            {
                //if (sharedCalendarVersionList == null)
                //{
                //    string currentVersions = ConfigurationManager.AppSettings["SharedCalenderForDeletingRecurringAppointment"].ToString();
                //    if (!string.IsNullOrEmpty(currentVersions))
                //    {
                //        string[] oVersions = currentVersions.Split(',');
                //        sharedCalendarVersionList = new List<string>(oVersions);
                //    }
                //    else
                //    {
                //        sharedCalendarVersionList = new List<string>();
                //    }
                //}
                sharedCalendarVersionList = new List<string>();
                return sharedCalendarVersionList;

            }
            set { UtilityManager.messageClassList = value; }
        }

        static private bool isWebSSOAuthenticated = false;

        public static bool IsWebSSOAuthenticated
        {
            get
            {
                return isWebSSOAuthenticated;
            }
            set
            {
                isWebSSOAuthenticated = value;
            }
        }
        static private bool isSSOTimeOutMsgShow = false;

        public static bool IsSSOTimeOutMsgShow
        {
            get
            {
                return isSSOTimeOutMsgShow;
            }
            set
            {
                isSSOTimeOutMsgShow = value;
            }
        }
        private static string condecoEncryptPassword(string password)
        {
            CondecoAddinV2.rc4Encrypt.rc4encrypt condecoEncryption = new CondecoAddinV2.rc4Encrypt.rc4encrypt();
            condecoEncryption.Password = "zeid";
            condecoEncryption.PlainText = password;
            return condecoEncryption.EnDeCrypt();
        }


        public static string CondecoDateFormat
        {
            get
            {
                return condecoDateFormat;
            }
        }
        public static string HostName
        {
            get
            {
                /* if (string.IsNullOrEmpty(hostName))
                 {
                     hostName = ConfigurationManager.AppSettings["CondecoHost"].ToString();

                 }*/
                return hostName;
            }


            set
            {
                hostName = value;
            }
        }

        public static string SSOUserName
        {
            get
            {
                return ssoUserName;
            }
            set
            {
                ssoUserName = value;
            }
        }
        public static string GetLocationStartDelimiter()
        {
            string locationstartdelimiter = "";
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LocationStartDelimiter"].ToString()))
                {
                    locationstartdelimiter = ConfigurationManager.AppSettings["LocationStartDelimiter"].ToString();
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("UtilityManager.GetLocationStartDelimiter() Error-" + ex.Message);
                UtilityManager.LogMessage("UtilityManager.GetLocationStartDelimiter() Error-" + ex.StackTrace);
            }
            UtilityManager.LogMessage("UtilityManager.GetLocationStartDelimiter() Value is-" + locationstartdelimiter);
            return locationstartdelimiter;
        }
        public static string GetLocationEndDelimiter()
        {
            string locationenddelimiter = "";
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LocationEndDelimiter"].ToString()))
                {
                    locationenddelimiter = ConfigurationManager.AppSettings["LocationEndDelimiter"].ToString();
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("UtilityManager.GetLocationEndDelimiter() Error-" + ex.Message);
                UtilityManager.LogMessage("UtilityManager.GetLocationEndDelimiter() Error-" + ex.StackTrace);
            }
            UtilityManager.LogMessage("UtilityManager.GetLocationEndDelimiter() Value is-" + locationenddelimiter);
            return locationenddelimiter;
        }
        static private bool isSSOTimeOut = false;

        private static bool IsSSOTimeOut
        {
            get
            {
                return isSSOTimeOut;
            }
            set
            {
                isSSOTimeOut = value;
            }
        }

        public static string IconPath
        {

            get
            {

                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                FileInfo fileinFo = new FileInfo(codeBase.Replace("file:///", String.Empty));
                string fullPath = fileinFo.Directory.FullName;
                iconpath = fullPath + @"\icons";
                return iconpath;
            }


        }

        public static string HostURL
        {
            get
            {
                /* if (string.IsNullOrEmpty(hostName))
                 {
                   
                        
                     hostName = ConfigurationManager.AppSettings["CondecoHost"].ToString();

                 }*/
                string result = string.Empty;
                string currentHostName = HostName;
                if (!string.IsNullOrEmpty(currentHostName))
                {
                    if (HostScheme.Contains(Uri.SchemeDelimiter))
                        result = HostScheme + currentHostName;
                    else
                        result = HostScheme + Uri.SchemeDelimiter + currentHostName;
                }

                return result;
            }


            set { hostURL = value; }
        }

        public static string HostScheme
        {
            get
            {
                if (string.IsNullOrEmpty(hostScheme))
                {
                    hostScheme = Uri.UriSchemeHttp;

                }
                return hostScheme;
            }


            set { hostScheme = value; }
        }

        public static bool IsCurrentUserValid
        {
            get
            {
                if (!currentUserCheck)
                {
                    iscurrentUserValid = IsValidCondecoUser(UserName, Password);
                    currentUserCheck = true;
                }
                return iscurrentUserValid;
            }
        }
        public static string EncryptedPassword
        {

            get
            {
                if (currentPassword == string.Empty)
                {
                    // encruypt the password beofre sending
                    currentPassword = condecoEncryptPassword(Password);
                    byte[] toEncodeAsBytes = System.Text.UnicodeEncoding.Unicode.GetBytes(currentPassword);
                    currentPassword = System.Convert.ToBase64String(toEncodeAsBytes);

                    currentPassword = HttpUtility.UrlEncode(currentPassword, System.Text.Encoding.GetEncoding(28591));
                    // currentPassword = HttpUtility.UrlEncode(currentPassword);
                    //currentPassword = currentPassword + "&version=2";
                }


                return currentPassword;

            }

        }


        public static string UserName
        {
            get
            {

                try
                {
                    username = "";
                    Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                    RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

                    if (masterKey != null)
                    {
                        try
                        {
                            username = masterKey.GetValue("UserName").ToString();
                        }
                        catch
                        {
                        }
                        masterKey.Close();
                    }
                }
                catch
                {

                }

                return username;
            }

            set
            {

                username = value;
            }

        }


        public static string Password
        {
            get
            {
                try
                {
                    password = "";
                    Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                    RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

                    if (masterKey != null)
                    {
                        try
                        {
                            password = masterKey.GetValue("Password").ToString();
                            password = condecoEncryptPassword(password);
                        }
                        catch
                        {
                        }
                        masterKey.Close();
                    }
                }
                catch
                {

                }

                return password;
            }

            set
            {
                password = value;

            }

        }



        private readonly static string LOG_FILENAME = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar + "Add-in Express" + Path.DirectorySeparatorChar + "CondecoAddin.txt";
        private readonly static string OutlookDeskNavFile = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar + "Add-in Express" + Path.DirectorySeparatorChar + "OutlookDeskNav.txt";

        public static void LogMessage(string msg)
        {
            if (!IsLoggingEnabled()) return;
            try
            {
                msg = string.Format("{0:G}: {1}\r\n", DateTime.Now, msg);

                File.AppendAllText(LOG_FILENAME, msg);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public static string GetCondecoHostPostingPath()
        {
            string hostPath = "";


            if (UtilityManager.ConnectionMode() == 2)
            {
                hostPath = GetPath("/saml/formpost_saml.asp");
                // hostPath = GetPath("/login/formpost_saml.asp");
            }
            else
            {
                hostPath = GetPath("/login/formpost.asp");
            }


            return hostPath;
        }

        public static string GetLoginPath()
        {

            string loginPath = "";

            if (UtilityManager.ConnectionMode() < 2)
            {
                loginPath = "/login/login.asp";
            }

            else
            {
                loginPath = "/saml/startsaml.asp";
                //loginPath = "/login/startsaml.asp";
            }

            return loginPath;

        }

        public static string GetCondecoConnectionPath()
        {

            string connectionPath = "";

            connectionPath = GetPath("/login/logcheck.asp");
            return connectionPath;
        }

        private static string GetPath(string path)
        {
            string currentHostName = "";
            try
            {
                currentHostName = HostName;

                if (currentHostName.Contains("http://") || currentHostName.Contains("https://"))
                {
                    currentHostName = currentHostName + path;
                }
                else
                {

                    currentHostName = HostURL + path;


                }
            }
            catch
            {
                currentHostName = "";
            }
            return currentHostName;

        }

        private static string GetPath(string host, string path)
        {
            string currentHostName = "";
            try
            {
                currentHostName = host;

                if (currentHostName.Contains("http://") || currentHostName.Contains("https://"))
                {
                    currentHostName = currentHostName + path;
                }
                else
                {
                    currentHostName = HostURL + path;
                }
            }
            catch
            {
                currentHostName = "";
            }
            return currentHostName;

        }

        public static string GetSplashPath()
        {
            //UriBuilder uriBuilder = new UriBuilder();
            //uriBuilder.Scheme = HostScheme;
            //uriBuilder.Host = HostName;
            //uriBuilder.Path = "login/splash.asp";
            //return uriBuilder.ToString();
            return GetPath("/login/splash.asp");
        }

        public static string GetCondecoHostName()
        {
            string hostvalue = "";
            try
            {
                Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

                if (masterKey != null)
                {
                    try
                    {
                        hostvalue = masterKey.GetValue("Host").ToString();
                    }
                    catch
                    {
                    }
                    masterKey.Close();
                }
            }
            catch (System.Exception ex)
            {
                hostvalue = ConfigurationManager.AppSettings["CondecoHost"].ToString();
                UtilityManager.LogMessage("Error Occurred while GetCondecoHostName :" + ex.Message);
                UtilityManager.LogMessage("Error stacked trace while GetCondecoHostName :" + ex.StackTrace);
            }
            string appHostvalue = ConfigurationManager.AppSettings["CondecoHost"].ToString();
            bool saveNewHost = false;
            if (string.IsNullOrEmpty(hostvalue))
            {
                saveNewHost = true;
            }
            if (!saveNewHost && !string.IsNullOrEmpty(appHostvalue))
            {
                if (!hostvalue.ToLower().Equals(appHostvalue.ToLower()))
                {
                    saveNewHost = true;
                }
            }

            if (saveNewHost)
            {
                if (!string.IsNullOrEmpty(appHostvalue))
                {
                    SaveCondecoHostName(appHostvalue);
                    hostvalue = appHostvalue;
                }
                saveNewHost = false;
            }


            try
            {
                string currentScheme = Uri.UriSchemeHttp;
                if (hostvalue.Contains("http://") || hostvalue.Contains("https://"))
                {
                    Uri currentUri = new Uri(hostvalue);
                    if (currentUri.Port == 80 || currentUri.Port == 0 || currentUri.Port == 443)
                        hostvalue = currentUri.Host;
                    else if (currentUri.Port > 0)
                        hostvalue = currentUri.Host + ":" + currentUri.Port;
                    currentScheme = currentUri.Scheme + Uri.SchemeDelimiter;


                }
                HostName = hostvalue;
                HostScheme = currentScheme;

            }
            catch
            {
                hostvalue = appHostvalue;
            }




            return hostvalue;
        }


        public static void GetCondecoUserLanguage()
        {
            string userLng = "";
            try
            {
                Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

                if (masterKey != null)
                {
                    try
                    {
                        userLng = masterKey.GetValue("UserLanguage").ToString();
                    }
                    catch
                    {
                    }
                    masterKey.Close();
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred while UserLanguage :" + ex.Message);
                UtilityManager.LogMessage("Error stacked trace while UserLanguage :" + ex.StackTrace);
            }
            userLanguage = userLng;
        }


        public static bool SaveCondecoUserNamePassword(string userName, string password)
        {
            bool result = false;
            Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

            if (masterKey != null)
            {
                password = condecoEncryptPassword(password);
                /*byte[] toEncodeAsBytes = System.Text.UnicodeEncoding.Unicode.GetBytes(currentPassword);
                password = System.Convert.ToBase64String(toEncodeAsBytes);

                password = HttpUtility.UrlEncode(password, System.Text.Encoding.GetEncoding(28591));
                */

                masterKey.SetValue("UserName", userName);
                masterKey.SetValue("Password", password);

                masterKey.Close();
                result = true;
            }

            UserName = userName;
            Password = password;

            return result;
        }



        public static bool SaveCondecoUserLanguage(string UserLanguage)
        {
            bool result = false;
            Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

            if (masterKey != null)
            {
                masterKey.SetValue("UserLanguage", UserLanguage);
                masterKey.Close();
                result = true;
            }

            return result;
        }



        public static bool SaveCondecoHostName(string hostName)
        {
            bool result = false;
            Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RNM Systems\\Condeco");

            if (masterKey != null)
            {
                masterKey.SetValue("Host", hostName);
                masterKey.Close();
                result = true;
            }


            try
            {
                string currentScheme = Uri.UriSchemeHttp;
                if (hostName.Contains("http://") || hostName.Contains("https://"))
                {
                    Uri currentUri = new Uri(hostName);
                    if (currentUri.Port == 80 || currentUri.Port == 0 || currentUri.Port == 443)
                        hostName = currentUri.Host;
                    else if (currentUri.Port > 0)
                        hostName = currentUri.Host + ":" + currentUri.Port;
                    currentScheme = currentUri.Scheme + Uri.SchemeDelimiter;
                }

                HostName = hostName;
                HostScheme = currentScheme;

            }
            catch
            {
                // HostName = "localhost";
                HostName = ConfigurationManager.AppSettings["CondecoHost"].ToString();
            }




            return result;
        }

        public static string PostDataToServer(string data)
        {
            UtilityManager.LogMessage("UtilityManager.PostDataToServer:*********Started********");
            string backstr = "";
            try
            {
                string url = GetCondecoHostPostingPath();
                backstr = SendDataToSever(url, data);
            }
            catch
            {
            }
            UtilityManager.LogMessage("UtilityManager.PostDataToServer:Result" + backstr);
            UtilityManager.LogMessage("UtilityManager.PostDataToServer:*********Finished********");
            return backstr;
        }

        public static string PostDataToServer(string url, string data, bool IsQuickResponse = false)
        {
            string backstr = "";
            UtilityManager.LogMessage("UtilityManager.PostDataToServer WITH URL:*********Started********");
            try
            {
                backstr = SendDataToSever(url, data, IsQuickResponse);
            }
            catch
            {
            }
            UtilityManager.LogMessage("UtilityManager.PostDataToServer  WITH URL:Result" + backstr);
            UtilityManager.LogMessage("UtilityManager.PostDataToServer WITH URL:*********Finished********");
            return backstr;
        }

        private static CookieContainer myContainer = new CookieContainer();

        private static string SendDataToSever(string url, string data, bool IsQuickResponse = false)
        {
            //Check url based data already available in offline
            string offlineData = OfflineUrlBasedData.GetOfflineData(url + data);
            if (offlineData != null) return offlineData;
            //Check url based data already available in offline complete
            UtilityManager.LogMessage("UtilityManager.SenDataToServer:*********Started********");
            UtilityManager.LogMessage("UtilityManager.SenDataToServer:Sending to URL" + url + " and Data:" + data);
            Stream requestStream = null;
            HttpWebResponse res = null;
            StreamReader sr = null;
            string result = "";
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                //string s = "id="+Server.UrlEncode(xml);
                if (ProxyServerEnabled())
                {
                    req.Proxy.Credentials = CredentialCache.DefaultCredentials;
                }
                byte[] requestBytes = System.Text.Encoding.Default.GetBytes(data);
                req.Method = "POST";
                req.UseDefaultCredentials = true;
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = requestBytes.Length;
                req.CookieContainer = myContainer;
                //added by Ravi Goyal, only for booking open and close to hit logcheck.asp (for PRB0040217)
                if (IsQuickResponse)
                    req.Timeout = 7000;
                else
                    //end added by Ravi Goyal
                    req.Timeout = 120 * 1000;
                req.AllowAutoRedirect = true;
                requestStream = req.GetRequestStream();
                requestStream.Write(requestBytes, 0, requestBytes.Length);
                requestStream.Close();
                try
                {
                    res = (HttpWebResponse)req.GetResponse();

                    sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);

                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        result = sr.ReadToEnd();

                        if (ConnectionMode() == 2 && res.ResponseUri.Host.Contains("adfs") == true)
                        {
                            result = checkSAML(result);
                        }
                    }
                }
                catch (WebException wa)
                {

                    // MessageBox.Show("The meeting you have just removed may not have been deleted from Condeco. Please contact your administrator.", "Condeco", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    LogMessage("100- Web Exception occured while posting Data:" + url + ":::" + data + "::" + wa.Message);
                }


            }
            catch (Exception ex)
            {
                // MessageBox.Show("Error " + ex.Message);
                // MessageBox.Show("The meeting you have just removed may not have been deleted from Condeco. Please contact your administrator.","Condeco",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                LogMessage("101-Exception occured while posting Data:" + url + ":::" + data + "::" + ex.Message);
            }

            finally
            {
                if (requestStream != null)
                    requestStream.Close();
                if (sr != null)
                    sr.Close();
                if (res != null)
                    res.Close();
            }
            UtilityManager.LogMessage("UtilityManager.SenDataToServer:Response bak from server:" + result);
            UtilityManager.LogMessage("UtilityManager.SenDataToServer:*********Finishded********");

            //Add url based data in offline
            OfflineUrlBasedData.SetOfflineData(url + data, result);
            //Add url based data in offline complete

            return result;
        }

        private static string checkSAML(string result)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(result);
            HtmlAgilityPack.HtmlNode docroot = document.DocumentNode;

            string SAMLResponse = "";
            string RelayState = "";
            string finalresponse = "";
            string url = "";

            SAMLResponse = docroot.SelectSingleNode("//input[@type='hidden' and @name='SAMLResponse']").Attributes["value"].Value;
            RelayState = docroot.SelectSingleNode("//input[@type='hidden' and @name='RelayState']").Attributes["value"].Value;
            url = docroot.SelectSingleNode("//form[@name='hiddenform']").Attributes["action"].Value;


            string data = "SAMLResponse=" + SAMLResponse + "&RelayState=" + RelayState;

            if (SAMLResponse != "")
            {

                try
                {
                    CookieAwareWebClient webClient = new CookieAwareWebClient();
                    webClient.UseDefaultCredentials = true;

                    using (webClient)
                    {
                        byte[] response2 = webClient.UploadValues(url, new NameValueCollection()
                                            {{ "SAMLResponse", SAMLResponse },
                                            { "RelayState", RelayState }
                                         });
                        finalresponse = Encoding.ASCII.GetString(response2);
                    }
                }

                catch (WebException ex)
                {
                    LogMessage("100- Web Exception occured while checking SAML Data:" + url + ":::" + data + "::" + ex.Message);
                }

            }
            return finalresponse;
        }


        public class CookieAwareWebClient : WebClient
        {

            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest request = base.GetWebRequest(address);
                HttpWebRequest webRequest = request as HttpWebRequest;
                webRequest.AllowAutoRedirect = true;
                if (webRequest != null)
                {
                    webRequest.CookieContainer = UtilityManager.myContainer;
                }
                return request;
            }
        }
        //Edited by Ravi Goyal-to enable reading config file only once at addin load time
        //public static bool IsLoggingEnabled()
        //{

        //    bool isLoggingEnabled = false;
        //    try
        //    {
        //        isLoggingEnabled = ConfigurationManager.AppSettings["EnableLogging"].ToString() == "1" ? true : false;
        //    }
        //    catch
        //    {
        //        isLoggingEnabled = false;
        //    }
        //    return isLoggingEnabled;
        //}
        //End Edited by Ravi Goyal

        ///Added by Ravi Goyal-to enable reading config file only once at addin load time
        public static bool IsLoggingEnabled()
        {
            bool isLoggingEnabled = false;
            if (string.IsNullOrEmpty(IsLoggingAllowed))
            {
                try
                {
                    isLoggingEnabled = ConfigurationManager.AppSettings["EnableLogging"].ToString() == "1" ? true : false;
                    IsLoggingAllowed = isLoggingEnabled.ToString();
                }
                catch
                {
                    isLoggingEnabled = false;
                    IsLoggingAllowed = isLoggingEnabled.ToString();
                }
            }
            else
            {
                isLoggingEnabled = Convert.ToBoolean(IsLoggingAllowed);
            }
            return isLoggingEnabled;
        }
        //End Added by Ravi Goyal
        public static bool IsSeriesSaveAllowed()
        {
            // = false; //
            bool isSeriesSavedAllowed = false;
            try
            {
                isSeriesSavedAllowed = ConfigurationManager.AppSettings["SeriesSaveAllowedforPastBookings"].ToString() == "1" ? true : false;
            }
            catch
            {
                isSeriesSavedAllowed = true;
            }
            return isSeriesSavedAllowed;
        }


        public static int ConnectionMode()
        {

            int connectionMode = 0;
            try
            {
                connectionMode = int.Parse(ConfigurationManager.AppSettings["ConnectionMode"].ToString());
                //connectionMode = ConfigurationManager.AppSettings["ConnectionMode"].ToString() == "1" ? 1 : 0;
            }
            catch
            {
                connectionMode = 0;
            }

            return connectionMode;
        }




        //public static string MenuPosition()
        //{

        //    menuPosition = "Side"; ;
        //    try
        //    {
        //        menuPosition = ConfigurationManager.AppSettings["MenuMode"].ToString() == "Top" ? "Top" : "Side";
        //    }
        //    catch
        //    {
        //        menuPosition = "Side";
        //    }
        //    return menuPosition;
        //}


        public static int OutlookGridType()
        {

            int gridType = 0;
            try
            {
                gridType = ConfigurationManager.AppSettings["AdvancedBookingGrid"].ToString() == "1" ? 1 : 0;
            }
            catch
            {
                gridType = 0;
            }

            return gridType;
        }


        public static int OutlookGridVersion
        {
            get
            {
                int gridVersion = 2;
                try
                {
                    int.TryParse(ConfigurationManager.AppSettings["gridVersion"].ToString(), out gridVersion);
                }
                catch
                {
                    gridVersion = 2;
                }

                return gridVersion;
            }
        }

        public static string UserLanguage
        {
            get
            {
                string currentLng = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
                if ((!string.IsNullOrEmpty(userLanguage)) && (!userLanguage.Equals(currentLng)))
                    currentLng = userLanguage;

                return currentLng;
            }
            set
            {
                userLanguage = value;
            }
        }


        public static int OutlookBookingFormVersion
        {
            get
            {

                int bookingFormVersion = 2;
                try
                {
                    int.TryParse(ConfigurationManager.AppSettings["bookingFormVersion"].ToString(), out bookingFormVersion);
                }
                catch
                {
                    bookingFormVersion = 2;
                }

                return bookingFormVersion;
            }
        }

        public static bool IsLastNameSwitchOn()
        {
            bool isLastNameSwitchOn = false;
            try
            {
                isLastNameSwitchOn = ConfigurationManager.AppSettings["LastNameSwitch"].ToString() == "1" ? true : false;
            }
            catch
            {
                isLastNameSwitchOn = false;
            }
            return isLastNameSwitchOn;
        }

        public static int GetOutlookVersion(object objOutlook)
        {
            Outlook.Application app = objOutlook as Outlook.Application;
            Version currentver = new Version(app.Version);

            return currentver.Major;
        }

        //Changed by Anand : Added a new method which will return the current user by replacing space with |
        public static string GetCurrentUserForPost()
        {
            return GetCurrentUserName().Replace(" ", "|");
        }
        //End Change

        public static string GetCurrentUserName()
        {

            if (ConnectionMode() == 0 || ConnectionMode() == 2)
            {
                return Environment.UserName;

            }
            else
            {
                return UserName;
            }

        }


        public static string GetLocalTZName()
        {
            /*        Dim lngTZBias, objShell, lngBiasKey, k,tzName
	        IF  CurrentOutlookVersion >= 12  then
		        tzName = item.StartTimeZone
	        else
	        ' Obtain local Time Zone bias from machine registry.
	        Set objShell = CreateObject("Wscript.Shell")
	        'Windows 7 KeyName
	        win7Key= "HKLM\System\CurrentControlSet\Control\" _
    	        & "TimeZoneInformation\TimeZoneKeyName"
		        if regkeyexists(win7Key) = true then
			        tzName = objShell.RegRead(win7Key)
		        else	
	        tzName = objShell.RegRead("HKLM\System\CurrentControlSet\Control\" _
    	        & "TimeZoneInformation\StandardName")
		        end if
	        Set objShell = nothing
	        end if
	        */
            string tzName = "";
            // if (this.GetOutlookVersion() >= 12)
            // {

            //tzName = Registry.LocalMachine.GetValue(@"System\CurrentControlSet\Control\TimeZoneInformation\TimeZoneKeyName", "").ToString();
            try
            {

                /// Below lines are commmneted  by Paritosh and added below two line  becoz while complied with 4.0 its gives unexpected values of Time Zone and its varies from system to system. This code is run accurate only in V2.0 
                tzName = Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\TimeZoneInformation").GetValue("TimeZoneKeyName", "").ToString();
                if (tzName == "")
                    tzName = Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\TimeZoneInformation").GetValue("StandardName", "").ToString();

                UtilityManager.LogMessage("GetLocalTZName()" + tzName);
                tzName = removeNonLetters(tzName).ToString().Trim();

                try
                {
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(tzName);
                }
                catch (Exception ex)
                {

                    UtilityManager.LogMessage("GetLocalTZName()  Catch FindSystemTimeZoneById -" + ex.Message);
                    if (tzName.Contains("TimeTime"))
                    {
                        tzName = tzName.Replace("TimeTime", "Time");
                    }
                    if (tzName.Contains("Timeme"))
                    {
                        tzName = tzName.Replace("Timeme", "Time");
                    }
                    if (tzName.Contains("Timeard "))
                    {
                        tzName = tzName.Replace("Timeard ", "");
                    }
                    if (tzName.Contains("India "))
                    {
                        tzName = "India Standard Time";
                    }
                    UtilityManager.LogMessage("GetLocalTZName()  Catch Fixed FindSystemTimeZoneById tzName-" + tzName);
                }
                //TimeZoneInfo localZone = TimeZoneInfo.Local;
                //tzName = localZone.Id.Trim();
                //   tzName = "India Standard Time";
            }
            catch (Exception exx)
            {
                UtilityManager.LogMessage("GetLocalTZName() Catch End Function -" + exx.Message);
                tzName = "GMT Standard Time";
            }

            /*  if (tzName.Equals("Singapore Standard Time"))
              {

                 tzName = "Malay Peninsula Standard Time";
              }*/
            // }
            return tzName.Trim();
        }
        public static String removeNonLetters(String dirty)
        {
            if (dirty != null)
            {
                char[] charfield = dirty.ToCharArray();
                String clean = "";
                int exitpoint = 0;
                for (int i = 0; i < charfield.Length; i++)
                {
                    bool ok = false;
                    if (Char.IsWhiteSpace(charfield[i]))
                    {
                        ok = true;
                    }

                    if (System.Text.RegularExpressions.Regex.IsMatch(Convert.ToString(charfield[i]), @"^[a-zA-Z1-9.()+-]+$"))
                    {
                        ok = true;
                    }
                    if (ok)
                    {
                        clean += charfield[i];
                        if (Char.IsWhiteSpace(charfield[i]))
                        {
                            if (Char.IsWhiteSpace(charfield[i + 1]))
                            {
                                return clean.Trim();
                            }
                        }
                    }
                    else
                    {
                        exitpoint += 1;
                    }
                    if (exitpoint == 2)
                    {
                        return clean.Trim();

                    }
                }
                return clean;
            }
            else
                return null;
        }

        public static void FreeCOMObject(object curObj)
        {
            //added by anand to track the caller info
            if (IsLoggingEnabled())
            {
                try
                {
                    if ((curObj as Outlook.AppointmentItem) != null)
                    {
                        StackTrace stackTrace = new StackTrace();
                        string callDetail = "!!! UtilityManager.FreeCOMObject";
                        for (int i = 1; i < stackTrace.FrameCount; i++)
                        {
                            if (stackTrace.GetFrame(i).GetMethod().DeclaringType.Namespace.IndexOf("Condeco") == -1) break;
                            string callingClass = stackTrace.GetFrame(i).GetMethod().ReflectedType.FullName;
                            string methodName = stackTrace.GetFrame(i).GetMethod().ToString();
                            callDetail += "<-Call from " + callingClass + " " + methodName;
                        }
                        Debug.Print(callDetail);
                    }
                }
                catch { }
            }
            //added by anand to track the caller info

            try
            {
                if (curObj != null)
                {
                    if (Marshal.IsComObject(curObj))
                    {
                        Marshal.ReleaseComObject(curObj);
                        curObj = null;
                    }
                }
            }
            catch
            { }
        }
        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern IntPtr SetFocus(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint msg, int wParam, int lParam);
        [DllImport("user32.dll")]

        public static extern bool ShowWindowAsync(int hWnd, int nCmdShow);

        public static void MinimizeAppointmentWindow(IntPtr hWnd)
        {
            ShowWindowAsync(hWnd.ToInt32(), (int)SW.MINIMIZE);

        }
        public static void MaximizeAppointmentWindow(IntPtr hWnd)
        {
            ShowWindowAsync(hWnd.ToInt32(), (int)SW.SHOWMAXIMIZED);

        }
        public static void NormalAppointmentWindow(IntPtr hWnd)
        {
            ShowWindowAsync(hWnd.ToInt32(), (int)SW.SHOWNORMAL);

        }

        public static void AppointmentWindow(IntPtr hWnd, int action)
        {
            ShowWindowAsync(hWnd.ToInt32(), action);

        }
        [DllImportAttribute("User32.dll")]
        public static extern IntPtr SetForegroundWindow(int hWnd);
        public enum SW : int
        {

            HIDE = 0,
            SHOWNORMAL = 1,
            SHOWMINIMIZED = 2,
            SHOWMAXIMIZED = 3,
            SHOWNOACTIVATE = 4,
            SHOW = 5,
            MINIMIZE = 6,
            SHOWMINNOACTIVE = 7,
            SHOWNA = 8,
            RESTORE = 9,
            SHOWDEFAULT = 10

        }

        public static void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void ShowWarningMessage(string message)
        {
            MessageBox.Show(message, CondecoResources.Condeco_Warning_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        //---------------------------------------------------------------------------------------------------------------------
        //QuickResponse()- Added by Ravi Goyal for PRB0040217 - Meetings dropping out of calendar when condeco host is offline
        //the method will check if condeco host is offline/online and response time is set to 7 seconds.
        //Method added date-"12-August-2015", "Only used in CondecoItemEventsClass:ProcessOpen and ProcessClosed."
        //Method modified date-"03-Nov-2015.
        public static bool QuickResponse()
        {
            LogMessage("UtilityManager.QuickResponse():*********Started********");
            bool isContactable = false;
            try
            {
                isContactable = IsCondecoContactable(true);
            }
            catch (Exception ex)
            {
                isContactable = false;
                LogMessage("UtilityManager.QuickResponse():An Exception Occured due to " + ex + " **or** host response not received------");
            }
            if (isContactable)
            {
                LogMessage("UtilityManager.QuickResponse():*********Condeco host response-OK********");
            }
            LogMessage("UtilityManager.QuickResponse():*********Finished********");
            return isContactable;
        }
        //End Added by Ravi Goyal
        public static bool IsCondecoContactable(bool IsQuickResponse = false)
        {
            bool isContactable = false;
            try
            {
                string url = GetCondecoConnectionPath();
                string data = string.Empty;

                string result = PostDataToServer(url, data, IsQuickResponse);

                if (!string.IsNullOrEmpty(result))
                {
                    if (result.ToLower().Contains("1"))
                    {
                        isContactable = true;
                    }
                }
            }
            catch { }
            return isContactable;
        }
        //public static string Pin

        //{
        //    get 

        //    {
        //        if (string.IsNullOrEmpty(pin))
        //        {
        //            string url = GetPath("/login/pin.asp?username=" + GetCurrentUserName());
        //        string data = string.Empty;
        //        pin = PostDataToServer(url, data);
        //        }
        //        return pin;
        //    }
        //}
        public static bool IsHostContactable(string host)
        {
            bool isContactable = false;
            try
            {
                string url = GetPath(host, "/login/logcheck.asp");
                string data = string.Empty;

                string result = PostDataToServer(url, data);

                if (!string.IsNullOrEmpty(result))
                {
                    if (result.ToLower().Contains("1"))
                    {
                        isContactable = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage("An error has occurred while geting IsHostContactable for host" + host + " " + ex.Message);
            }
            return isContactable;
        }

        public static string GetUserPin()
        {
            string userPin = string.Empty;
            try
            {
                string url = GetPath("/login/pin.asp?username=" + GetCurrentUserName());
                string data = string.Empty;
                userPin = PostDataToServer(url, data);
            }
            catch (Exception ex)
            {
                LogMessage("An error has occurred while geting user pin" + ex.Message);
            }

            return userPin;

        }
        //Added by Ravi Goyal for PRB0040174, to retrive the application properties, in formpost.asp a new switch iSwitch=16 is introduced.
        public static string ApplicationSettings(string propertyname)
        {
            string propertyval = string.Empty;
            try
            {
                string url = GetPath("/login/formpost.asp");
                string data = string.Empty;
                data = "rAppProperty=" + propertyname + "&iSwitch=16";
                propertyval = PostDataToServer(url, data);
            }
            catch (Exception ex)
            {
                LogMessage("An error has occurred while geting application settings" + ex.Message);
            }
            return propertyval;
        }
        public static bool IsValidCondecoUser(string userName, string password)
        {
            bool isValidUser = false;
            try
            {
                //UriBuilder uriBuilder = new UriBuilder();
                //Uri cURI = null;
                //try
                //{
                //    string host = UtilityManager.HostURL;
                //    string loginPath = "/login/login.asp";                   

                //    uriBuilder.Scheme = UtilityManager.HostScheme;

                //    string port = "";
                //    string currentHost = UtilityManager.HostName; ;
                //    string hostCheck = currentHost;
                //    if (hostCheck.Contains(":"))
                //    {
                //        string[] hostData = hostCheck.Split(':');
                //        currentHost = hostData[0];
                //        port = hostData[1];
                //    }
                //    uriBuilder.Host = currentHost;
                //    if (!string.IsNullOrEmpty(port))
                //    {
                //        int currentPort;
                //        Int32.TryParse(port, out currentPort);
                //        uriBuilder.Port = currentPort;
                //    }
                //    uriBuilder.Path = loginPath;
                //    string currentPassword = condecoEncryptPassword(password);
                //   // string currentPassword = password;
                //    byte[] toEncodeAsBytes= System.Text.UnicodeEncoding.Unicode.GetBytes(currentPassword);
                //    currentPassword= System.Convert.ToBase64String(toEncodeAsBytes);

                //    currentPassword = HttpUtility.UrlEncode(currentPassword,System.Text.Encoding.GetEncoding(28591));


                //    uriBuilder.Query = newPath.ToString();
                //}
                //catch (System.Exception)
                //{
                //}
                //cURI = uriBuilder.Uri;
                string currentPassword = condecoEncryptPassword(password);
                //   // string currentPassword = password;
                byte[] toEncodeAsBytes = System.Text.UnicodeEncoding.Unicode.GetBytes(currentPassword);
                currentPassword = System.Convert.ToBase64String(toEncodeAsBytes);

                currentPassword = HttpUtility.UrlEncode(currentPassword, System.Text.Encoding.GetEncoding(28591));
                StringBuilder newPath = new StringBuilder();
                newPath.Append("thisU=" + userName);
                newPath.Append("&outlookGrid=1&userAuthCheck=1&version=1");
                newPath.Append("&thisP=" + currentPassword);
                newPath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                string data = newPath.ToString();

                string result = SendDataToSever(UtilityManager.GetPath(UtilityManager.GetLoginPath()), data);

                if (!string.IsNullOrEmpty(result))
                {
                    if (result.ToLower().Contains("1"))
                    {
                        isValidUser = true;

                    }
                }
                currentUserCheck = false;
            }
            catch
            { }
            return isValidUser;
        }
        public static string GetOfflinePath()
        {
            string fullPath = "";
            try
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                FileInfo fileinFo = new FileInfo(codeBase.Replace("file:///", String.Empty));
                fullPath = fileinFo.Directory.FullName;
                fullPath = fullPath + @"\Offline.htm";
            }
            catch { }


            return fullPath;
        }
        public static DateTime ConvertDateToLocalTZ(DateTime dateToConvert, int locationID)
        {
            string dateFormat = "ddd " + CondecoDateFormat + " HH:mm";
            DateTime currentDate = DateTime.MinValue;
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                string localTimeZone = GetLocalTZName();
                string tzURL = GetPath("/CondecoGridCache/processCall.ashx?staticResourceID=1");
                string dataToSend = "&func=convertDateToLocalTZ&locationID=" + locationID + "&specificDates=" + localTimeZone + "&selectedDate=" + dateToConvert.ToString(dateFormat);
                string convertedDate = PostDataToServer(tzURL, dataToSend);
                DateTime.TryParseExact(convertedDate, CondecoDateFormat + " HH:mm:ss", provider, DateTimeStyles.AllowWhiteSpaces, out currentDate);
            }
            catch { }
            return currentDate;
        }
        public static DateTime ConvertDateToLocalTZ(DateTime dateToConvert, int locationID, string localTimeZone)
        {
            string dateFormat = "ddd " + CondecoDateFormat + " HH:mm";
            DateTime currentDate = DateTime.MinValue;
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                // string localTimeZone = GetLocalTZName();
                if (string.IsNullOrEmpty(localTimeZone))
                {
                    localTimeZone = GetLocalTZName();
                }
                string tzURL = GetPath("/CondecoGridCache/processCall.ashx?staticResourceID=1");
                string dataToSend = "&func=convertDateToLocalTZ&locationID=" + locationID + "&specificDates=" + localTimeZone + "&selectedDate=" + dateToConvert.ToString(dateFormat);
                string convertedDate = PostDataToServer(tzURL, dataToSend);
                DateTime.TryParseExact(convertedDate, CondecoDateFormat + " HH:mm:ss", provider, DateTimeStyles.AllowWhiteSpaces, out currentDate);
            }
            catch { }
            return currentDate;
        }
        public static DateTime ConvertDateToTZ(DateTime dateToConvert, int locationID)
        {
            string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
            DateTime currentDate = DateTime.MinValue;
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;

                string localTimeZone = GetLocalTZName();
                string tzURL = GetPath("/CondecoGridCache/processCall.ashx?staticResourceID=1");
                string dataToSend = "&func=convertDateToTZ&locationID=" + locationID + "&specificDates=" + localTimeZone + "&selectedDate=" + dateToConvert.ToString(dateFormat);
                string convertedDate = PostDataToServer(tzURL, dataToSend);
                DateTime.TryParseExact(convertedDate, CondecoDateFormat + " HH:mm:ss", provider, DateTimeStyles.AllowWhiteSpaces, out currentDate);
            }
            catch { }
            return currentDate;
        }
        public static DateTime ConvertDateToTZ(DateTime dateToConvert, int locationID, string localTimeZone)
        {
            string dateFormat = "ddd " + CondecoDateFormat + " HH:mm";
            DateTime currentDate = DateTime.MinValue;
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;

                if (string.IsNullOrEmpty(localTimeZone))
                {
                    localTimeZone = GetLocalTZName();
                }
                string tzURL = GetPath("/CondecoGridCache/processCall.ashx?staticResourceID=1");
                string dataToSend = "&func=convertDateToTZ&locationID=" + locationID + "&specificDates=" + localTimeZone + "&selectedDate=" + dateToConvert.ToString(dateFormat);
                string convertedDate = PostDataToServer(tzURL, dataToSend);
                DateTime.TryParseExact(convertedDate, CondecoDateFormat + " HH:mm:ss", provider, DateTimeStyles.AllowWhiteSpaces, out currentDate);
            }
            catch { }
            return currentDate;
        }

        // public static DateTime ConvertDateToCondecoFormat(DateTime dateToConvert)
        //{
        //    string dateFormat = "ddd dd/MM/yyyy HH:mm";
        //    DateTime currentDate = DateTime.MinValue;
        //    try
        //    {
        //        CultureInfo provider = CultureInfo.InvariantCulture;
        //        DateTime currentDate = new DateTime();


        //        DateTime.TryParseExact(convertedDate, "dd/MM/yyyy HH:mm:ss", provider, DateTimeStyles.AllowWhiteSpaces, out currentDate);
        //    }
        //    catch { }
        //    return currentDate;
        //}

        #region CodeForAddinExpressFormRegionCollapseAddedForTPIssue12692
        [DllImport("user32.dll")]
        public static extern bool InvalidateRect(IntPtr hWnd, IntPtr lpRect, bool bErase);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetWindow(IntPtr hwnd, int uCmd);

        [DllImport("user32.dll")]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", EntryPoint = "LockWindowUpdate")]
        public static extern bool LockWindowUpdate(IntPtr hWndLock);

        public const int GW_CHILD = 5;
        public const int GW_HWNDNEXT = 2;

        public static void Invalidate(IntPtr StartWnd)
        {
            IntPtr tempWindow = IntPtr.Zero;
            if (IsWindowVisible(StartWnd))
                InvalidateRect(StartWnd, IntPtr.Zero, true);
            tempWindow = GetWindow(StartWnd, GW_CHILD);
            while (tempWindow.ToInt32() > 0)
            {
                Invalidate(tempWindow);
                tempWindow = GetWindow(tempWindow, GW_HWNDNEXT);
            }
        }
        #endregion


        public static void CollapseCondecoRegion(AddinExpress.OL.ADXOlForm currentForm)
        {
            if (currentForm == null) return;
            IntPtr inspHandle = AddinModule.CurrentInstance.GetOutlookWindowHandle(currentForm.InspectorObj);
            if (inspHandle != IntPtr.Zero)
                LockWindowUpdate(inspHandle);
            try
            {
                currentForm.GetType().InvokeMember("HideFloating", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, currentForm, null);
                if (inspHandle != IntPtr.Zero)
                    Invalidate(inspHandle);
            }
            finally { LockWindowUpdate(IntPtr.Zero); }

            //currentForm.GetType().InvokeMember("HideFloating", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, currentForm, new object[] { });
        }

        public static string GetUserDomainName()
        {
            string result = string.Empty;

            try
            {
                //Below Two lines are change to merge 4.0.17 to V5 By Paritosh
                // result = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
                result = Environment.UserDomainName; //
            }
            catch
            {
            }
            return result;
        }

        public static string GetQueryStringParameters(string outlookPostID, string meetingDate, int resourceItemID, int gridBookingID, string outlookGrid, string meetingEndDate, string outlookEditType, string edit, string From, string To, string BookingID)
        {
            StringBuilder strParams = new StringBuilder();

            strParams.AppendFormat("thisU={0}", UtilityManager.GetCurrentUserName());

            if (UtilityManager.ConnectionMode() == 1)
                strParams.AppendFormat("&thisP={0}", UtilityManager.EncryptedPassword);

            if (!string.IsNullOrEmpty(outlookPostID.Trim()))
                strParams.AppendFormat("&outlookPostID={0}", outlookPostID);
            else
                strParams.Append("&outlookPostID=");

            strParams.Append("&outlook=1");
            strParams.AppendFormat("&edit={0}", edit);
            strParams.Append("&x=1");

            strParams.AppendFormat("&OutlookGrid={0}", outlookGrid);

            strParams.AppendFormat("&MeetingDate={0}", meetingDate);

            if (!string.IsNullOrEmpty(meetingEndDate))
                strParams.AppendFormat("&MeetingEndDate={0}", meetingEndDate);

            if (resourceItemID > 0)
                strParams.AppendFormat("&RoomID={0}", resourceItemID);

            strParams.AppendFormat("&OutlookEditType={0}", outlookEditType);


            if (!string.IsNullOrEmpty(From))
                strParams.AppendFormat("&From={0}", From);

            if (!string.IsNullOrEmpty(To))
                strParams.AppendFormat("&To={0}", To);


            if (!string.IsNullOrEmpty(BookingID))
                strParams.AppendFormat("&BookingID={0}", BookingID);


            //+ UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat));
            //newpath.Append("&To=" + UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat));



            if (gridBookingID > 0)
                strParams.AppendFormat("&GridBookingID={0}", gridBookingID);

            strParams.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
            strParams.Append("&ssoUser=" + UtilityManager.SSOUserName);

            strParams.AppendFormat("&gridVersion={0}", OutlookGridVersion);
            strParams.AppendFormat("&bookingFormVersion={0}", OutlookBookingFormVersion);
            strParams.AppendFormat("&Culture={0}", UserLanguage);

            return strParams.ToString();
        }

        public static string MyRequestURL
        {
            get
            {   //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                return UtilityManager.HostURL + "/funcLinks/myRequests.asp?outlook=1&username=" + UtilityManager.GetCurrentUserName() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName; ;
            }
        }


        public static string GetSSOPath()
        {

            string connectionPath = "";

            connectionPath = GetPath("/saml/startOthersaml.asp");
            return connectionPath;
        }

        public static bool IsSSOAuthentication()
        {
            UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method *********start*****");
            bool isWebLoadedCompleted = false;
            SSOFormAuthentication objSSOFormAuthentication = null;
            try
            {
                objSSOFormAuthentication = SSOFormAuthentication.Instance(UtilityManager.GetSSOPath());
                if (UtilityManager.SsoWebBasedAuthentication() && !UtilityManager.IsWebSSOAuthenticated)
                {
                    UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method UtilityManager.SsoWebBasedAuthentication()=true and UtilityManager.IsWebSSOAuthenticated= false :Form to be open ");
                    objSSOFormAuthentication.StartPosition = FormStartPosition.CenterParent;
                    objSSOFormAuthentication.Top = 10;
                    objSSOFormAuthentication.Size = new Size(998, 680);
                    objSSOFormAuthentication.ShowDialog();
                }
                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication =" + objSSOFormAuthentication + " # URL-" + UtilityManager.GetSSOPath());

                if (UtilityManager.SsoWebBasedAuthentication())
                {
                    if (string.IsNullOrEmpty(SSOUserName))
                    {
                        isWebLoadedCompleted = false;
                    }
                    else
                    {
                        isWebLoadedCompleted = true;
                    }
                    UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method SSOUserName=" + SSOUserName + " ::isWebLoadedCompleted=" + isWebLoadedCompleted);
                    return isWebLoadedCompleted;
                }
                while (objSSOFormAuthentication.webBrowserSSO.ReadyState != WebBrowserReadyState.Complete)
                {
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                }
                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication.webBrowserSSO.WebBrowserReadyState.Complete");
                bool bln = false;
                if (objSSOFormAuthentication.webBrowserSSO.Url.Host.Contains("adfs") == true)
                {
                    bln = ValidateSSORequest(objSSOFormAuthentication.webBrowserSSO.DocumentText);
                    if (!bln)
                    {
                        SSOUserName = "";
                        isWebLoadedCompleted = false;
                        return isWebLoadedCompleted;
                    }
                }

                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString() =" + objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString());
                //while (!objSSOFormAuthentication.webBrowserSSO.Url.ToString().ToLower().Contains(UtilityManager.GetSSOPath().ToLower() + "?ref="))
                //{
                //    Application.DoEvents();
                //}

                IsSSOTimeOut = false;
                objSSOFormAuthentication.aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                objSSOFormAuthentication.aTimer.Enabled = true;
                string getuserFromServer = string.Empty;
                while (!objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString().ToLower().Contains("condeco#~"))
                {
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                    if (IsSSOTimeOut)
                    {
                        IsSSOTimeOut = false;
                        objSSOFormAuthentication.aTimer.Enabled = false;
                        getuserFromServer = "-100";
                        break;
                    }
                }
                objSSOFormAuthentication.aTimer.Elapsed -= new ElapsedEventHandler(OnTimedEvent);
                objSSOFormAuthentication.aTimer.Enabled = false;
                SSOUserName = string.Empty;
                if (getuserFromServer == "-100")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.SSOTimeOut);
                    isWebLoadedCompleted = false;
                    IsSSOTimeOutMsgShow = true;

                    return isWebLoadedCompleted;
                }
                else
                {
                    //--Commented by Ravi Goyal for PRB0042508---
                    //getuserFromServer = objSSOFormAuthentication.webBrowserSSO.DocumentText.Trim();
                    //UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  getuserFromServer =" + getuserFromServer);
                    //getuserFromServer = objSSOFormAuthentication.webBrowserSSO.DocumentText.Replace("condeco#~", "").Trim();
                    //--End Commented by Ravi Goyal for PRB0042508---

                    //Below edited by Ravi Goyal for PRB0042508
                    getuserFromServer = objSSOFormAuthentication.webBrowserSSO.DocumentText.Trim();
                    UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  getuserFromServer =" + getuserFromServer);
                    if (IsStringHTML(getuserFromServer))
                    {
                        getuserFromServer = GetSSOUserNameInHTML(getuserFromServer);
                    }
                    getuserFromServer = getuserFromServer.Replace("condeco#~", "").Trim();
                    //End edited by Ravi Goyal for PRB0042508
                }


                if (getuserFromServer == "-99")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    isWebLoadedCompleted = false;
                }
                else if (getuserFromServer == "0")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Inactive);
                    isWebLoadedCompleted = false;
                }
                else if (getuserFromServer == "-1")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Suspended);
                    isWebLoadedCompleted = false;
                }
                else
                {
                    SSOUserName = getuserFromServer;
                    isWebLoadedCompleted = true;
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  catch(Exception ex) ex=" + ex.Message);
                isWebLoadedCompleted = false;
            }
            finally
            {
                if (objSSOFormAuthentication != null)
                {
                    objSSOFormAuthentication.Dispose();
                }
            }
            UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method *******END ******* isWebLoadedCompleted =" + isWebLoadedCompleted);
            return isWebLoadedCompleted;

        }
        //Added by Ravi Goyal for PRB0042508
        public static bool IsStringHTML(string Inputstring)
        {
            bool containsHTML = false;
            if (!string.IsNullOrEmpty(Inputstring))
            {
                try
                {
                    containsHTML = (Inputstring != HttpUtility.HtmlEncode(Inputstring));
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("UtilityManager.IsStringHTML()-*Exception*" + ex.Message);
                }

            }
            UtilityManager.LogMessage("UtilityManager.IsStringHTML()- value returned is= " + containsHTML);
            return containsHTML;
        }
        public static string GetSSOUserNameInHTML(string HTMLInputstring)
        {
            if (string.IsNullOrEmpty(HTMLInputstring)) return HTMLInputstring;
            string SSOUserName = HTMLInputstring;
            try
            {

                UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML() Method ** start **");
                UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML() Method Input SSOString is =" + HTMLInputstring);
                Regex regex = new Regex(@"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;", RegexOptions.Singleline);
                SSOUserName = regex.Replace(HTMLInputstring, " ").Trim();
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML()-*Error* " + ex.Message);
                UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML()-*trying to handle error now* ");
                try
                {
                    int Pos1 = HTMLInputstring.IndexOf("<BODY>") + "<BODY>".Length;
                    int Pos2 = HTMLInputstring.IndexOf("</BODY>");
                    if (Pos1 > -1 && Pos2 > -1)
                    {
                        SSOUserName = HTMLInputstring.Substring(Pos1, Pos2 - Pos1);
                    }
                }
                catch (Exception exc)
                {
                    UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML()-*Error in catch while handling exception, error message is =* " + exc.Message);
                }
            }
            UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML() Method *********finished*****");
            UtilityManager.LogMessage("UtilityManager.GetSSOUserNameInHTML(), SSOUser Name is= " + SSOUserName);
            if (!IsStringHTML(SSOUserName))
            {
                return SSOUserName;
            }
            else
            {
                return HTMLInputstring;
            }
        }
        //End Added by Ravi Goyal for PRB0042508
        public static bool IsDeskBookingSSOAuthentication()
        {
            UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method *********start*****");
            bool isWebLoadedCompleted = false;
            SSOFormAuthentication objSSOFormAuthentication = null;
            try
            {
                objSSOFormAuthentication = SSOFormAuthentication.Instance(UtilityManager.GetSSOPath() + "?deskbooking=1");
                if (UtilityManager.SsoWebBasedAuthentication() && !UtilityManager.IsWebSSOAuthenticated)
                {

                    objSSOFormAuthentication.StartPosition = FormStartPosition.CenterParent;
                    objSSOFormAuthentication.Top = 10;
                    objSSOFormAuthentication.Size = new Size(998, 680);
                    objSSOFormAuthentication.ShowDialog();
                }
                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication =" + objSSOFormAuthentication + " # URL-" + UtilityManager.GetSSOPath());
                if (UtilityManager.SsoWebBasedAuthentication())
                {
                    if (string.IsNullOrEmpty(SSOUserName))
                    {
                        isWebLoadedCompleted = false;
                    }
                    else
                    {
                        isWebLoadedCompleted = true;
                    }
                    return isWebLoadedCompleted;
                }
                while (objSSOFormAuthentication.webBrowserSSO.ReadyState != WebBrowserReadyState.Complete)
                {
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                }
                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication.webBrowserSSO.WebBrowserReadyState.Complete");
                bool bln = false;
                if (objSSOFormAuthentication.webBrowserSSO.Url.Host.Contains("adfs") == true)
                {
                    bln = ValidateSSORequest(objSSOFormAuthentication.webBrowserSSO.DocumentText);
                    if (!bln)
                    {
                        SSOUserName = "";
                        isWebLoadedCompleted = false;
                        UtilityManager.LogMessage("UtilityManager IsSSOAuthentication() Method ValidateSSORequest=False; objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString() =" + objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString());
                        return isWebLoadedCompleted;
                    }
                }

                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString() =" + objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString());
                //while (!objSSOFormAuthentication.webBrowserSSO.Url.ToString().ToLower().Contains(UtilityManager.GetSSOPath().ToLower() + "?ref="))
                //{
                //    Application.DoEvents();
                //}
                objSSOFormAuthentication.aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                objSSOFormAuthentication.aTimer.Enabled = true;
                string getuserFromServer = string.Empty;
                while (!objSSOFormAuthentication.webBrowserSSO.DocumentText.ToString().ToLower().Contains("condeco#~"))
                {
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                    if (IsSSOTimeOut)
                    {
                        UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  getuserFromServer request TimeOut detected, user not authenticated");
                        IsSSOTimeOut = false;
                        objSSOFormAuthentication.aTimer.Enabled = false;
                        getuserFromServer = "-100";
                        break;
                    }

                }
                objSSOFormAuthentication.aTimer.Elapsed -= new ElapsedEventHandler(OnTimedEvent);
                objSSOFormAuthentication.aTimer.Enabled = false;
                SSOUserName = string.Empty;
                if (getuserFromServer == "-100")
                {
                    UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  getuserFromServer returned value is =" + getuserFromServer);
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.SSOTimeOut);
                    isWebLoadedCompleted = false;
                    IsSSOTimeOutMsgShow = true;
                }
                else
                {
                    getuserFromServer = objSSOFormAuthentication.webBrowserSSO.DocumentText.Trim();
                    getuserFromServer = objSSOFormAuthentication.webBrowserSSO.DocumentText.Replace("condeco#~", "").Trim();
                }


                if (getuserFromServer == "-99")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    isWebLoadedCompleted = false;
                }
                else if (getuserFromServer == "0")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Inactive);
                    isWebLoadedCompleted = false;
                }
                else if (getuserFromServer == "-1")
                {
                    UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_SSO_Suspended);
                    isWebLoadedCompleted = false;
                }
                else
                {
                    SSOUserName = getuserFromServer;
                    isWebLoadedCompleted = true;
                }
                UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method  getuserFromServer =" + getuserFromServer);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage(" BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  catch(Exception ex) ex=" + ex.Message);
                isWebLoadedCompleted = false;
            }
            finally
            {
                if (objSSOFormAuthentication != null)
                {
                    objSSOFormAuthentication.Dispose();
                }
            }
            UtilityManager.LogMessage("UtilityManager  IsSSOAuthentication() Method *******END ******* isWebLoadedCompleted =" + isWebLoadedCompleted);
            return isWebLoadedCompleted;

        }
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            IsSSOTimeOut = true;

        }

        private static bool ValidateSSORequest(string result)
        {
            bool bln = false;
            try
            {
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(result);
                HtmlAgilityPack.HtmlNode docroot = document.DocumentNode;

                string SAMLResponse = "";
                string RelayState = "";

                string url = "";

                SAMLResponse = docroot.SelectSingleNode("//input[@type='hidden' and @name='SAMLResponse']").Attributes["value"].Value;
                RelayState = docroot.SelectSingleNode("//input[@type='hidden' and @name='RelayState']").Attributes["value"].Value;
                url = docroot.SelectSingleNode("//form[@name='hiddenform']").Attributes["action"].Value;
                UtilityManager.LogMessage("UtilityManager.ValidateSSORequest-Relay State returned is  " + RelayState);
                UtilityManager.LogMessage("UtilityManager.ValidateSSORequest-url returned is  " + url);
                UtilityManager.LogMessage("UtilityManager.ValidateSSORequest, Request Validated is- " + bln);
                bln = true;
            }
            catch
            {
                bln = false;
            }
            return bln;
        }

        public static bool SsoWebBasedAuthentication()
        {

            bool ssoWebBasedAuthentication = false;
            try
            {
                ssoWebBasedAuthentication = ConfigurationManager.AppSettings["SsoWebBasedAuthentication"].ToString() == "1" ? true : false;
            }
            catch
            {
                ssoWebBasedAuthentication = false;
            }
            return ssoWebBasedAuthentication;
        }

        public static int SSOTimeOut()
        {

            int ssoTimeOut = 1;
            try
            {
                ssoTimeOut = Convert.ToInt16(ConfigurationManager.AppSettings["SSOTimeOut"]);
            }
            catch
            {
                ssoTimeOut = 60;
            }
            return ssoTimeOut;
        }

        //public static DataSet CreateDatasetRecDateHasDST()
        //{
        //    DataSet ds = new DataSet("OccData");
        //    DataTable dt;
        //   // DataRow dr;
        //    DataColumn postidColumn;
        //    DataColumn bookingidColumn;
        //    DataColumn OccrDateColumn;
        //    int i = 0;

        //    dt = new DataTable();
        //    postidColumn = new DataColumn("PostID", Type.GetType("System.String"));
        //    bookingidColumn = new DataColumn("BookingID", Type.GetType("System.Int32"));
        //    OccrDateColumn = new DataColumn("OccuranceDate", Type.GetType("System.DateTime"));

        //    dt.Columns.Add(postidColumn);
        //    dt.Columns.Add(bookingidColumn);
        //    dt.Columns.Add(OccrDateColumn);

        //    //dr = dt.NewRow();
        //    //dr["ID"] = PostID;
        //    //dr["BookingID"] = BookingID;
        //    //dr["OccuranceDate"] = OccuranceDate;

        //    //dt.Rows.Add(dr);

        //    ds.Tables.Add(dt);
        //    return ds;
        //}
        //public static void FilledDSTStoreOccDateHasDST( DataSet ds, string PostID, int BookingID, DateTime OccuranceDate)
        //{
        //    try
        //    {
        //        DataTable dt = ds.Tables[0];
        //        DataRow dr;
        //        dr = dt.NewRow();
        //        dr["PostID"] = PostID;
        //        dr["BookingID"] = BookingID;
        //        dr["OccuranceDate"] = OccuranceDate;

        //        dt.Rows.Add(dr);
        //    }
        //    catch
        //    {
        //    }
        //}

        /*public static bool StringExistsInCulture(string key, CultureInfo ci)
        {
            ResourceManager resources = new ResourceManager(typeof(Admin));
            string defaultString = resources.GetString(key, CultureInfo.InvariantCulture);
            string transString = resources.GetString(key, ci);

            return (defaultString == transString);
        }*/
        //public static bool IsForceCultureENGB()
        //{

        //    bool isForceCultureENGB = true;
        //    try
        //    {
        //        isForceCultureENGB = ConfigurationManager.AppSettings["ForceCultureENGB"].ToString() == "true" ? true : false;
        //    }
        //    catch
        //    {
        //        isForceCultureENGB = true;
        //    }
        //    return isForceCultureENGB;
        //}

        #region Added by Paritosh ref: CRD-7237,(below function copy of web method private string HTMLDecode(string sText) + some more replace character) To handle special character in Location
        public static string DecodeSpecialCharacter(string sText)
        {
            sText = WebUtility.HtmlDecode(sText);
            //sText = sText.Replace("&#33;", "!");
            //sText = sText.Replace("&#34;", "\"");
            //sText = sText.Replace("&#35;", "#");
            //sText = sText.Replace("&#36;", "$");
            //sText = sText.Replace("&#37;", "%");
            //sText = sText.Replace("&#38;", "&");
            //sText = sText.Replace("&#40;", "(");
            //sText = sText.Replace("&#41;", ")");
            //sText = sText.Replace("&#43;", "+");
            //sText = sText.Replace("&#44;", ",");
            //sText = sText.Replace("&#60;", "<");
            //sText = sText.Replace("&#61;", "=");
            //sText = sText.Replace("&#62;", ">");
            //sText = sText.Replace("&#63;", "?");
            //sText = sText.Replace("&#64;", "@");
            //sText = sText.Replace("&#94;", "^");
            //sText = sText.Replace("&#124;", "|");
            //sText = sText.Replace("&#126;", "~");
            //sText = sText.Replace("&#160;", "�");
            //sText = sText.Replace("&#161;", "�");
            //sText = sText.Replace("&#162;", "�");

            //sText = sText.Replace("&#163;", "�");
            //sText = sText.Replace("&#167;", "�");
            //sText = sText.Replace("&#170;", "�");
            //sText = sText.Replace("&#172;", "�");
            //sText = sText.Replace("&#176;", "�");
            //sText = sText.Replace("&#186;", "�");
            //sText = sText.Replace("&#191;", "�");
            //sText = sText.Replace("&#192;", "�");
            //sText = sText.Replace("&#198;", "�");
            //sText = sText.Replace("&#201;", "�");
            //sText = sText.Replace("&#202;", "�");
            //sText = sText.Replace("&#218;", "�");
            //sText = sText.Replace("&#224;", "�");
            //sText = sText.Replace("&#225;", "�");
            //sText = sText.Replace("&#226;", "�");
            //sText = sText.Replace("&#227;", "�");
            //sText = sText.Replace("&#228;", "�");
            //sText = sText.Replace("&#229;", "�");
            //sText = sText.Replace("&#230;", "�");
            //sText = sText.Replace("&#231;", "�");
            //sText = sText.Replace("&#232;", "�");
            //sText = sText.Replace("&#233;", "�");
            //sText = sText.Replace("&#234;", "�");
            //sText = sText.Replace("&#235;", "�");
            //sText = sText.Replace("&#237;", "�");
            //sText = sText.Replace("&#238;", "�");
            //sText = sText.Replace("&#239;", "�");
            //sText = sText.Replace("&#241;", "�");
            //sText = sText.Replace("&#243;", "�");
            //sText = sText.Replace("&#244;", "�");
            //sText = sText.Replace("&#245;", "�");

            //sText = sText.Replace("&#246;", "�");
            //sText = sText.Replace("&#247;", "�");
            //sText = sText.Replace("&#248;", "�");
            //sText = sText.Replace("&#249;", "�");

            //sText = sText.Replace("&#250;", "�");
            //sText = sText.Replace("&#251;", "�");
            //sText = sText.Replace("&#252;", "�");
            //sText = sText.Replace("&#253;", "�");
            //sText = sText.Replace("&#254;", "�");
            //sText = sText.Replace("&#255;", "�");		
            //sText = sText.Replace("&#710;", "�");
            //sText = sText.Replace("&#8211;", "�");
            //sText = sText.Replace("&#8216;", "�");
            //sText = sText.Replace("&#8217;", "�");
            //sText = sText.Replace("&#8225;", "�");
            return sText;
        }
        #endregion

        public static bool SharedCalendarUsed()
        {

            bool sharedCalendarUsed = false;
            try
            {
                sharedCalendarUsed = ConfigurationManager.AppSettings["SharedCalenderUsed"].ToString() == "1" ? true : false;
            }
            catch
            {
                sharedCalendarUsed = false;
            }

            return sharedCalendarUsed;
        }


        public static bool ProxyServerEnabled()
        {

            bool proxyServerEnabled = false;
            try
            {
                proxyServerEnabled = true;// ConfigurationManager.AppSettings["ProxyServerEnabled"].ToString() == "1" ? true : false;
            }
            catch
            {
                proxyServerEnabled = false;
            }

            return proxyServerEnabled;
        }
        public static bool IsUnicode(string input)
        {
            Int32 asciiBytes = 0;
            Int32 unicodBytes = 0;
            if (!String.IsNullOrEmpty(input))
            {
                try
                {
                    asciiBytes = Encoding.ASCII.GetByteCount(input);
                    unicodBytes = Encoding.UTF8.GetByteCount(input);
                    UtilityManager.LogMessage("BookingHelper.IsUnicode:Output> ASCII Bytes count" + asciiBytes);
                    UtilityManager.LogMessage("BookingHelper.IsUnicode:Output> UNICODE Bytes count" + unicodBytes);
                    if (asciiBytes > 0 && unicodBytes > 0)
                    {
                        return asciiBytes != unicodBytes;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("BookingHelper.IsUnicode:Exception occured" + ex.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static string CheckHtmlDecode(string Input)
        {
            UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:*********started********");
            UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:-Meeting Input value is:-" + Input);
            string MeetingTitle = string.Empty;
            try
            {
                MeetingTitle = System.Net.WebUtility.HtmlDecode(Input);
                int count1 = MeetingTitle.Where(x => x == '(').Count();
                int count2 = MeetingTitle.Where(x => x == ')').Count();
                int count3 = MeetingTitle.Where(x => x == '<').Count();
                int count4 = MeetingTitle.Where(x => x == '>').Count();
                if (count1 > count2)
                {
                    if (count2 < 1)
                    {
                        MeetingTitle = MeetingTitle.Replace('(', '"');
                    }
                }
                if (count3 > count4)
                {
                    if (count4 < 1)
                    {
                        MeetingTitle = MeetingTitle.Replace("<", "");
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:catch():-" + ex.Message);
                MeetingTitle = Input;
                UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:catch():- **meeting title reset in catch & matched with Input value**");
            }
            if (MeetingTitle.Equals(Input))
            {
                UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:No Changes in meeting title in CheckHtmlDecode");
            }
            else
            {
                UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:Changes identified in meeting title in CheckHtmlDecode");
            }
            UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:Meeting title returned is:-" + MeetingTitle);
            UtilityManager.LogMessage("SyncManager.CheckHtmlDecode:*********finished********");
            return MeetingTitle;
        }
        //Added by Ravi Goyal to read & write the user Outlook navigation preferences-PRB0041123
        public static string ReadNavigationPreferences()
        {
            string values = string.Empty;
            try
            {
                string text = System.IO.File.ReadAllText(OutlookDeskNavFile);
                if (!string.IsNullOrEmpty(text))
                {
                    values = text;
                }
            }
            catch 
            {
                values = "4,True";
            }
            
            if(String.IsNullOrEmpty(values))
            {
                values = "4,True";
            }
            return values;
        }
        //Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
        public static void SetDeskNavPropertyOnClose(Outlook.NavigationPane ObjNavPane, int HostMajorVersion)
        {
            LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose:  **Started** ");
            
            int position = -1;
            try
            {
                int modulecount = ObjNavPane.Modules.Count;
                bool writedone = false;
                for (int i = 1; i <= modulecount; i++)
                {
                    if(!writedone)
                    {
                    try
                    {
                        if (ObjNavPane.Modules[i].Name.ToLower() == "solutions")
                        {
                            position = i;
                            string Valuestowrite = string.Empty;
                            if (HostMajorVersion == 14)
                            {
                                Valuestowrite = position + "," + ObjNavPane.Modules[i].Visible; 
                            }
                            else
                            {
                                Valuestowrite = position + "," + "True"; 
                            }
                            File.WriteAllText(OutlookDeskNavFile, Valuestowrite);
                            LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose: SolutionModule is saved to position =" + position + "  With Visible  " + ObjNavPane.Modules[i].Visible);
                            writedone = true;
                        }
                    }
                    catch (Exception ex) { LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose:  **Error while saving Solutions in Property " + ex.Message); }
                    }
                    if(writedone)
                    {
                        FreeCOMObject(ObjNavPane);
                        LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose:  **Finished** ");
                        return;
                    }
                }
            }
            catch (Exception ex) { LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose:  **Error in method "+ex.Message); }
            FreeCOMObject(ObjNavPane);
            LogMessage("UtilityManager.SetDeskNavPropertyBeforeClose:  **Finished** ");
        }
        //End Added by Ravi Goyal to write the user Outlook navigation preferences-PRB0041123
        public static bool IsSharedCalendarItem(Outlook.AppointmentItem appItem)
        {
            bool IsItemOnSharedCal = false;
            LogMessage("UtilityManager.IsSharedCalendar: checking if appoinment item is on shared calendar");
            try
            {
                Outlook.NameSpace ns = appItem.Session as Outlook.NameSpace;
                Outlook.Recipient recp = ns.CurrentUser;
                if (!appItem.Organizer.Equals(recp.Name))
                {
                    IsItemOnSharedCal = true;
                }
                UtilityManager.FreeCOMObject(recp);
                UtilityManager.FreeCOMObject(ns);
            }
            catch (Exception ex) { LogMessage("UtilityManager.IsSharedCalendar: " + ex.Message); }
            if (IsItemOnSharedCal)
            {
                LogMessage("UtilityManager.IsSharedCalendar: appoinment item is on shared calendar- true");
            }
            else
            {
                LogMessage("UtilityManager.IsSharedCalendar: appoinment item is on shared calendar- false");
            }
            return IsItemOnSharedCal;
        }
    }
}
