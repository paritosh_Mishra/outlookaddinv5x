using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using Outlook = Microsoft.Office.Interop.Outlook;
using Exception = System.Exception;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;
using System.Web;
using Microsoft.Office.Interop.Outlook;
using System.Linq;


namespace CondecoAddinV2
{
    public sealed class BookingHelper
    {
        public bool IsVCWithin1hour = false;
        private static AppointmentHelper appHelper = new AppointmentHelper();
        public void DeleteSingleOccurrenceFromCondeco(Outlook.AppointmentItem currentItem, DateTime meetingOriginalDate, string postID, string bookingData)
        {
            //Changed by Anand : Returning as received mails should not cancel the booking. CRI 5451
            UtilityManager.LogMessage("1-Checking meeting is not olMeetingReceived");
            if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived) return;


            //End Change

            bool singleInstance = true;
            UtilityManager.LogMessage("BookingHelper.DeleteSingleOccurrenceFromCondeco:*********Started********");
            try
            {
                UtilityManager.LogMessage("DeleteSingleOccurrenceFromCondeco:1-Inside DeleteMeetingFromCondeco for booking dated:" + meetingOriginalDate);
                //string permcheck = "false";
                string CondecoHost = UtilityManager.HostName;
                UtilityManager.LogMessage("2-Getting the Host:" + CondecoHost);
                if (!String.IsNullOrEmpty(CondecoHost))
                {


                    UtilityManager.LogMessage("3-Getting the Post ID:" + postID);

                    UtilityManager.LogMessage("4-Releasing the com objects after getting ID: " + postID);
                    //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | 
                    //for spaces CRI 5549
                    bool permcheck = PermissionCheck(postID, UtilityManager.GetCurrentUserForPost());
                    //string permissionData = "iID=" + postID + "&iEmail=" + UtilityManager.GetCurrentUserForPost() + "&iSwitch=5";
                    //End Change
                    //UtilityManager.LogMessage("5a-checking user Permission wiht data: " + permissionData);
                    //permcheck = UtilityManager.PostDataToServer(permissionData);
                    //permcheck = permcheck.ToLower();
                    //UtilityManager.LogMessage("5b-User Permission check return: " + permcheck);
                    //if (permcheck.Equals("true") && !String.IsNullOrEmpty(postID) && currentItem.Size > 0 && !currentItem.Location.ToLower().Contains(" - cancelled"))
                    if (permcheck && !String.IsNullOrEmpty(postID) && currentItem.Size > 0 && !currentItem.Location.ToLower().Contains(" - cancelled"))
                    {
                        string fkBookingID = "";
                        string recurranceID = "";
                        string locationID = "";
                        DateTime bookingDate = new DateTime();
                        string delRecurring = currentItem.IsRecurring ? "1" : "0";


                        //string bookingData = currentItem.UserProperties.Find("CondecoBookingRecordSet", true).Value.ToString();

                        string individualEdit = "0";

                        if (currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || singleInstance)
                        {
                            individualEdit = "1";
                        }
                        StringReader sData = new StringReader(bookingData);
                        DataSet bookingDataSet = new DataSet();
                        bookingDataSet.ReadXml(sData);
                        bool isCrossDayBooking = false;
                        if (isCrossDayRecurranceBooking(currentItem))
                        {
                            isCrossDayBooking = true;
                            //meetingOriginalDate = UtilityManager.ConvertDateToTZ(meetingOriginalDate, Convert.ToInt32(bookingDataSet.Tables[6].Rows[0]["locationID"].ToString()), bookingDataSet.Tables[6].Rows[0]["OriginalTimeZone"].ToString());
                        }
                        bool isPastOccurrence = false;
                        foreach (DataRow dr in bookingDataSet.Tables[6].Rows)
                        {
                            if (singleInstance)
                            {
                                try
                                {
                                    //bookingDate = Convert.ToDateTime(dr["DateFrom"]);
                                    //Changed by Vineet Yadav regarding TP 20825 on 01/24/2014
                                    bookingDate = Convert.ToDateTime(dr["OldDateFrom"]);
                                    if (isCrossDayBooking)
                                    {
                                        bookingDate = UtilityManager.ConvertDateToLocalTZ(bookingDate, Convert.ToInt32(dr["locationID"].ToString()), dr["OriginalTimeZone"].ToString());
                                    }

                                    if (meetingOriginalDate.Date.CompareTo(bookingDate.Date) == 0)
                                    {
                                        BookingHelper bookingHelper = new BookingHelper();
                                        CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(currentItem, false);
                                        bookingHelper = null;
                                        long meetingTicks = bookingDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                                        DateTime currMeetingItemEnd = new DateTime(meetingTicks);
                                        DateTime condecoTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemEnd,
                                                                                                                     currMeeting.LocationID);

                                        if (condecoTimeInLocalTz < DateTime.Now)
                                        {
                                            isPastOccurrence = true;
                                            break;
                                        }

                                        if (!dr["deleteBooking"].ToString().Equals("1"))
                                        {
                                            fkBookingID = dr["bookingID"].ToString();
                                            locationID = dr["locationID"].ToString();
                                            if (currentItem.IsRecurring)
                                            {
                                                recurranceID = dr["fkRecurranceID"].ToString();
                                                UtilityManager.LogMessage("6b-Getting bookingID and Reccurrance ID for single meeting: " + fkBookingID + "::" + recurranceID);
                                            }
                                        }
                                        break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    UtilityManager.LogMessage("6bb-Exception occured " + ex.Message);
                                }
                            }
                            else
                            {
                                fkBookingID = dr["bookingID"].ToString();
                                locationID = dr["locationID"].ToString();
                                if (currentItem.IsRecurring)
                                    recurranceID = dr["fkRecurranceID"].ToString();
                                UtilityManager.LogMessage("6c-Getting bookingID and Reccurrance ID for Series meeting: " + fkBookingID + "::" + recurranceID);
                            }

                        }
                        if (recurranceID.Equals("") || individualEdit.Equals("1")) recurranceID = "0";

                        if (fkBookingID != "" && !isPastOccurrence)
                        {
                            string dateFormat = UtilityManager.CondecoDateFormat + " HH:mm";
                            string currentLocationDate = UtilityManager.ConvertDateToTZ(DateTime.Now, Convert.ToInt32(locationID)).ToString(dateFormat);
                            //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                            ////Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                            string dataToSent = "iID=" + postID + "&iEmail=" + UtilityManager.GetCurrentUserForPost() + "&iSwitch=4&IsRecurring=" + delRecurring + "&recurranceID=" + recurranceID + "&Indiv=" + individualEdit + "&fkbookingID=" + fkBookingID + "&locationCurrentDate=" + currentLocationDate.Replace(" ", "|") + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName;

                            //End Change
                            UtilityManager.LogMessage("7-creating Request for Delete with parm: " + dataToSent);
                            string result = UtilityManager.PostDataToServer(dataToSent);
                            // MessageBox.Show(result, Resources.Condeco_CancelledMeeting_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            //if condition added by chandra #18802-  created by chandra 27Oct13
                            if (result.StartsWith("-9"))
                            {
                                UtilityManager.ShowErrorMessage(CondecoResources.VcNotDeletedWithinAnHour);
                                //MessageBox.Show(CondecoResources.VcNotDeletedWithinAnHour, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                IsVCWithin1hour = true;
                                return;
                            }
                            //End- if condition added by chandra #18802-  created by chandra 27Oct13
                            UtilityManager.LogMessage("8-Delte response: " + result);
                        }
                        else
                        {
                            UtilityManager.LogMessage("9-No Booking ID Found");
                            //MessageBox.Show("No booking Found");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                UtilityManager.LogMessage("10-Exception occured while deleting booking dated:" + meetingOriginalDate + "::" + ex.Message);
            }
            finally
            {
                //if (currentItem != null && !singleInstance)
                // {
                //   UtilityManager.FreeCOMObject(currentItem);
                // currentItem = null;
                // }

            }
            UtilityManager.LogMessage("BookingHelper.DeleteSingleOccurrenceFromCondeco:*********Finished********");
        }
       public string GetBookingLocation(string postID, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.GetBookingLocation:*********Started********");
            string result = "";
            try
            {

                if (String.IsNullOrEmpty(postID))
                {
                    postID = appHelper.GetAppointmentPostID(appItem);
                }
                UtilityManager.LogMessage("BookingHelper.GetBookingLocation:Getting Condeco Meeting");
                CondecoMeeting cMeeting = GetCondecoMeeting(postID, appItem, true);
                UtilityManager.LogMessage("BookingHelper.GetBookingLocation: cMeeting.MainLocation ==" + cMeeting.MainLocation);
                // Added UtilityManager.DecodeSpecialCharacter by Paritosh ref: CRD-7237, To handle special character in Location 
                result = UtilityManager.DecodeSpecialCharacter(cMeeting.MainLocation);
                UtilityManager.LogMessage("BookingHelper.GetBookingLocation:Condeco Meeting Location found:" + result);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetBookingLocation" + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.GetBookingLocation:*********Finished********");
            return result;

        }

        public int GetBookingID(string postID, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.GetBookingID:*********Started********");
            int currentBookingID = 0;
            try
            {
                UtilityManager.LogMessage("BookingHelper.GetBookingID:Getting Condeco Meeting");
                CondecoMeeting cMeeting = GetCondecoMeeting(postID, appItem, false);
                currentBookingID = cMeeting.BookingID;
                UtilityManager.LogMessage("BookingHelper.GetBookingLocation:Condeco Booking ID found:" + currentBookingID);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetBookingID" + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.GetBookingID:*********Finished********");
            return currentBookingID;

        }

        public DataSet GetMeetingFromCondeco(string postID)
        {
            UtilityManager.LogMessage("BookingHelper.GetMeetingFromCondeco with just postid:*********Started********");
            DataSet bookingDataSet = new DataSet();
            try
            {
                UtilityManager.LogMessage("1-Inside GetMeetingFromCondeco for outlook post id:" + postID);
                // string permcheck = "false";
                string CondecoHost = UtilityManager.HostName;

                UtilityManager.LogMessage("2-Getting the Host:" + CondecoHost);
                if (!String.IsNullOrEmpty(CondecoHost))
                {

                    UtilityManager.LogMessage("3-Getting the Post ID:" + postID);


                    if (!String.IsNullOrEmpty(postID))
                    {

                        //string getBookingData = "iID=" + postID + "&iSwitch=7";
                        string getBookingData = "iID=" + postID + "&iSwitch=7&iEmail=" + UtilityManager.GetCurrentUserForPost() + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName;

                        UtilityManager.LogMessage("6a-Getting user booking Data with parm: " + getBookingData);
                        string bookingData = UtilityManager.PostDataToServer(getBookingData);
                        //CRD-7432 French character not supported in Subject title of appointment with single instance in recurrence series when SSO web authentication enabled.
                        StringReader sData;
                        if (UtilityManager.ConnectionMode() == 2)
                        {
                            sData = new StringReader(UtilityManager.DecodeSpecialCharacter(bookingData));
                        }
                        else
                        {
                            sData = new StringReader(bookingData);
                        }
                        bookingDataSet.ReadXml(sData);


                    }
                }
            }
            catch (Exception ex)
            {

                UtilityManager.LogMessage("10-Exception occured while deleting outlook post id:" + postID + "::" + ex.Message);
            }
            finally
            {


            }
            UtilityManager.LogMessage("GetMeetingFromCondeco with just postid:*********Finished********");
            return bookingDataSet;
        }

        public CondecoMeeting GetCondecoMeeting(string postID, Outlook.AppointmentItem appItem, bool loadFromDB)
        {
            UtilityManager.LogMessage("BookingHelper.GetMeetingFromCondeco with loadfromDB:*********Started********");
            CondecoMeeting cMeeting = new CondecoMeeting();
            DataSet condecoDataSet = this.GetCondecoDataSet(postID, appItem, loadFromDB);
            try
            {

                // }
                if (condecoDataSet.Tables.Count < 7) return new CondecoMeeting();
                if (condecoDataSet.Tables[6].Rows.Count == 0) return new CondecoMeeting();
                if (appItem == null) return new CondecoMeeting();
                int rowID = 0; // user for normal booking

                if (appItem.IsRecurring)
                {

                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        condecoDataSet.Tables[6].DefaultView.Sort = "BookingID desc";
                        DataTable dt = condecoDataSet.Tables[6].DefaultView.ToTable();
                        rowID = GetSeriesItem(dt.Rows);
                        cMeeting = this.PopulateCondecoMeeting(postID, dt.Rows[rowID]);
                    }
                    else
                    {
                        rowID = GetOccurrenceItem(condecoDataSet.Tables[6].Rows, appItem);

                        cMeeting = this.PopulateCondecoMeeting(postID, condecoDataSet.Tables[6].Rows[rowID]);
                    }


                }
                else
                {
                    cMeeting = this.PopulateCondecoMeeting(postID, condecoDataSet.Tables[6].Rows[rowID]);
                }


            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco meeting:" + postID + "::" + ex.Message);
            }

            UtilityManager.LogMessage("BookingHelper.GetCondecoMeeting with loadfromDB:*********Finished********");
            return cMeeting;



        }

        public CondecoMeeting GetCondecoSeriesMeeting(Outlook.AppointmentItem appItem, bool loadFromDB)
        {
            UtilityManager.LogMessage("BookingHelper.GetCondecoSeriesMeeting with loadfromDB:*********Started********");
            CondecoMeeting cMeeting = new CondecoMeeting();
            string postID = appHelper.GetAppointmentPostID(appItem);
            DataSet condecoDataSet = this.GetCondecoDataSet(postID, appItem, loadFromDB);
            try
            {


                // }
                if (condecoDataSet.Tables.Count < 7) return new CondecoMeeting();
                if (condecoDataSet.Tables[6].Rows.Count == 0) return new CondecoMeeting();
                if (appItem == null) return new CondecoMeeting();
                int rowID = 0; // user for normal booking

                condecoDataSet.Tables[6].DefaultView.Sort = "BookingID desc";
                DataTable dt = condecoDataSet.Tables[6].DefaultView.ToTable();
                rowID = GetSeriesItem(dt.Rows);
                cMeeting = this.PopulateCondecoMeeting(postID, dt.Rows[rowID]);
                //  cMeeting = this.PopulateCondecoMeeting(postID, condecoDataSet.Tables[6].Rows[rowID]);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco Series meeting:" + postID + "::" + ex.Message);
            }

            UtilityManager.LogMessage("BookingHelper.GetCondecoSeriesMeeting with loadfromDB:*********Finished********");
            return cMeeting;



        }


        public CondecoMeeting GetCondecoMeetingSingleOccurrence(string postID, Outlook.AppointmentItem appItem, bool loadFromDB, DateTime startDate)
        {
            UtilityManager.LogMessage("BookingHelper.GetCondecoMeetingSingleOccurrence with loadfromDB:*********Started********");
            CondecoMeeting cMeeting = new CondecoMeeting();


            DataSet condecoDataSet = this.GetCondecoDataSet(postID, appItem, loadFromDB);
            try
            {


                // }
                if (condecoDataSet.Tables.Count < 7) return new CondecoMeeting();
                if (condecoDataSet.Tables[6].Rows.Count == 0) return new CondecoMeeting();
                if (appItem == null) return new CondecoMeeting();
                int rowID = 0; // user for normal booking
                if (appItem.IsRecurring)
                {

                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        rowID = GetOccurrenceItemByDate(condecoDataSet.Tables[6].Rows, startDate);
                    }


                }

                cMeeting = this.PopulateCondecoMeeting(postID, condecoDataSet.Tables[6].Rows[rowID]);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco meeting:" + postID + "::" + ex.Message);
            }

            UtilityManager.LogMessage("BookingHelper.GetCondecoMeetingSingleOccurrence with loadfromDB:*********Finished********");
            return cMeeting;



        }

        private DataSet GetCondecoDataSet(string postID, Outlook.AppointmentItem appItem, bool loadFromDB)
        {
            UtilityManager.LogMessage("BookingHelper.GetCondecoDataSet with loadfromDB:*********Started********");
            DataSet condecoDataSet = new DataSet();
            try
            {
                Outlook.AppointmentItem currentItem = null;
                //Changed by Anand : Now Setting the currentItem to parent if its occurance or exception instead of checking loadFromDB.
                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    currentItem = appItem.Parent as Outlook.AppointmentItem;
                else
                    currentItem = appItem;
                //End Change


                bool dataFound = false;
                if (!loadFromDB)
                {
                    UtilityManager.LogMessage("BookingHelper.GetCondecoDataSet with loadfromDB:false");
                    string dataSetXml = "";

                    //Changed by Anand : Now Setting the currentItem to parent if its occurance or exception instead of checking loadFromDB.
                    //if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    //{
                    //    currentItem = appItem.Parent as Outlook.AppointmentItem;

                    //}
                    //else
                    //{
                    //    currentItem = appItem;
                    //}
                    //End Change

                    dataSetXml = appHelper.GetAppointmentRecord(currentItem);

                    UtilityManager.LogMessage("BookingHelper.GetCondecoDataSet with loadfromDB:dataSetXML:" + dataSetXml);
                    if (!String.IsNullOrEmpty(dataSetXml))
                    {
                        try
                        {
                            StringReader sData = new StringReader(dataSetXml);
                            condecoDataSet.ReadXml(sData);
                            if (condecoDataSet.Tables.Count < 7)
                            {
                                dataFound = false;
                            }
                            else
                            {
                                if (condecoDataSet.Tables[6].Rows.Count > 0)
                                    dataFound = true;
                            }
                        }
                        catch
                        {
                        }

                    }
                }

                //Get from DataBase and store it into User Property
                bool userPropertyUpdated = false;
                if (!dataFound)
                {

                    UtilityManager.LogMessage(
                        "BookingHelper.GetCondecoDataSet with loadfromDB:dataSetXML: No DataFound load from DB");
                    condecoDataSet = this.GetMeetingFromCondeco(postID);
                    if (condecoDataSet.Tables.Count < 7)
                    {
                        dataFound = false;
                    }
                    else
                    {
                        if (condecoDataSet.Tables[6].Rows.Count > 0)
                        {
                            UtilityManager.LogMessage(
                                "BookingHelper.GetCondecoDataSet with loadfromDB:dataSetXML: Saving Appointment Record");
                            bool updated = appHelper.SaveAppointmentRecord(currentItem, condecoDataSet.GetXml());
                            if (updated)
                            {
                                UtilityManager.LogMessage(
                              "BookingHelper.GetCondecoDataSet with loadfromDB:dataSetXML: Saving Appointment Record updated=" + updated);
                                dataFound = true;
                                userPropertyUpdated = true;
                            }
                        }
                    }

                }

                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                {
                    if (currentItem != null && userPropertyUpdated)
                    {
                        UtilityManager.LogMessage(
                             "BookingHelper.GetCondecoDataSet with loadfromDB:dataSetXML: Releasing Appointment Record:: userPropertyUpdated=" + userPropertyUpdated);
                      //  currentItem.Save();


                    }
                    UtilityManager.FreeCOMObject(currentItem);
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("error ocuured while geting Condeco Data Set" + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.GetCondecoDataSet with loadfromDB:*********Finished********");




            return condecoDataSet;
        }


  
        private int GetSeriesItem(DataRowCollection drCol)
        {
            UtilityManager.LogMessage("BookingHelper.GetSeriesItem:*********Started********");
            int rowIndex = 0;
            int i = 0;
            foreach (DataRow dr in drCol)
            {
                //ToDO: change this

                bool edited = false;
                bool deleted = false;
                bool seriesDeleted = false;
                bool seriesRejected = false;
                try
                {
                    if (dr["Edited"].ToString().Equals("1"))
                    {
                        edited = true;
                    }
                }
                catch
                {
                }

                try
                {
                    if (dr["deleteBooking"].ToString().Equals("1"))
                    {
                        deleted = true;
                    }
                }
                catch
                {
                }

                try
                {
                    if (dr["isDeleted"].ToString().Equals("1"))
                    {
                        seriesDeleted = true;
                    }
                }
                catch
                {
                }
                try
                {
                    if (dr["isRejected"].ToString().Equals("1"))
                    {
                        seriesRejected = true;
                    }
                }
                catch
                {
                }
                if (i > 0)
                    rowIndex++;
                if ((!edited && !deleted) || (seriesDeleted || seriesRejected))
                {
                    return rowIndex;
                    // break;
                }

                i++;
            }
            UtilityManager.LogMessage("BookingHelper.GetSeriesItem: Series Item Found with Index" + rowIndex);
            UtilityManager.LogMessage("BookingHelper.GetSeriesItem:*********Finished********");
            return rowIndex;
        }

        private int GetOccurrenceItem(DataRowCollection drCol, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.GetOccurrenceItem:*********Started********");

            int result = 0;
            //UserProperties uProps = appItem.UserProperties;
            //UserProperty refProp = uProps.Find("CondecoReference", true);
            ////Outlook.UserProperty refProp = uProps.Find("CondecoReference", true);

            //string condecoRef = "";
            //if (refProp != null)
            //{
            //    condecoRef = refProp.Value.ToString();
            //    UtilityManager.FreeCOMObject(refProp);
            //}
            //UtilityManager.FreeCOMObject(uProps);
            int bookingId = AppointmentDataInfo.GetBookingId(appItem);

            string condecoRef = (bookingId > 0) ? bookingId.ToString() : string.Empty;

            int rowIndex = 0;
            bool bookingFound = false;
            foreach (DataRow dr in drCol)
            {
                string currentItemID = "";
                if (String.IsNullOrEmpty(currentItemID))
                {
                    currentItemID = appHelper.GetAppointmentItemID(appItem, Convert.ToInt32(dr["locationID"].ToString()));
                }
                bool itemFound = false;
                if (!String.IsNullOrEmpty(currentItemID) && String.IsNullOrEmpty(condecoRef))
                {
                    DateTime condecoItemDate;
                    DateTime.TryParse(dr["DateTo"].ToString(), out condecoItemDate);
                    DateTime currentItemDate = appHelper.GetAppointmentItemDate(appItem, Convert.ToInt32(dr["locationID"].ToString()));

                    if (condecoItemDate.Date.CompareTo(currentItemDate.Date) == 0)
                        itemFound = true;
                }

                if (dr["BookingID"].ToString().ToLower().Equals(condecoRef) || dr["ItemID"].ToString().Equals(currentItemID) || itemFound)
                {
                    result = rowIndex;
                    bookingFound = true;
                    // break;
                }
                rowIndex++;
            }
            if (!bookingFound)
                result = 0;
            UtilityManager.LogMessage("BookingHelper.GetSeriesItem: Occurrence Item Found with Index" + result);
            UtilityManager.LogMessage("BookingHelper.GetOccurrenceItem:*********Finished********");
            return result;

        }

        private int GetOccurrenceItemByDate(DataRowCollection drCol, DateTime startDate)
        {
            UtilityManager.LogMessage("BookingHelper.GetOccurrenceItemByDate:*********Started********");
            int result = 0;
            int rowIndex = 0;
            bool bookingFound = false;
            string dateFormat = UtilityManager.CondecoDateFormat + " HH:mm:ss";
            foreach (DataRow dr in drCol)
            {
                DateTime itemDate = DateTime.MinValue;
                try
                {

                    //itemDate = DateTime.ParseExact(dr["DateFrom"].ToString(), dateFormat, CultureInfo.InvariantCulture);
                    DateTime.TryParse(dr["DateFrom"].ToString(), out itemDate);
                }
                catch
                {
                }

                if (itemDate != DateTime.MinValue)
                {
                    Int32 currentLoctaionID;
                    Int32.TryParse(dr["locationID"].ToString(), out currentLoctaionID);
                    DateTime appStartDate = UtilityManager.ConvertDateToTZ(startDate, currentLoctaionID);
                    if (itemDate.Date.CompareTo(appStartDate.Date) == 0)
                    {
                        result = rowIndex;
                        bookingFound = true;
                        break;
                    }
                }
                rowIndex++;
            }
            if (!bookingFound)
                result = 0;
            UtilityManager.LogMessage("BookingHelper.GetOccurrenceItemByDate: Occurrence Item Found with Index" + result);
            UtilityManager.LogMessage("BookingHelper.GetOccurrenceItemByDate:*********Finished********");
            return result;

        }

        public CondecoMeeting PopulateCondecoMeeting(string postID, DataRow dr)
        {
            UtilityManager.LogMessage("BookingHelper.PopulateCondecoMeeting:*********Started********PostID:" + postID);
            CondecoMeeting currentMeeting = new CondecoMeeting();

            try
            {
                currentMeeting.PostID = postID;
                currentMeeting.BookingID = Convert.ToInt32(dr["BookingID"].ToString());
                int intResult = 0;
                try
                {
                    if (dr.Table.Columns.Contains("fkRecurranceID"))
                    {
                        Int32.TryParse(dr["fkRecurranceID"].ToString(), out intResult);
                        currentMeeting.FkRecurrenceID = intResult;
                    }
                }
                catch { }

                currentMeeting.AllLocation = String.IsNullOrEmpty(dr["AllLocations"].ToString()) ? String.Empty : dr["AllLocations"].ToString();
                currentMeeting.MainLocation = String.IsNullOrEmpty(dr["MainLocation"].ToString()) ? String.Empty : dr["MainLocation"].ToString();
                bool result = false;
                Boolean.TryParse(dr["approved"].ToString(), out result);
                currentMeeting.Approved = result;
                //Boolean.TryParse(dr["deployment"].ToString(),out result);
                currentMeeting.Deployment = (dr["deployment"].ToString() == "1");
                try
                {
                    if (dr.Table.Columns.Contains("Edited"))
                    {
                        currentMeeting.Edited = (dr["Edited"].ToString() == "1");
                    }
                }
                catch { }

                DateTime dateResult;
                DateTime.TryParse(dr["DateFrom"].ToString(), out dateResult);
                currentMeeting.DateFrom = dateResult;
                DateTime.TryParse(dr["TimeFrom"].ToString(), out dateResult);
                currentMeeting.TimeFrom = dateResult;
                DateTime.TryParse(dr["DateTo"].ToString(), out dateResult);
                currentMeeting.DateTo = dateResult;
                DateTime.TryParse(dr["TimeTo"].ToString(), out dateResult);
                currentMeeting.TimeTo = dateResult;
                currentMeeting.ItemID = String.IsNullOrEmpty(dr["ItemID"].ToString()) ? String.Empty : dr["ItemID"].ToString();

                Int32.TryParse(dr["resourceItemID"].ToString(), out intResult);
                currentMeeting.ResourceItemID = intResult;
                currentMeeting.MeetingTitle = String.IsNullOrEmpty(dr["MeetingTitle"].ToString()) ? String.Empty : dr["MeetingTitle"].ToString();
                //Support & and 's in subject title 
                currentMeeting.MeetingTitle = currentMeeting.MeetingTitle.Replace("&#39;", "'").Replace("&#38;", "&");

                DateTime.TryParse(dr["RealStartTime"].ToString(), out dateResult);
                currentMeeting.RealStartTime = dateResult;
                DateTime.TryParse(dr["RealEndTime"].ToString(), out dateResult);
                currentMeeting.RealEndTime = dateResult;
                Int32.TryParse(dr["locationID"].ToString(), out intResult);
                currentMeeting.LocationID = intResult;
                //Boolean.TryParse(dr["deleteBooking"].ToString(),out result);
                currentMeeting.DeleteBooking = (dr["deleteBooking"].ToString() == "1");
                currentMeeting.OutlookID = String.IsNullOrEmpty(dr["OutlookID"].ToString()) ? String.Empty : dr["OutlookID"].ToString();
                Int32.TryParse(dr["cancelBeforeDays"].ToString(), out intResult);
                currentMeeting.CancelBeforeDays = intResult;
                try
                {
                    DateTime.TryParse(dr["cancelBeforeTime"].ToString(), out dateResult);
                    currentMeeting.CancelBeforeTime = dateResult;
                }
                catch (Exception)
                {
                }

                DateTime.TryParse(dr["businessDayStart"].ToString(), out dateResult);
                currentMeeting.BusinessDayStart = dateResult;
                //Boolean.TryParse(dr["bookingRejected"].ToString(),out result);
                currentMeeting.BookingRejected = Convert.ToBoolean(dr["bookingRejected"]);
                // Boolean.TryParse(dr["isRejected"].ToString(),out result);
                currentMeeting.IsSeriesRejected = (dr["isRejected"].ToString() == "1");
                //  Boolean.TryParse(dr["isDeleted"].ToString(),out result);
                currentMeeting.IsSeriesDeleted = (dr["isDeleted"].ToString() == "1");
                try
                {
                    if (dr.Table.Columns.Contains("OriginalTimeZone"))
                    {
                        currentMeeting.OriginalTZ = dr["OriginalTimeZone"].ToString();
                    }
                    if (dr.Table.Columns.Contains("locationTimeZone"))
                    {
                        currentMeeting.LocationTZ = dr["locationTimeZone"].ToString();
                    }

                }
                catch (Exception ex)
                {

                    UtilityManager.LogMessage("Erro occured while populating OriginalTZ " + ex.Message);
                }

                try
                {
                    DateTime.TryParse(dr["oldDateFrom"].ToString(), out dateResult);
                    currentMeeting.OriginalDateFrom = dateResult;
                    DateTime.TryParse(dr["oldTimeFrom"].ToString(), out dateResult);
                    currentMeeting.OriginalTimeFrom = dateResult;
                    DateTime.TryParse(dr["oldDateTo"].ToString(), out dateResult);
                    currentMeeting.OriginalDateTo = dateResult;
                    if (dr.Table.Columns.Contains("oldTimeTo"))
                    {
                        DateTime.TryParse(dr["oldTimeTo"].ToString(), out dateResult);
                        currentMeeting.OriginalTimeTo = dateResult;
                    }

                    try
                    {
                        currentMeeting.IsBookingHasDST = Convert.ToBoolean(dr["IsDST"]);
                    }
                    catch { }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("Exception occured while populating OldDateTime " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Exception occured while populating Condeco Meeting " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.PopulateCondecoMeeting:*********Finished********PostID:" + postID);
            return currentMeeting;
        }

        public string GetOutlookEntryDatatoPost(string postId, string outlookEntryID, string itemID)
        {
            // xmlhttp.Send "iSubject=" & REPLACE(iSubject, " ", "|") & "&iFrom=" & REPLACE(iFrom, " ", "|") & "&iTo=" & REPLACE(iTo, " ", "|") & "&iAttendee=" & REPLACE(iAttendee, " ", "|") & "&iRecDates=" &iDatePass& "&iEmail=" & REPLACE(thisEmail, " ", "|") & "&iSwitch=1&currentTZ="& REPLACE(iCurrentTZ, " ", "|")       
            // iID = xmlhttp.ResponseText


            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append("iID=" + postId);
                sb.Append("&iOutlookID=" + outlookEntryID);
                sb.Append("&iItemID=" + itemID);
                sb.Append("&iSwitch=6");
            }
            catch
            {
            }


            return sb.ToString();
        }

        //    public string AddCondecoReference(AppointmentItem appItem)
        //    {
        //        string condecoRef = "";
        //        UserProperty refProp = null;
        //        UserProperty postProp = null;
        //        UserProperties uProps = null;
        //        try
        //        {
        //            if (appItem.IsRecurring && appItem.RecurrenceState != OlRecurrenceState.olApptMaster)
        //            {
        //                string postDataID = appHelper.GetAppointmentPostID(appItem); ;
        //                //Changed below by Anand on 23-Jan-13 for TP10224
        //                if (!string.IsNullOrEmpty(postDataID))
        //                {
        //                    uProps = appItem.UserProperties;
        //                    refProp = uProps.Find("CondecoReference", true);

        //                    CondecoMeeting currMeeting = GetCondecoMeeting(postDataID, appItem, false);
        //                    if (refProp == null)
        //                    {
        //                        //  string dataToPost = this.GetOutlookEntryDatatoPost(postDataID, condecoRef, currentItemID);
        //                        //  UtilityManager.PostDataToServer(dataToPost);
        //                        refProp = uProps.Add("CondecoReference", OlUserPropertyType.olText, false, false);
        //                    }
        //                    if (currMeeting.BookingID > 0)
        //                        refProp.Value = currMeeting.BookingID;
        //                }
        //                //End Change by Anand for TP10224
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            UtilityManager.LogMessage("AddCondecoReference " + ex.Message);
        //        }
        //        finally
        //        {
        //            UtilityManager.FreeCOMObject(postProp);
        //            UtilityManager.FreeCOMObject(refProp);
        //            UtilityManager.FreeCOMObject(uProps);

        //        }

        //        return condecoRef;
        //}

        public string GetBookingStatusDatatoPost(string postId)
        {


            StringBuilder sb = new StringBuilder();

            sb.Append("iID=" + postId);
            sb.Append("&iSwitch=9");



            return sb.ToString();
        }

        public string GetBookingMovedDatatoPost(int bookingID, DateTime startDate, DateTime endDate, string subject, Outlook.AppointmentItem appItem)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string dateFormat = UtilityManager.CondecoDateFormat + " HH:mm";
                int individualyEdited = 0;
                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence)
                {
                    individualyEdited = 1;
                }
                // sb.Append("iID=" + postId);            
                sb.Append("iFrom=" + startDate.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iTo=" + endDate.ToString(dateFormat).Replace(" ", "|"));

                if (UtilityManager.ConnectionMode() == 2)
                {
                    sb.Append("&iSubject=" + HttpUtility.UrlEncode(subject.Replace(" ", "|").Replace("&", "^")));
                }
                else
                {
                    sb.Append("&iSubject=" + subject.Replace(" ", "|").Replace("&", "^"));
                }
                // sb.Append("&iSubject=" + subject.Replace(" ", "|").Replace("&", "^"));

                sb.Append("&fkBookingID=" + bookingID);
                sb.Append("&individualyEdited=" + individualyEdited);
                //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                sb.Append("&iEmail=" + UtilityManager.GetCurrentUserForPost());
                //End Change
                //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                sb.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                sb.Append("&ssoUser=" + UtilityManager.SSOUserName);
                sb.Append("&iSwitch=10");
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("GetBookingMovedDatatoPost " + ex.Message);
            }


            return sb.ToString();
        }

        public bool IsCondecoBookingSaved(Outlook.AppointmentItem appItem)
        {
            bool isCondecoBookingSaved = true;
            try
            {
                string postID = appHelper.GetAppointmentPostID(appItem);
                string dataToPost = this.GetBookingStatusDatatoPost(postID);
                string result = UtilityManager.PostDataToServer(dataToPost);

                isCondecoBookingSaved = Convert.ToBoolean(result.ToLower());
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("IsCondecoBookingSaved " + ex.Message);
            }
            return isCondecoBookingSaved;
        }

        public bool IsCondecoBooking(Outlook.AppointmentItem appItem)
        {
            bool result = false;
            try
            {
                //Changed by Anand : Returning as received mails should not update the booking. TP 12183
                UtilityManager.LogMessage("1-Checking meeting is not olMeetingReceived");
                if (appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived) return false;
                //End Change

                string postID = appHelper.GetAppointmentPostID(appItem);
                UtilityManager.LogMessage("BookingHelper.IsCondecoBooking:*********Started********PostID:" + postID);
                if (String.IsNullOrEmpty(postID)) return false;
                CondecoMeeting currMeeting = this.GetCondecoMeeting(postID, appItem, false);

                if (currMeeting != null)
                {
                    if (currMeeting.BookingID > 0 && !currMeeting.DeleteBooking && !currMeeting.BookingRejected && (!appItem.IsRecurring || (!currMeeting.IsSeriesDeleted && !currMeeting.IsSeriesRejected)))
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("IsCondecoBooking " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.IsCondecoBooking:*********Finished********Result:" + result);
            return result;
        }

        public bool IsCondecoBookingV2(Outlook.AppointmentItem appItem)
        {
            bool result = false;
            try
            {
                string postID = appHelper.GetAppointmentPostID(appItem);
                UtilityManager.LogMessage("BookingHelper.IsCondecoBookingV2:*********Started********PostID:" + postID);
                if (String.IsNullOrEmpty(postID)) return false;
                CondecoMeeting currMeeting = this.GetCondecoMeeting(postID, appItem, false);

                if (currMeeting != null)
                {
                    if ((appItem.IsRecurring) && (currMeeting.IsSeriesDeleted || currMeeting.IsSeriesRejected))
                    {
                        result = false;
                    }
                    else if (currMeeting.BookingID > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("IsCondecoBooking " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.IsCondecoBookingV2:*********Finished********Result:" + result);
            return result;
        }

        public int UpdateCondecoBooking(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            int result = -100;
            try
            {
                UtilityManager.LogMessage("BookingHelper.UpdateCondecoBooking:*********Started********Appointmetn Start:" + appItem.Start);
                bool havingPermission = PermissionCheck(currMeeting.PostID, UtilityManager.GetCurrentUserForPost());
                if (!havingPermission) return -999;
                //string postID = appHelper.GetAppointmentPostID(appItem);
                DateTime appStart = UtilityManager.ConvertDateToTZ(appItem.Start, currMeeting.LocationID);
                DateTime appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
                string appSubject = appItem.Subject;
                /* if (!string.IsNullOrEmpty(appSubject))
                 {
                     appSubject.Replace("'", "&#39;").Replace("&","&#38");
                 }*/
                string dataToPost = this.GetBookingMovedDatatoPost(currMeeting.BookingID, appStart, appEnd, appSubject, appItem);
                UtilityManager.LogMessage("BookingHelper.UpdateCondecoBooking:Posting Data:" + dataToPost);
                string response = UtilityManager.PostDataToServer(dataToPost);
                UtilityManager.LogMessage("BookingHelper.UpdateCondecoBooking:Response:" + response);
                result = Convert.ToInt32(response);

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("IsCondecoBookingSaved " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.UpdateCondecoBooking:*********Finished********");
            return result;
        }

        public CondecoMeeting GetCurrentCondecoMeeting(Outlook.AppointmentItem appItem)
        {
            CondecoMeeting currMeeting = new CondecoMeeting();
            try
            {
                string postID = appHelper.GetAppointmentPostID(appItem);
                currMeeting = this.GetCondecoMeeting(postID, appItem, false);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetCurrentCondecoMeeting" + ex.Message);
            }

            return currMeeting;
        }

        public CondecoMeeting GetCurrentCondecoMeeting(Outlook.AppointmentItem appItem, bool latestFromDB)
        {
            CondecoMeeting currMeeting = new CondecoMeeting();
            try
            {
                string postID = appHelper.GetAppointmentPostID(appItem);
                currMeeting = this.GetCondecoMeeting(postID, appItem, latestFromDB);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetCurrentCondecoMeeting with reload from db" + ex.Message);
            }

            return currMeeting;
        }

        public List<CondecoMeeting> GetIndividuallyEditedItems(Outlook.AppointmentItem appItem)
        {
            List<CondecoMeeting> editedMeetings = new List<CondecoMeeting>();
            string postID = appHelper.GetAppointmentPostID(appItem);
            DataSet condecoData = this.GetMeetingFromCondeco(postID);
            try
            {


                // }
                if (condecoData.Tables.Count < 7) return editedMeetings;
                if (condecoData.Tables[6].Rows.Count == 0) return editedMeetings;
                if (appItem == null) return editedMeetings;
                int rowID = 0; // user for normal booking

                foreach (DataRow dr in condecoData.Tables[6].Rows)
                {
                    //&& (dr["isDeleted"].ToString().Equals("0") && dr["isRejected"].ToString().Equals("0"))
                    if ((dr["Edited"].ToString().Equals("1") || dr["deleteBooking"].ToString().Equals("1")))
                    {
                        CondecoMeeting cMeeting = this.PopulateCondecoMeeting(postID, condecoData.Tables[6].Rows[rowID]);
                        editedMeetings.Add(cMeeting);
                    }
                    rowID++;
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco meeting:" + postID + "::" + ex.Message);
            }
            return editedMeetings;


        }

        public void DeleteCondecoBooking(Outlook.AppointmentItem currentItem, AddinModule CurrentAddinModule = null)
        {
            HandleBookingDelete(currentItem, false, CurrentAddinModule);
        }
        public void DeleteCondecoBooking(Outlook.AppointmentItem currentItem, bool isSeriesDelete)
        {
            HandleBookingDelete(currentItem, isSeriesDelete);
        }
        public List<CondecoMeeting> GetIndividuallyEditedAndMissingItems(Outlook.AppointmentItem appItem, ref List<DateTime> missingDates, ref int locationID)
        {
            List<CondecoMeeting> editedMeetings = new List<CondecoMeeting>();
            List<DateTime> meetingDates = new List<DateTime>();
            string postID = appHelper.GetAppointmentPostID(appItem);
            DataSet condecoData = this.GetMeetingFromCondeco(postID);
            bool isCrossDayBooking = false;
            try
            {


                // }
                if (condecoData.Tables.Count < 7) return editedMeetings;
                if (condecoData.Tables[6].Rows.Count == 0) return editedMeetings;
                if (appItem == null) return editedMeetings;
                int rowID = 0; // user for normal booking
                CondecoMeeting currMeeting1 = GetCurrentCondecoMeeting(appItem, false);
                if (isCrossDayRecurranceBooking(appItem))
                {
                    isCrossDayBooking = true;
                }


                foreach (DataRow dr in condecoData.Tables[6].Rows)
                {
                    //&& (dr["isDeleted"].ToString().Equals("0") && dr["isRejected"].ToString().Equals("0"))
                    DataRow currentRow = condecoData.Tables[6].Rows[rowID];
                    DateTime dateResult = DateTime.MinValue;
                    DateTime.TryParse(dr["DateFrom"].ToString(), out dateResult);

                    if (dateResult != DateTime.MinValue)
                    {
                        DateTime dt;
                        if (isCrossDayBooking)
                        {
                            dt = UtilityManager.ConvertDateToLocalTZ(dateResult, currMeeting1.LocationID);
                            meetingDates.Add(dt.Date);
                        }
                        else
                        {
                            meetingDates.Add(dateResult.Date);
                        }
                    }
                    //    meetingDates.Add(dateResult.Date);

                    if (locationID == 0)
                        Int32.TryParse(dr["locationID"].ToString(), out locationID);
                    if ((dr["Edited"].ToString().Equals("1") || dr["deleteBooking"].ToString().Equals("1")))
                    {
                        CondecoMeeting cMeeting = this.PopulateCondecoMeeting(postID, condecoData.Tables[6].Rows[rowID]);
                        editedMeetings.Add(cMeeting);
                    }
                    rowID++;
                }
                if (meetingDates.Count > 0)
                {
                    string appRecDates = appHelper.GetAppointmentRecurrenceDates(appItem);
                    List<DateTime> recDatesList = new List<DateTime>();
                    if (!string.IsNullOrEmpty(appRecDates))
                    {
                        string[] recDates = appRecDates.Split(',');
                        foreach (string strDate in recDates)
                        {
                            recDatesList.Add(Convert.ToDateTime((strDate)));
                        }
                    }
                    foreach (var recDate in recDatesList)
                    {
                        if (!meetingDates.Contains(recDate.Date))
                            missingDates.Add(recDate);
                    }

                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco meeting:" + postID + "::" + ex.Message);
            }
            return editedMeetings;
        }

        
        public bool IsNonCondecoOccurrence(Outlook.AppointmentItem appItem)
        {
            bool result = false;
            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring)
            {
                return result;
            }
            List<DateTime> meetingDates = new List<DateTime>();
            string postID = appHelper.GetAppointmentPostID(appItem);
            DataSet condecoData = GetCondecoDataSet(postID, appItem, false);
            try
            {


                // }
                if (condecoData.Tables.Count < 7) return result;
                if (condecoData.Tables[6].Rows.Count == 0) return result;
                if (appItem == null) return result;
                int rowID = 0; // user for normal booking


                foreach (DataRow dr in condecoData.Tables[6].Rows)
                {
                    //&& (dr["isDeleted"].ToString().Equals("0") && dr["isRejected"].ToString().Equals("0"))
                    DataRow currentRow = condecoData.Tables[6].Rows[rowID];
                    DateTime dateResult = DateTime.MinValue;

                    DateTime.TryParse(dr["DateFrom"].ToString(), out dateResult);
                    if (dateResult != DateTime.MinValue)
                        meetingDates.Add(dateResult.Date);
                    rowID++;
                }
                if (meetingDates.Count > 0)
                {
                    //Change by Anand on 30-Jan-2013 for 9508/10547/10548/10663
                    //If moving first occurance date back or last occurance date forward then it should not check for that

                    //**belows lines Added by Paritosh Mishra to fix issue -TP 23071	On opening any single occurrence � except the first � in a weekly recurrence booking, a message No room booking is associated with this occurrence as there was no room available when this appointment was created appears.
                    DateTime? appointmentDate;
                    DateTime dtToDestinationTZ = DateTime.MinValue;
                    CondecoMeeting currMeeting = this.GetCurrentCondecoMeeting(appItem, false);
                    if (currMeeting.LocationID > 0)
                    {
                        dtToDestinationTZ = AppointmentDataInfo.GetOriginalStartDate(appItem) ?? DateTime.MinValue;
                    }
                    if (dtToDestinationTZ == DateTime.MinValue)
                    {
                        appointmentDate = null;
                    }
                    else
                    {
                        appointmentDate = UtilityManager.ConvertDateToTZ(dtToDestinationTZ, currMeeting.LocationID);
                    }
                    //**End 
                    //   DateTime? appointmentDate = AppointmentDataInfo.GetOriginalStartDate(appItem);
                    meetingDates.Sort();
                    //Edited by Ravi Goyal for PRB0040163 (CRD-7585)
                    if (appointmentDate != null)
                    {
                        if (appointmentDate >= meetingDates[0] && appointmentDate <= meetingDates[meetingDates.Count - 1])
                        {
                            if (!meetingDates.Contains((appointmentDate ?? appItem.Start).Date))
                                result = true;
                        }
                    }
                    else
                    {
                        if (!meetingDates.Contains((appItem.Start).Date))
                            result = true;
                    }
                    //end edited by Ravi Goyal for PRB0040163 (CRD-7585)
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get Condeco meeting:" + postID + "::" + ex.Message);
            }
            return result;


        }
        private void HandleBookingDelete(Outlook.AppointmentItem currentItem, bool isSeriesDelete, AddinModule CurrentAddinModule = null)
        {
            UtilityManager.LogMessage("BookingHelper.DeleteCondecoBooking:*********Started********");
            try
            {
                //Changed by Anand : Returning as received mails should not cancel the booking. CRI 5451
                UtilityManager.LogMessage("1-Checking meeting is not olMeetingReceived");
                if (currentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingReceived) return;
                //End Change

                UtilityManager.LogMessage("2-Inside DeleteMeetingFromCondeco for booking dated:" + currentItem.Start);
                bool permCheck = false;
                CondecoMeeting cMeeting = this.GetCurrentCondecoMeeting(currentItem, true);
                string postID = cMeeting.PostID;
                string dateFormat = UtilityManager.CondecoDateFormat + " HH:mm";
                string currentLocationDate = UtilityManager.ConvertDateToTZ(DateTime.Now, cMeeting.LocationID).ToString(dateFormat);
                //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                permCheck = PermissionCheck(postID, UtilityManager.GetCurrentUserForPost());
                //End Change
                UtilityManager.LogMessage("5b-User Permission check return: " + permCheck);
                if (permCheck && !String.IsNullOrEmpty(postID) && !currentItem.Location.ToLower().Contains(" - cancelled"))
                {
                    //string fkBookingID = "";
                    string recurranceID = cMeeting.FkRecurrenceID.ToString();
                    string delRecurring;
                    if (isSeriesDelete)
                        delRecurring = "1";
                    else
                        delRecurring = currentItem.IsRecurring ? "1" : "0";
                    string individualEdit = "0";

                    if (currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || currentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    {
                        individualEdit = "1";

                    }
                    if (recurranceID.Equals("") || individualEdit.Equals("1")) recurranceID = "0";

                    if (cMeeting.BookingID > 0 && !cMeeting.DeleteBooking)
                    {
                        //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                        //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                        string dataToSent = "iID=" + postID + "&iEmail=" + UtilityManager.GetCurrentUserForPost() + "&iSwitch=4&IsRecurring =" + delRecurring + "&recurranceID=" + recurranceID + "&Indiv=" + individualEdit + "&fkbookingID=" + cMeeting.BookingID + "&locationCurrentDate=" + currentLocationDate.Replace(" ", "|") + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName;
                        //End Change
                        UtilityManager.LogMessage("7-creating Request for Delete with parm: " + dataToSent);
                        string result = UtilityManager.PostDataToServer(dataToSent);
                        if (result.StartsWith("-9"))
                        {

                            MessageBox.Show(CondecoResources.VcNotDeletedWithinAnHour, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            IsVCWithin1hour = true;
                            return;
                        }
                        else if (result.StartsWith("-1") || result.StartsWith("-2"))
                        {
                            UtilityManager.LogMessage("8-Delte response: " + result);
                        }
                        else
                        {
                            //Fixed issue #16403. #16405 reported in TP 
                            // MessageBox.Show(result, CondecoResources.Condeco_CancelledMeeting_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (currentItem.IsRecurring)
                            {
                                //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                                //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
                                UtilityManager.LogMessage("BookingHelper.DeleteCondecoBooking:***Appointment is recurring, SyncIndiviudallyEditedItems Started***");
                                SyncManager sync = new SyncManager();
                                sync.SyncIndiviudallyEditedItems(currentItem, CurrentAddinModule, false, true);
                                sync = null;
                                UtilityManager.LogMessage("BookingHelper.DeleteCondecoBooking:***Sync SyncIndiviudallyEditedItems Completed***");
                            }
                            else
                            {
                                UtilityManager.LogMessage("BookingHelper.DeleteCondecoBooking:***Appointment is not recurring , SyncIndiviudallyEditedItems not required***");
                            }
                            MessageBox.Show(CondecoResources.Condeco_CancelledMeeting_Message, CondecoResources.Condeco_CancelledMeeting_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        UtilityManager.LogMessage("8-Delte response: " + result);
                    }
                    else
                    {
                        UtilityManager.LogMessage("9-No Booking ID Found");
                        //MessageBox.Show("No booking Found");
                    }
                }

            }
            catch (Exception ex)
            {

                UtilityManager.LogMessage("10-Exception occured while deleting booking dated:" + currentItem.Start + "::" + ex.Message);
            }
            finally
            {
                //if (currentItem != null && !singleInstance)
                // {
                //   UtilityManager.FreeCOMObject(currentItem);
                // currentItem = null;
                // }

            }
            UtilityManager.LogMessage("BookingHelper.DeleteCondecoBooking:*********Finished********");
        }

        public bool PermissionCheck(string postID, string userName)
        {
            string permcheck = "";
            bool result = false;
            try
            {

                // DelegateCalenderProcess 
                //result = true;
                ////////////Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                string permissionData = "iID=" + postID + "&iEmail=" + userName + "&iSwitch=5" + "&thisDomain=" + UtilityManager.GetUserDomainName() + "&ssoUser=" + UtilityManager.SSOUserName;
                UtilityManager.LogMessage("5a-checking user Permission wiht data: " + permissionData);
                permcheck = UtilityManager.PostDataToServer(permissionData);
                if (!String.IsNullOrEmpty(permcheck))
                    result = Convert.ToBoolean(permcheck.ToLower());
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("PermissionCheck " + ex.Message);
            }
            return result;

        }

        private string GetNewBookingDatatoPost(Outlook.AppointmentItem appItem)
        {
            // xmlhttp.Send "iSubject=" & REPLACE(iSubject, " ", "|") & "&iFrom=" & REPLACE(iFrom, " ", "|") & "&iTo=" & REPLACE(iTo, " ", "|") & "&iAttendee=" & REPLACE(iAttendee, " ", "|") & "&iRecDates=" &iDatePass& "&iEmail=" & REPLACE(thisEmail, " ", "|") & "&iSwitch=1&currentTZ="& REPLACE(iCurrentTZ, " ", "|")       
            // iID = xmlhttp.ResponseText
            UtilityManager.LogMessage("BookingHelper.GetNewBookingDatatoPost:*********Started********");
            //if (UtilityManager.IsForceCultureENGB())
            //{
            //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            //}
            string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
            StringBuilder sb = new StringBuilder();
            try
            {
               
                string currentSubj = String.IsNullOrEmpty(appItem.Subject) ? "New Appointment" : appItem.Subject.Replace(" ", "|");
                if (UtilityManager.ConnectionMode() == 2)
                {
                    sb.Append("iSubject=" + HttpUtility.UrlEncode(currentSubj.Replace(" ", "|").Replace("&", "^")));
                }
                else
                {
                    sb.Append("iSubject=" + currentSubj.Replace(" ", "|").Replace("&", "^"));
                }
                sb.Append("&iFrom=" + appItem.Start.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iTo=" + appItem.End.ToString(dateFormat).Replace(" ", "|"));
                //Added by Ravi Goyal for PRB0040815
                if (UtilityManager.IsUnicode(appHelper.GetAppointmentAttendees(appItem)))
                {
                    sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|"), Encoding.GetEncoding("ISO-8859-1")));
                }
                else
                {
                    sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                }
                //sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                //End Added by Ravi Goyal for PRB0040815
                sb.Append("&iRecDates=" + appHelper.GetAppointmentRecurrenceDates(appItem).Replace(" ", "|"));
                //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                sb.Append("&iEmail=" + UtilityManager.GetCurrentUserForPost());

                //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                sb.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                //End Change
                sb.Append("&ssoUser=" + UtilityManager.SSOUserName);
                sb.Append("&iMeetingType=" + ((int)appItem.Sensitivity).ToString());
                sb.Append("&iSwitch=1");
                sb.Append("&currentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("PermissionCheck " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.GetNewBookingDatatoPost: Data:" + sb.ToString());
            UtilityManager.LogMessage("BookingHelper.GetNewBookingDatatoPost:*********Finished********");
            return sb.ToString();
        }
        public string HandelNewBookingRequest(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.HandelNewBookingRequest:*********Started********");
            string result = "";
            try
            {
                string dataToPost = this.GetNewBookingDatatoPost(appItem);

                result = UtilityManager.PostDataToServer(dataToPost);

                appHelper.SaveAppointmentPostID(appItem, result);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("HandelNewBookingRequest " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.HandelNewBookingRequest:*********Finished********");
            return result;

        }




        public string GetExistingBookingDatatoPost(Outlook.AppointmentItem appItem)
        {

            UtilityManager.LogMessage("BookingHelper.GetExistingBookingDatatoPost:*********Started********");
            CondecoMeeting currMeeting = this.GetCurrentCondecoMeeting(appItem, false);
            string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
            StringBuilder sb = new StringBuilder();
            try
            {
                //if (UtilityManager.IsForceCultureENGB())
                //{
                //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                //}
                DateTime appStart = appItem.Start;
                DateTime appEnd = appItem.End;
                if (currMeeting.LocationID > 0)
                {
                    appStart = UtilityManager.ConvertDateToTZ(appItem.Start, currMeeting.LocationID);
                    appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
                }

                string currentSubj = String.IsNullOrEmpty(appItem.Subject) ? "New Appointment" : appItem.Subject;
                string currentID = "";
                currentID = appHelper.GetAppointmentPostID(appItem);

                sb.Append("iID=" + currentID);
                // sb.Append("&iSubject=" + currentSubj.Replace(" ", "|").Replace("&", "^"));
                if (UtilityManager.ConnectionMode() == 2)
                {
                    sb.Append("&iSubject=" + HttpUtility.UrlEncode(currentSubj.Replace(" ", "|").Replace("&", "^")));
                }
                else
                {
                    sb.Append("&iSubject=" + currentSubj.Replace(" ", "|").Replace("&", "^"));
                }

                sb.Append("&iFrom=" + appStart.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iTo=" + appEnd.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iFromOrig=" + appStart.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iToOrig=" + appEnd.ToString(dateFormat).Replace(" ", "|"));
                //Added by Ravi Goyal for PRB0040815
                //Encoding ISO-8859-1 with complete coverage of Languages
                //Afrikaans,Albanian,Basque,Breton,Corsican,Danish[a],English[b],Faroese,Galician,German,Icelandic,Indonesian,Italian,Kurdish[c],
                //Leonese,Luxembourgish[d],Malay[e],Manx
                //,Norwegian[f],Occitan,Portuguese[g],Rhaeto-Romanic,Scottish Gaelic,Spanish,Swahili,Swedish,Walloon
                if (UtilityManager.IsUnicode(appHelper.GetAppointmentAttendees(appItem)))
                {
                    sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|"), Encoding.GetEncoding("ISO-8859-1")));
                }
                else
                {
                    sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                }
                //End Added by Ravi Goyal for PRB0040815

                //Below was the oroginal line before **PRB0040815**
                //sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                
                //sb.Append("&iRecDates=" + appHelper.GetAppointmentRecurrenceDates(appItem).Replace(" ", "|"));
                //By Anand on 8-Feb-2013 for TP issue 10614, 10611
                string filteredRecDates = FilterIndividuallyEditedItems(appHelper.GetAppointmentRecurrenceDates(appItem), currentID, appItem);
                sb.Append("&iRecDates=" + filteredRecDates.Replace(" ", "|"));
                //end
                //by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549

                sb.Append("&iEmail=" + UtilityManager.GetCurrentUserForPost());
                //End Change
                sb.Append("&iMeetingType=" + ((int)appItem.Sensitivity).ToString());
                //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                sb.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                sb.Append("&ssoUser=" + UtilityManager.SSOUserName);
                sb.Append("&iSwitch=2");
                sb.Append("&currentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetExistingBookingDatatoPost " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.GetExistingBookingDatatoPost: Data:" + sb.ToString());
            UtilityManager.LogMessage("BookingHelper.GetExistingBookingDatatoPost:*********Finished********");
            return sb.ToString();
        }

        //By Waqas on 8-Feb-2013 for TP issue 10614, 10611
        public string FilterIndividuallyEditedItems(string recDates, string postID, AppointmentItem appItem)
        {
            List<CondecoMeeting> editedMeetings = new List<CondecoMeeting>();
            List<DateTime> meetingDates = new List<DateTime>();

            DataSet condecoData = this.GetMeetingFromCondeco(postID);
            string result = recDates;
            try
            {


                // }
                if (condecoData.Tables.Count < 7) return recDates;
                if (condecoData.Tables[6].Rows.Count == 0) return recDates;

                int rowID = 0; // user for normal booking


                foreach (DataRow dr in condecoData.Tables[6].Rows)
                {
                    //&& (dr["isDeleted"].ToString().Equals("0") && dr["isRejected"].ToString().Equals("0"))
                    DataRow currentRow = condecoData.Tables[6].Rows[rowID];
                    DateTime dateResult = DateTime.MinValue;
                    DateTime.TryParse(dr["DateFrom"].ToString(), out dateResult);


                    if ((dr["Edited"].ToString().Equals("1")))
                    {
                        CondecoMeeting cMeeting = this.PopulateCondecoMeeting(postID, condecoData.Tables[6].Rows[rowID]);
                        editedMeetings.Add(cMeeting);
                        meetingDates.Add(CrossDateTimeReturnLTZDate(appItem, cMeeting.OriginalDateFrom));
                        //if (isCrossDayRecurranceBooking(appItem))
                        //{
                        //    DateTime dt = UtilityManager.ConvertDateToLocalTZ(cMeeting.OriginalDateFrom, cMeeting.LocationID);
                        //    meetingDates.Add(dt.Date);
                        //}
                        //else
                        //{
                        //    meetingDates.Add(cMeeting.OriginalDateFrom);
                        //}

                    }
                    rowID++;
                }
                List<DateTime> recDatesList = new List<DateTime>();
                List<DateTime> recDatesStore = new List<DateTime>();

                if (editedMeetings.Count > 0)
                {
                    string appRecDates = recDates;

                    if (!string.IsNullOrEmpty(appRecDates))
                    {
                        string[] recDatesData = appRecDates.Split(',');
                        foreach (string strDate in recDatesData)
                        {
                            recDatesList.Add(Convert.ToDateTime((strDate)));
                        }
                    }
                    recDatesStore = recDatesList;
                    foreach (var recDate in meetingDates)
                    {
                        if (recDatesList.Contains(recDate))
                            recDatesStore.Remove(recDate);
                    }
                    int counter = 0;
                    foreach (var newDate in recDatesStore)
                    {
                        if (counter == 0)
                            result = newDate.ToString(UtilityManager.CondecoDateFormat);
                        else
                            result = result + "," + newDate.ToString(UtilityManager.CondecoDateFormat);
                        //increment the counter
                        counter++;
                    }
                }

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("FilterIndividuallyEditedItems:" + postID + "::" + ex.Message);
            }

            return result;


        }


        public string GetCurrentBookingData(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.HandelNewBookingRequest:*********Started********");
            UtilityManager.LogMessage("BookingHelper.GetCurrentBookingData:*********Started********");
            CondecoMeeting currMeeting = this.GetCurrentCondecoMeeting(appItem, false);
            string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
            StringBuilder sb = new StringBuilder();
            try
            {
                //if (UtilityManager.IsForceCultureENGB())
                //{
                //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                //}
                DateTime appStart = appItem.Start;
                DateTime appEnd = appItem.End;
                if (currMeeting.LocationID > 0)
                {
                    appStart = UtilityManager.ConvertDateToTZ(appItem.Start, currMeeting.LocationID);
                    appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
                }

                string currentSubj = String.IsNullOrEmpty(appItem.Subject) ? "New Appointment" : appItem.Subject;

                // sb.Append("iSubject=" + currentSubj.Replace("&", "^"));
                if (UtilityManager.ConnectionMode() == 2)
                {
                    sb.Append("iSubject=" + HttpUtility.UrlEncode(currentSubj.Replace(" ", "|").Replace("&", "^")));
                }
                else
                {
                    sb.Append("iSubject=" + currentSubj.Replace(" ", "|").Replace("&", "^"));
                }

                sb.Append("&iFrom=" + appStart.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iTo=" + appEnd.ToString(dateFormat).Replace(" ", "|"));
                //Added by Ravi Goyal for PRB0040815
                //ISO-8859-1 with complete coverage of Languages
                //Afrikaans,Albanian,Basque,Breton,Corsican,Danish[a],English[b],Faroese,Galician,German,Icelandic,Indonesian,Italian,Kurdish[c],
                //Leonese,Luxembourgish[d],Malay[e],Manx
                //,Norwegian[f],Occitan,Portuguese[g],Rhaeto-Romanic,Scottish Gaelic,Spanish,Swahili,Swedish,Walloon
                if (UtilityManager.IsUnicode(appHelper.GetAppointmentAttendees(appItem)))
                {
                    try
                    {
                        sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|"), Encoding.GetEncoding("ISO-8859-1")));
                    }
                    catch 
                    {
                        sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                    }
                }
                else
                {
                    sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                }
                //sb.Append("&iAttendee=" + HttpUtility.UrlEncode(appHelper.GetAppointmentAttendees(appItem).Replace(" ", "|")));
                //End Added by Ravi Goyal for PRB0040815
                
                sb.Append("&iRecDates=" + appHelper.GetAppointmentRecurrenceDates(appItem).Replace(" ", "|"));
                //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                sb.Append("&iEmail=" + UtilityManager.GetCurrentUserForPost());
                //End Change
                sb.Append("&iMeetingType=" + ((int)appItem.Sensitivity).ToString());
                sb.Append("&currentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));

                UtilityManager.LogMessage("BookingHelper.GetCurrentBookingData: Data:" + sb.ToString());

            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("GetExistingBookingDatatoPost: " + ex.Message);
            }

            UtilityManager.LogMessage("BookingHelper.GetCurrentBookingData:*********Finished********");
            return sb.ToString();
        }

        public string HandleExistingBookingRquest(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("BookingHelper.HandleExistingBookingRquest:*********Started********");
            string result = "";
            try
            {

                string dataToPost = this.GetExistingBookingDatatoPost(appItem);
                UtilityManager.PostDataToServer(dataToPost);
                result = appHelper.GetAppointmentPostID(appItem);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("BookingHelper.HandleExistingBookingRquest: Data:" + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.HandleExistingBookingRquest:*********Finished********");
            return result;
        }
        public void RemoveSeriesFromCondeco(Outlook.AppointmentItem delAppItem)
        {
            try
            {
                if (!delAppItem.Location.ToLower().Contains("cancelled"))
                {
                    DeleteCondecoBooking(delAppItem, true);
                    CondecoMeeting curMeeting = GetCurrentCondecoMeeting(delAppItem, true);
                    appHelper.SetAppointmentLocation(delAppItem, curMeeting.MainLocation);
                    //UserProperties uProps = delAppItem.UserProperties;
                    //UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
                    //if (prop == null)
                    //{

                    //    prop = uProps.Add("AppointmentLocationSynced", Outlook.OlUserPropertyType.olYesNo, false, null);
                    //    prop.Value = true;



                    //}

                    //UtilityManager.FreeCOMObject(prop);
                    //UtilityManager.FreeCOMObject(uProps);
                    string NameValue = "";
                    NameValue = UserPropertiesExtension.GetNamedPropertyRecord(delAppItem, CustomProperty.AppointmentLocationSynced);
                    if (String.IsNullOrEmpty(NameValue))
                    {
                        UserPropertiesExtension.SetNamedPropertyRecord(delAppItem, CustomProperty.AppointmentLocationSynced, Convert.ToString("True"));
                    }

                    appHelper.ClearAppointmentData(delAppItem);
                    delAppItem.Save();
                }
            }
            catch (Exception)
            {

                // throw;
            }



        }

        //added by anand for checking date change due to time zone variance
        public bool CheckCrossDateBookingDueToTimeZoneDifference(CondecoMeeting currMeeting, Outlook.AppointmentItem currApp, bool showMessage)
        {
            string postId = appHelper.GetAppointmentPostID(currApp);
            if (!String.IsNullOrEmpty(postId))
            {
                if (currMeeting == null)
                    currMeeting = GetCondecoMeeting(postId, currApp, false);

                if (currMeeting.BookingID > 0)
                {
                    DateTime startDate = currApp.Start;
                    DateTime endDate = currApp.End;
                    DateTime meetingStartDate = UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID);
                    DateTime meetingEndDate = UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID);

                    if (startDate.Date != meetingStartDate.Date)
                    {
                        if (showMessage)
                            MessageBox.Show(CondecoResources.Location_DateDiff_As_Timezone_Not_Same, CondecoResources.Condeco_Error_Caption,
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (endDate.Date != meetingEndDate.Date)
                    {
                        if (showMessage)
                            MessageBox.Show(CondecoResources.Location_DateDiff_As_Timezone_Not_Same, CondecoResources.Condeco_Error_Caption,
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            return true;
        }
        public void IsVCWithin1hourCheck(int bookingID, string userName)
        {
            //method created by chandra 27Oct13
            this.IsVCWithin1hour = false;
            try
            {
                //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                string IsVCWithin1hourCheck_URL = "fkbookingID=" + bookingID.ToString() + "&iEmail=" + userName + "&iSwitch=12" + "&ssoUser=" + UtilityManager.SSOUserName;
                UtilityManager.LogMessage("checking IsVCWithin1hourCheck wiht data: " + IsVCWithin1hourCheck_URL);
                string rslt = UtilityManager.PostDataToServer(IsVCWithin1hourCheck_URL);
                if (!String.IsNullOrEmpty(rslt))
                    this.IsVCWithin1hour = !Convert.ToBoolean(rslt);
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("PermissionCheck " + ex.Message);
            }
        }
        public int BookingAdvanceChecking(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            //method created by chandra 27Oct13
            int result = -100;
            try
            {
                UtilityManager.LogMessage("BookingHelper.BookingAdvanceChecking:*********Started********Appointmetn Start:" + appItem.Start);

                DateTime appStart = UtilityManager.ConvertDateToTZ(appItem.Start, currMeeting.LocationID);
                DateTime appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
                string appSubject = appItem.Subject;

                string dataToPost = this.GetBookingAdvanceCheckingDatatoPost(currMeeting.BookingID, appStart, appEnd, appSubject, appItem);
                UtilityManager.LogMessage("BookingHelper.BookingAdvanceChecking:Posting Data:" + dataToPost);
                string response = UtilityManager.PostDataToServer(dataToPost);
                UtilityManager.LogMessage("BookingHelper.BookingAdvanceChecking:Response:" + response);
                //Check not to return empty string  by Paritosh Mishra to fix TP #19968. CRI2-6550:
                if (!String.IsNullOrEmpty((Convert.ToString(response))))
                {
                    result = Convert.ToInt32(response);
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("IsCondecoBookingSaved " + ex.Message);
            }
            UtilityManager.LogMessage("BookingHelper.UpdateCondecoBooking:*********Finished********");
            return result;
        }
        public string GetBookingAdvanceCheckingDatatoPost(int bookingID, DateTime startDate, DateTime endDate, string subject, Outlook.AppointmentItem appItem)
        {
            //method created by chandra 27Oct13
            StringBuilder sb = new StringBuilder();
            try
            {
                string dateFormat = UtilityManager.CondecoDateFormat + " HH:mm";
                int individualyEdited = 0;
                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence)
                {
                    individualyEdited = 1;
                }
                // sb.Append("iID=" + postId);            
                sb.Append("iFrom=" + startDate.ToString(dateFormat).Replace(" ", "|"));
                sb.Append("&iTo=" + endDate.ToString(dateFormat).Replace(" ", "|"));
                //sb.Append("&iSubject=" + subject.Replace(" ", "|").Replace("&", "^"));
                if (UtilityManager.ConnectionMode() == 2)
                {
                    sb.Append("&iSubject=" + HttpUtility.UrlEncode(subject.Replace(" ", "|").Replace("&", "^")));
                }
                else
                {
                    sb.Append("&iSubject=" + subject.Replace(" ", "|").Replace("&", "^"));
                }

                sb.Append("&fkBookingID=" + bookingID);
                sb.Append("&individualyEdited=" + individualyEdited);
                //Changed by Anand : Now using GetCurrentUserForPost instead of GetCurrentUser to pass | for spaces CRI 5549
                sb.Append("&iEmail=" + UtilityManager.GetCurrentUserForPost());
                //End Change
                //Below a line is change to merge 4.0.17 to V5 By Paritosh only add parameter thisDomains
                sb.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                sb.Append("&ssoUser=" + UtilityManager.SSOUserName);
                sb.Append("&iSwitch=11");
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("GetBookingMovedDatatoPost " + ex.Message);
            }


            return sb.ToString();
        }
        //end

        public void OpenSSOFormAuthentication(String url)
        {
            UtilityManager.LogMessage("BookingHelper.OpenSSOFormAuthentication *********Started********");
            SSOFormAuthentication objSSOFormAuthentication = SSOFormAuthentication.Instance(url);
            //  objSSOFormAuthentication.webBrowserSSO
            objSSOFormAuthentication.Show();
            UtilityManager.LogMessage("BookingHelper.OpenSSOFormAuthentication *********Finished********");
        }
        public void CloseOpenSSOFormAuthentication(String url)
        {
            UtilityManager.LogMessage("BookingHelper.CloseOpenSSOFormAuthentication *********Started********");
            if (SSOFormAuthentication.IsInstanceCreated)
            {
                SSOFormAuthentication.Instance(url).Close();

            }
            UtilityManager.LogMessage("BookingHelper.CloseOpenSSOFormAuthentication*********Finished********");
        }

        // Switch this method from Appointment Helper  to remove booking helper from apphelper
        //public bool IsCondecoRegionVisible(Outlook.Inspector curInsp)
        //{
        //    bool result = true;
        //    try
        //    {
        //        object item = curInsp.CurrentItem;
        //        if (item == null) return result;
        //        if (item is Outlook.AppointmentItem)
        //        {
        //            Outlook.AppointmentItem curAppItem = item as Outlook.AppointmentItem;
        //            if (curAppItem.MeetingStatus == OlMeetingStatus.olMeetingReceived || (int)curAppItem.MeetingStatus == 7 ||
        //                !appHelper.IsMessageClassValid(curAppItem.MessageClass) || appHelper.IsOldOccurrenceItem(curAppItem)
        //                || IsNonCondecoOccurrence(curAppItem))
        //            {
        //                result = false;
        //            }

        //        }
        //        UtilityManager.FreeCOMObject(item);
        //    }
        //    catch
        //    {
        //    }
        //    return result;

        //}

        //Below method added by Paritosh to make booking on Outlook with respect to DST 
        public List<CondecoMeeting> GetRecurranceBookingHasDST(Outlook.AppointmentItem appItem, bool isSyncing)
        {
            List<CondecoMeeting> RecDSTMeetings = new List<CondecoMeeting>();
            string postID = appHelper.GetAppointmentPostID(appItem);
            DataSet condecoData = this.GetMeetingFromCondeco(postID);
            try
            {
                if (condecoData.Tables.Count < 7) return RecDSTMeetings;
                if (condecoData.Tables[6].Rows.Count == 0) return RecDSTMeetings;
                if (appItem == null) return RecDSTMeetings;
                int rowID = 0; // user for normal booking

                foreach (DataRow dr in condecoData.Tables[6].Rows)
                {
                    if (!dr["deleteBooking"].ToString().Equals("1"))
                    {
                        if (!isSyncing)
                        {
                            if (Convert.ToBoolean(dr["IsDST"]))
                            {
                                CondecoMeeting cMeeting = this.PopulateCondecoMeeting(postID, condecoData.Tables[6].Rows[rowID]);
                                RecDSTMeetings.Add(cMeeting);
                            }
                        }
                        else
                        {
                            CondecoMeeting cMeeting = this.PopulateCondecoMeeting(postID, condecoData.Tables[6].Rows[rowID]);
                            RecDSTMeetings.Add(cMeeting);
                        }
                    }
                    rowID++;
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Get GetRecurranceBookingHasDST" + postID + "::" + ex.Message);
            }
            return RecDSTMeetings;


        }

        public bool isCrossDayRecurranceBooking(Outlook.AppointmentItem appItem)
        {
            // bool ret = false;
            Outlook.RecurrencePattern recPattern = null;
            if (!appItem.IsRecurring)
            {
                return false;
            }
            try
            {
                recPattern = appItem.GetRecurrencePattern();
                if (recPattern == null) return false;
                // BookingHelper bookingHelper = new BookingHelper();
                DateTime rDTStartDateToTZ;

                CondecoMeeting currMeeting = GetCurrentCondecoMeeting(appItem, false);
                if (currMeeting.BookingID > 0)
                {
                    long meetingTicks = recPattern.PatternStartDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                    DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                    rDTStartDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesStart, currMeeting.LocationID, currMeeting.OriginalTZ);

                    //meetingTicks = recPattern.PatternEndDate.Date.Ticks + recPattern.EndTime.TimeOfDay.Ticks;
                    //DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                    //rDTEndDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesEnd, currMeeting.LocationID, currMeeting.OriginalTZ);//UtilityManager.ConvertDateToTZ(recPattern.PatternEndDate, currMeeting.LocationID);
                }
                else
                {
                    return false;

                    //rDTStartDateToTZ = recPattern.PatternStartDate;
                    //rDTEndDateToTZ = recPattern.PatternEndDate;
                }
                if (rDTStartDateToTZ.Date == recPattern.PatternStartDate.Date)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            finally
            {
                UtilityManager.FreeCOMObject(recPattern);
                recPattern = null;
            }

            // bookingHelper = null;
            //   return ret;
        }
        public DateTime CrossDateTimeReturnLTZDate(Outlook.AppointmentItem appItem, DateTime dt1)
        {
            CondecoMeeting currMeeting = GetCurrentCondecoMeeting(appItem, false);
            DateTime dt;
            if (isCrossDayRecurranceBooking(appItem))
            {
                dt = UtilityManager.ConvertDateToLocalTZ(dt1, currMeeting.LocationID);

            }
            else
            {
                dt = dt1;
            }
            return dt.Date;
        }

    }
}
