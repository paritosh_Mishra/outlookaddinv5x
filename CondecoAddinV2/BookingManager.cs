using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Security.Policy;
using Outlook = Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;
using System.Reflection;
using CondecoAddinV2.Repository;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;
using System.Collections.Generic;
using System.IO;

namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for BookingManager.
    /// </summary>
    public class BookingManager : Form //: AddinExpress.OL.ADXOlForm
    {
        public delegate void CollapseHandler(object sender);
        public event CollapseHandler Collapse;

        private System.ComponentModel.IContainer components = null;
        private static AppointmentHelper appHelper = new AppointmentHelper();
        private static BookingHelper bookingHelper = new BookingHelper();
        //  private static SyncManager syncManager = new SyncManager();

        //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
        private static bool IsLoaderHide = false;
        //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
        private bool IsSeriesEdited = false;
        //Added by Ravi Goyal for PRB0040163 (CRD-7585)
        private bool IsNoOccurrenceBookingExist = false;
        //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
        //
        private string currentBookingData = string.Empty;
        // bool minimizedForm = true;
        bool bookingSaved = false;
        bool IsValidationValid = true;
        // bool condition = true;

        object OutlookAppObj = null;
        private WebBrowser condecoBrowser;
        object AddinModule = null;
        PictureBox loaderPicture = null;
        public BookingManager(WebBrowser browserObject, object OutlookAppObj, object AddinModule, PictureBox loaderPicture)
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            condecoBrowser = browserObject;
            condecoBrowser.Visible = true;
            this.condecoBrowser.Navigated += new WebBrowserNavigatedEventHandler(condecoBrowser_Navigated);
            this.condecoBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(condecoBrowser_DocumentCompleted);
            this.condecoBrowser.ProgressChanged += new WebBrowserProgressChangedEventHandler(condecoBrowser_ProgressChanged);
            // this.condecoBrowser.Navigating += new WebBrowserNavigatingEventHandler(condecoBrowser_Navigating);

            this.OutlookAppObj = OutlookAppObj;
            this.AddinModule = AddinModule;
            this.loaderPicture = loaderPicture;
            // TODO: Add any initialization after the InitializeComponent call
            //string iconpath = UtilityManager.IconPath + "\\room_booking.ico";
            //System.Drawing.Icon ico = new System.Drawing.Icon(iconpath);
            //this.Icon = ico;


        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.condecoBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // condecoBrowser
            // 
            this.condecoBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.condecoBrowser.Location = new System.Drawing.Point(0, 0);
            this.condecoBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.condecoBrowser.Name = "condecoBrowser";
            this.condecoBrowser.Size = new System.Drawing.Size(975, 400);
            this.condecoBrowser.TabIndex = 5;
            this.condecoBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.condecoBrowser_DocumentCompleted_1);
            // 
            // BookingManager
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(975, 400);
            this.Controls.Add(this.condecoBrowser);
            this.Name = "BookingManager";
            this.Text = "Room Booking";
            this.ResumeLayout(false);

        }

        //void condecoBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        //{
        //    //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
        //    //if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx"))
        //    //{
        //    //    IsLoaderHide = true;
        //    //}
        //    //else
        //    //{

        //    //    IsLoaderHide = false;
        //    //}
        //    ////

        //    //if (e.Url.ToString().ToLower().CompareTo("about:blank") != 0 && !e.Url.ToString().ToLower().Contains("redirected=1"))
        //    //{
        //    //    e.Cancel = true;
        //    //    UtilityManager.RedirectWithWait(condecoBrowser, e.Url);
        //    //}
        //    ////try
        //    ////{
        //    ////    Uri path = new Uri(Assembly.GetExecutingAssembly().CodeBase);


        //    ////    String redirectPath = "file://" + (new System.IO.FileInfo(path.AbsolutePath)).Directory.ToString().Replace("\\", "/");
        //    ////    redirectPath += "/redirect.htm?url=";

        //    ////    if (!(e.Url.ToString().ToLower().Contains("file://") || UtilityManager.GetReferrer(condecoBrowser).ToLower().Contains("redirect.htm")))
        //    ////    {
        //    ////        e.Cancel = true;
        //    ////        condecoBrowser.Url = new Uri(redirectPath + e.Url.ToString());
        //    ////    }
        //    ////}
        //    ////catch { }

        //    //this.condecoBrowser.Visible = false;
        //    //this.loaderPicture.Visible = true;

        //}


        void condecoBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {

            // !IsLoaderHide  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            if (e.CurrentProgress != e.MaximumProgress && e.MaximumProgress >= 0 && e.CurrentProgress >= 0 && !IsLoaderHide)
            {
                UtilityManager.LogMessage("True --condecoBrowser.Url " + condecoBrowser.Url);
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2), (condecoBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = true;
                //this.condecoBrowser.Visible = false;
            }
            else
            {
                //Tp# 21514 fixed by paritosh loader not visible 

                if (condecoBrowser.ReadyState == WebBrowserReadyState.Complete)
                {
                    this.loaderPicture.Visible = false;
                    UtilityManager.LogMessage("False --condecoBrowser.Url " + condecoBrowser.Url);
                }

                //this.condecoBrowser.Visible = true;
            }
        }
        #endregion

        #region BookingManager Form Methods

        //private void BookingManager_ADXBeforeFormShow()
        //{

        //    this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);

        //}

        private void BookingManager_ADXSelectionChange()
        {
            //this.condecoBrowser.Url = new Uri(this.GetNewBookingPath());
            //if (minimizedForm)
            //{
            //    RegionState = AddinExpress.OL.ADXRegionState.Minimized;


            //}
            //else
            //{
            //    RegionState = AddinExpress.OL.ADXRegionState.Normal;
            //}
            // minimizedForm = !minimizedForm;
        }

        public void BookingManager_Activated(object sender, EventArgs e)
        {
            // MessageBox.Show("Activated");

            // if (!minimizedForm)
            //     RegionState = AddinExpress.OL.ADXRegionState.Normal;

            // if (Active)
            // {

            if (!UtilityManager.IsCondecoContactable())
            {
                //this.Location = new Point(0, 0);
                // this.condecoBrowser.Size = this.Size;
                //TODO - Offline Browser
                //this.condecoBrowser.Url = new Uri(UtilityManager.GetOfflinePath());
                //this.condecoBrowser.fin
                //string x = this.condecoBrowser.Document.Body.InnerHtml;
                //x = x.Replace("#text#","DONE");

                //System.IO.MemoryStream langStream = new System.IO.MemoryStream();
                //System.IO.StreamWriter sw = new System.IO.StreamWriter(langStream);
                //sw.WriteLine("<html><body>hello</body></html>");
                //sw.Flush();
                //sw.Close();
                string strHtmlBuffer = global::CondecoAddinV2.App_Resources.CondecoResources.Offline;
                strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                //Added by Paritosh .Fix issue #16519 of TP ,added multilingual msg 
                strHtmlBuffer = strHtmlBuffer.Replace("#OfflineMsg#", global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Offline_Message);
                // 
                this.condecoBrowser.DocumentText = strHtmlBuffer;
            }
            else
            {
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((condecoBrowser.Width / 2) - (loaderPicture.Width / 2), (condecoBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = true;
                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    if (!condecoBrowser.IsDisposed)
                    {
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                    return;
                }
                string result = this.PostBookingData();

                // activated1 = true;
            }

            /* }
             else
             {

                 activated1 = false;
             }*/


            //http://localhost/login/login.asp?thisU=waqask&outlookPostID=1010&outlook=1&edit=1&OutlookEditType=&x=1&OutlookGrid=&BookingID=

        }

        //private void BookingManager_ADXAfterFormShow()
        //{

        //}

        #endregion

        #region Condeco Methods

        private Uri GetNewBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            UriBuilder uriBuilder = new UriBuilder();
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();

                //StringBuilder newpath = new StringBuilder();
                string currentUser = UtilityManager.GetCurrentUserName();

                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;
                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /* newpath.Append("thisU=" + currentUser);
                
                 newpath.Append("&outlookPostID=" + postID);
                 newpath.Append("&outlook=1&edit=");
                 newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                 newpath.Append("&x=1&OutlookGrid=&BookingID=");
                 if (UtilityManager.ConnectionMode() == 1)
                 {
                     newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                 }
                 newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/

                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, "", -1, -1, "", "", appHelper.GetAppointmentBookingType(appItem), "", "", "", "");// newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("GetNewBookingPath(string postID, Outlook.AppointmentItem appItem) Catch " + ex.Message);
            }

            Uri cURI = uriBuilder.Uri;
            return cURI;
        }

        private Uri GetExistingBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            // iLoginPath & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "
            //&OutlookEditType="&iOutlookEditType&"&From="&iFrom&"&To="&iTo&"&BookingID="&iBookingID)    
            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();
                DateTime startDate = appItem.Start;
                DateTime endDate = appItem.End;
                //StringBuilder newpath = new StringBuilder();
                // check if Single Occcurrence Got a Condeco Reference or not

                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                if (currMeeting.BookingID == 0)
                {
                    cURI = GetNewBookingPath(postID, appItem);
                    return cURI;
                }
                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;

                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /* newpath.Append("thisU=" + UtilityManager.GetCurrentUserName());                
                 newpath.Append("&outlookPostID=" + postID);
                 newpath.Append("&outlook=1&edit=1&x=1");
                 newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                 newpath.Append("&From=" + UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat));
                 newpath.Append("&To=" + UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat));
                 newpath.Append("&BookingID=" + currMeeting.BookingID);
                  if (UtilityManager.ConnectionMode() == 1)
                  {
                      newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                  }
                  newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/
                //newpath.Append("&currrentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, "", -1, -1, "", "", appHelper.GetAppointmentBookingType(appItem), "1", UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat), UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat), currMeeting.BookingID.ToString());// newpath.ToString();
            }
            catch (System.Exception)
            {
            }
            cURI = uriBuilder.Uri;
            return cURI;
        }

        private string PostBookingData()
        {
            string postID = "";
            Outlook.AppointmentItem appItem = null;
            try
            {
                UtilityManager.LogMessage("BookingManager -PostBookingData");
                appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return string.Empty;
                if (UtilityManager.ConnectionMode() == 1)
                {
                    //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                    if (UtilityManager.UserName.Trim().Equals(""))
                    {
                        if (UtilityManager.GetOutlookVersion(this.OutlookAppObj) == 12)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                        }
                        else
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                        }
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                    //END CHANDRA
                    else if (!UtilityManager.IsCurrentUserValid)
                    {
                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized_RoomBooking);
                        condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                }

                ////moved here so will set the booking id when clicking on room booking only.
                //bookingHelper.AddCondecoReference(appItem);

                //check for past booking
                if (appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
                {
                    DateTime? origStart = AppointmentDataInfo.GetOriginalStartDate(appItem);
                    DateTime? origEnddate = AppointmentDataInfo.GetOriginalEndDate(appItem);
                    if (origStart != null && origStart < DateTime.Now && !appItem.Location.ToLower().Contains("cancelled"))
                    {
                        //show message that updated is not allowed
                        if (origStart == DateTime.MinValue)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.ObjectIssue_NeedToReopen);
                            condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                            return "-1";
                        }
                        // Below End date  check to fix the CRRI 6550 ,TP-19968 by Paritosh 
                        else if (origEnddate < DateTime.Now)
                        {
                            UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                            condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                            IsValidationValid = false;
                            return "-1";
                        }
                        //else if (origStart < DateTime.Now)
                        //{
                        //    appItem.Save();
                        //    if (appItem.Start < origStart)
                        //    {
                        //        IsValidationValid = false;
                        //        UtilityManager.ShowErrorMessage(CondecoResources.Booking_Update_Past);
                        //        return "-1";
                        //    }

                        //}
                    }
                }
                //end

                AppointmentDataInfo.SetSkipWriteCheck(appItem, true);
                //changed by Anand on 27_Feb_2013 for TP11988/TP12075
                bool saveNotRequired = (this.AddinModule as AddinModule).isItemNotInSync;
                postID = appHelper.GetAppointmentPostID(appItem);
                if (!string.IsNullOrEmpty(postID))
                {
                    int bookingId = bookingHelper.GetBookingID(postID, appItem);
                    if (bookingId > 0 && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem)))
                    {
                        saveNotRequired = true;
                    }
                    //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
                    if (bookingId > 0 && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                    {
                        IsSeriesEdited = true;
                    }
                    //
                }
                bool rtnVal = ValidationManager.ValidateAppointment(appItem, saveNotRequired);
                //end
                AppointmentDataInfo.SetSkipWriteCheck(appItem, false);
                if (!rtnVal)
                {
                    IsValidationValid = false;
                    condecoBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    return "-1";
                }

                (this.AddinModule as AddinModule).isItemNotInSync = false;
                //To increase Performance below is commented  
                //if (string.IsNullOrEmpty(currentBookingData))
                //    currentBookingData = bookingHelper.GetCurrentBookingData(appItem);


                if (appHelper.IsNewAppointment(appItem))
                {
                    postID = bookingHelper.HandelNewBookingRequest(appItem);
                    this.condecoBrowser.Url = this.GetNewBookingPath(postID, appItem);
                    if (string.IsNullOrEmpty(appItem.Location))
                        appItem.Location = CondecoResources.Appointment_Initial_Location;
                }
                else
                {
                    postID = bookingHelper.HandleExistingBookingRquest(appItem);

                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.BookRoomClicked, Convert.ToString("True"));
                    
                    //Added by paritosh on 10/oct 2013 to fix TP issue #18159
                    if (IsCheckBusinessHour(appItem))
                    {
                        this.condecoBrowser.Url = null;
                        bookingSaved = false;
                        IsValidationValid = true;
                        Collapse(this);
                        this.loaderPicture.Visible = false;
                        return "-1";
                    }
                    else
                    {
                        this.condecoBrowser.Url = this.GetExistingBookingPath(postID, appItem);
                        AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                    }
                }

            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("PostBookingData Catch " + ex.Message);
            }
            finally
            {

                UtilityManager.FreeCOMObject(appItem);
            }
            return postID;

        }


        //private bool IsCheckBusinessHour(Outlook.AppointmentItem appItem)
        //{
        //    Cursor.Current = Cursors.WaitCursor;
        //    CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
        //    int result = 1;

        //    // Below appItem.End check to fix the CRRI2 6550 ,CRI26043,TP-19968 by Paritosh 
        //    long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //    DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);
        //    if (appItem.Start.CompareTo(appItem.End) == 0)
        //    {
        //        result = -2;
        //    }
        //    //TP-21251 by Travendra
        //    else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0)
        //    {                
        //        result = -200;
        //    }
        //    //24/jan/2004
        //    else if (meetingStartDateTime1.CompareTo(DateTime.MinValue) != 0 && appItem.Start.CompareTo(meetingStartDateTime1) != 0 && (meetingStartDateTime1.CompareTo(DateTime.Now) < 0) && (appItem.Start.CompareTo(meetingStartDateTime1) < 0))
        //    {
        //        // MessageBox.Show("date change to past");
        //        result = -4;
        //    }
        //    // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh 
        //    else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
        //    {
        //        result = -3;
        //    }
        //    else if (appItem.AllDayEvent)
        //    {
        //        result = -400;
        //    }
        //    //else if (!bookingHelper.PermissionCheck(currMeeting.PostID, UtilityManager.GetCurrentUserForPost()))
        //    //{
        //    //    result = -999;
        //    //}
        //    else
        //    {
        //        result = bookingHelper.BookingAdvanceChecking(currMeeting, appItem);
        //    }
        //    bool blnSave = true;
        //    string message = "";
        //    if (result == -999 || result == -200 || result == -400 || result == -81 || result == -82 || result == -83 || result == -84 || result == -100 || result == -4 || result == -2 || result == -3 || result == -85 || result == -86)
        //    {
        //        if (result == -81)
        //            message = CondecoResources.Group_SingleDayBookingOnly;
        //        else if (result == -82)
        //            message = CondecoResources.Group_BusinessHourOverLap;
        //        else if (result == -83)
        //            message = CondecoResources.Group_AdvancedBookingPeriod;
        //        else if (result == -84)
        //            message = CondecoResources.Group_NoticeRequired;
        //        else if (result == -200)
        //        {
        //            message = CondecoResources.Booking_Moved_Past;
        //            blnSave = false;
        //        }
        //        else if (result == -400)
        //            message = CondecoResources.AllDayEvent_NotSupported;
        //        else if (result == -999) //added by Anand for permission verification
        //            message = CondecoResources.Check_Booking_Permission;
        //        else if (result == -2)
        //            message = CondecoResources.MeetingDate_Start_End_Same;
        //        else if (result == -3)
        //            message = CondecoResources.Booking_Update_Past;
        //        else if (result == -4)
        //        {
        //            message = CondecoResources.Booking_Moved_Past;
        //        }
        //        else if (result == -85)
        //        {
        //            message = CondecoResources.BookingIsInProgress;
        //        }
        //        else if (result == -86)
        //        {
        //            message = CondecoResources.AppointmentIsAlreadyClosed;
        //        }
        //        else if (result == -100)
        //        {
        //        }
        //        UtilityManager.LogMessage("BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  result ==" + result);
        //    }
        //    Cursor.Current = Cursors.Default;
        //    if (!string.IsNullOrEmpty(message))
        //    {
        //        MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        try
        //        {
        //            //below lines are Added by Paritosh To fix CRI2-5814 on 23/Jan/2014
        //            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
        //            {
        //                SyncManager sync = new SyncManager();
        //                Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
        //                long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //                DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
        //                DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
        //                                                                                             currMeeting.LocationID,
        //                                                                                             currMeeting.OriginalTZ);
        //                if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
        //                {
        //                    rpItem.StartTime = condecoSeriesStartTimeInLocalTz;     
        //                }

        //                meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //                DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
        //                DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
        //                                                                                             currMeeting.LocationID,
        //                                                                                             currMeeting.OriginalTZ);
        //                if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
        //                {
        //                    rpItem.EndTime = condecoSeriesEndTimeInLocalTz;

        //                }
        //                if (blnSave)
        //                {
        //                    appItem.Subject = currMeeting.MeetingTitle;
        //                    appItem.Save();
        //                }
        //                UtilityManager.FreeCOMObject(rpItem);
        //                UtilityManager.FreeCOMObject(appItem);
        //            }
        //            else 
        //            {

        //            long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //            DateTime meetingStartDateTime = new DateTime(meetingTicks);
        //            meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
        //            DateTime meetingEndDateTime = new DateTime(meetingTicks);
        //            appItem.Start = meetingStartDateTime;
        //            appItem.End = meetingEndDateTime;
        //            appItem.Subject = currMeeting.MeetingTitle;
        //            appItem.Save();
        //            }
        //            //Added by Paritosh by 23/Jan/2014
        //            UtilityManager.FreeCOMObject(appItem);
        //        }
        //        catch(Exception ex)
        //        {
        //            UtilityManager.LogMessage(" BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  catch(Exception ex) ex="+ ex.Message);
        //        }

        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        private bool IsCheckBusinessHour(Outlook.AppointmentItem appItem)
        {
            Cursor.Current = Cursors.WaitCursor;
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
            int result = 1;

            // Below appItem.End check to fix the CRRI2 6550 ,CRI26043,TP-19968 by Paritosh 
            long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);
            if (appItem.Start.CompareTo(appItem.End) == 0)
            {
                result = -2;
            }
            //TP-21251 by Travendra
            else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                result = -200;
            }
            //24/jan/2004
            else if (meetingStartDateTime1.CompareTo(DateTime.MinValue) != 0 && appItem.Start.CompareTo(meetingStartDateTime1) != 0 && (meetingStartDateTime1.CompareTo(DateTime.Now) < 0) && (appItem.Start.CompareTo(meetingStartDateTime1) < 0) && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                // MessageBox.Show("date change to past");
                result = -4;
            }
            // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh 
            else if (DateTime.Now.CompareTo(appItem.Start) >= 0 && DateTime.Now.CompareTo(appItem.End) > 0 && appItem.RecurrenceState != Outlook.OlRecurrenceState.olApptMaster)
            {
                result = -3;
            }
            else if (appItem.AllDayEvent)
            {
                result = -400;
            }
            else if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && appItem.Start.CompareTo(DateTime.Now) < 0 && !UtilityManager.IsSeriesSaveAllowed())
            {
                // MessageBox.Show(CondecoResources.Recurrence_Series_PastSave, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                result = -5;
            }
            else
            {
                result = bookingHelper.BookingAdvanceChecking(currMeeting, appItem);
            }
            bool blnSave = true;
            string message = "";
            if (result == -999 || result == -200 || result == -400 || result == -81 || result == -82 || result == -83 || result == -84 || result == -100 || result == -4 || result == -2 || result == -3 || result == -5 || result == -85 || result == -86)
            {
                if (result == -81)
                    message = CondecoResources.Group_SingleDayBookingOnly;
                else if (result == -82)
                    message = CondecoResources.Group_BusinessHourOverLap;
                else if (result == -83)
                    message = CondecoResources.Group_AdvancedBookingPeriod;
                else if (result == -84)
                    message = CondecoResources.Group_NoticeRequired;
                else if (result == -200)
                {
                    message = CondecoResources.Booking_Moved_Past;
                    blnSave = false;
                }
                else if (result == -400)
                    message = CondecoResources.AllDayEvent_NotSupported;
                else if (result == -999) //added by Anand for permission verification
                    message = CondecoResources.Check_Booking_Permission;
                else if (result == -2)
                    message = CondecoResources.MeetingDate_Start_End_Same;
                else if (result == -3)
                    message = CondecoResources.Booking_Update_Past;
                else if (result == -4)
                {
                    message = CondecoResources.Booking_Moved_Past;
                }
                else if (result == -85 && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && appItem.Start.CompareTo(DateTime.Now) < 0 && !UtilityManager.IsSeriesSaveAllowed())
                {
                    message = CondecoResources.BookingIsInProgress;
                }
                else if (result == -86)
                {
                    message = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (result == -5)
                {
                    message = CondecoResources.Recurrence_Series_PastSave;
                }
                else if (result == -100)
                {

                }
                UtilityManager.LogMessage("BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  result ==" + result);
            }
            Cursor.Current = Cursors.Default;
            if (!string.IsNullOrEmpty(message))
            {
                MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                try
                {
                    //below lines are Added by Paritosh To fix CRI2-5814 on 23/Jan/2014
                    if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                    {
                        SyncManager sync = new SyncManager();
                        Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
                        long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                        DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
                        DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                                                                                                     currMeeting.LocationID,
                                                                                                     currMeeting.OriginalTZ);
                        if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                        {
                            rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                        }

                        meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                        DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
                        DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                                                                                                     currMeeting.LocationID,
                                                                                                     currMeeting.OriginalTZ);
                        if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
                        {
                            rpItem.EndTime = condecoSeriesEndTimeInLocalTz;

                        }
                        if (blnSave)
                        {
                            appItem.Subject = currMeeting.MeetingTitle;
                            appItem.Save();
                        }
                        UtilityManager.FreeCOMObject(rpItem);
                        UtilityManager.FreeCOMObject(appItem);
                    }
                    else
                    {

                        long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                        DateTime meetingStartDateTime = new DateTime(meetingTicks);
                        meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                        DateTime meetingEndDateTime = new DateTime(meetingTicks);
                        appItem.Start = meetingStartDateTime;
                        appItem.End = meetingEndDateTime;
                        appItem.Subject = currMeeting.MeetingTitle;
                        appItem.Save();
                    }
                    //Added by Paritosh by 23/Jan/2014
                    UtilityManager.FreeCOMObject(appItem);
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage(" BookingManager IsCheckBusinessHour(Outlook.AppointmentItem appItem)  catch(Exception ex) ex=" + ex.Message);
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        //private void BookingManager_ADXBeforeInspectorSubpaneClose()
        //{

        //}

        //private void BookingManager_ADXLeave(object sender, AddinExpress.OL.ADXLeaveEventArgs e)
        //{

        //}

        //~BookingManager()
        //{
        //    Deactivate(); 
        //}

        public new void Deactivate()
        {
            UtilityManager.LogMessage("BookingManager -new void Deactivate() before SetAppointmentLocation()");
            //Added by Ravi Goyal for PRB0040163 (CRD-7585)
            if (!IsNoOccurrenceBookingExist)
            {
                SetAppointmentLocation();
            }
            //End Added by Ravi Goyal for PRB0040163 (CRD-7585)


            this.condecoBrowser.Navigated -= condecoBrowser_Navigated;
            this.condecoBrowser.DocumentCompleted -= condecoBrowser_DocumentCompleted;
            this.condecoBrowser.ProgressChanged -= condecoBrowser_ProgressChanged;
            // this.condecoBrowser.Navigating -= condecoBrowser_Navigating;
            UtilityManager.LogMessage("BookingManager -new void Deactivate() End");
        }
        private void SetAppointmentLocation()
        {
            try
            {
                UtilityManager.LogMessage("BookingManager -SetAppointmentLocation*****Start******");
                Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                if (appItem == null) return;
                string newLocSimple = bookingHelper.GetBookingLocation("", appItem);
                UtilityManager.LogMessage("BookingManager -SetAppointmentLocation newLocSimple =" + newLocSimple);
                string newLoc = appHelper.FormatLocationInCondecoStyle(newLocSimple);
                UtilityManager.LogMessage("BookingManager -SetAppointmentLocation newLoc =" + newLoc);
                bool updateLoctaion = true;
                if (newLoc.ToLower().Contains("cancelled"))
                {
                    string NameValueLocSync = "";
                    NameValueLocSync = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                    if (!String.IsNullOrEmpty(NameValueLocSync))
                    {
                        updateLoctaion = false;
                    }
                }
                if (!String.IsNullOrEmpty(newLoc) && updateLoctaion)
                {//anand changed location to " " if new loc is not empty
                    if (appItem.Location == null) appItem.Location = " ";
                    try
                    {
                        UtilityManager.LogMessage("BookingManager -SetAppointmentLocation appItem.Location = " + appItem.Location);
                    }
                    catch (Exception EX)
                    {
                        UtilityManager.LogMessage("BookingManager -SetAppointmentLocation catch " + EX.Message);
                    }
                    if (!appItem.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        UtilityManager.LogMessage("BookingManager -SetAppointmentLocation !appItem.Location.ToLower().Contains(newLoc.ToLower()) ");
                        appHelper.SetAppointmentLocation(appItem, newLocSimple);
                        UtilityManager.LogMessage("BookingManager -SetAppointmentLocation After  appHelper.SetAppointmentLocation(appItem, newLocSimple);");
                        //UserProperties uProps = appItem.UserProperties;
                        //UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
                        //if (prop != null)
                        //{
                        //    prop.Delete();
                        //}
                        //UtilityManager.FreeCOMObject(prop);
                        //UtilityManager.FreeCOMObject(uProps);
                        string namedValue = "";
                        namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        if (!String.IsNullOrEmpty(namedValue))
                        {
                            UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        }
                        if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !bookingHelper.isCrossDayRecurranceBooking(appItem))
                        {
                            //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402-check for outlook 2013 version and this event will be created for outlook 2010 & above versions, not applicable for outlook 2007.
                            //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.

                            if ((AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                            {
                                IsSeriesEdited = true;
                            }
                            SyncManager sync = new SyncManager();
                            sync.SyncIndiviudallyEditedItems(appItem, this.AddinModule as AddinModule, IsSeriesEdited, false);
                            sync = null;
                        }
                        UtilityManager.LogMessage("BookingManager.SetAppointmentLocation Appoitment location==  " + appItem.Location);
                    }
                }
                //Added by Paritosh by 23/Jan/2014
                UtilityManager.FreeCOMObject(appItem);
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("BookingManager.SetAppointmentLocation Catch " + ex.Message);
            }
            UtilityManager.LogMessage("BookingManager.SetAppointmentLocation ******END******  ");
        }
        //Ader below function tp fix TP:23070 	Description On creating a weekly meeting with times such that the endpoints span over two days, the location field is blank for all instances.
        //private bool isCrossDayRecurranceBooking(Outlook.AppointmentItem appItem)
        //{
        //   // bool ret = false;
        //    Outlook.RecurrencePattern recPattern = null;
        //    try
        //    {
        //        recPattern = appItem.GetRecurrencePattern();
        //        if (recPattern == null) return false;
        //        // BookingHelper bookingHelper = new BookingHelper();
        //        DateTime rDTStartDateToTZ;
        //        DateTime rDTEndDateToTZ;
        //        CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
        //        if (currMeeting.BookingID > 0)
        //        {
        //            long meetingTicks = recPattern.PatternStartDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
        //            DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
        //            rDTStartDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesStart, currMeeting.LocationID, currMeeting.OriginalTZ);

        //            //meetingTicks = recPattern.PatternEndDate.Date.Ticks + recPattern.EndTime.TimeOfDay.Ticks;
        //            //DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
        //            //rDTEndDateToTZ = UtilityManager.ConvertDateToTZ(currMeetingItemSeriesEnd, currMeeting.LocationID, currMeeting.OriginalTZ);//UtilityManager.ConvertDateToTZ(recPattern.PatternEndDate, currMeeting.LocationID);
        //        }
        //        else
        //        {
        //            return false;

        //            //rDTStartDateToTZ = recPattern.PatternStartDate;
        //            //rDTEndDateToTZ = recPattern.PatternEndDate;
        //        }
        //        if (rDTStartDateToTZ.Date == recPattern.PatternStartDate.Date)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }

        //    }
        //    finally
        //    {
        //        UtilityManager.FreeCOMObject(recPattern);
        //        recPattern = null;
        //    }

        //   // bookingHelper = null;
        // //   return ret;
        //}

        private void condecoBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            condecoBrowser.Tag = "";

            //Added calender.aspx to hide loader to fix #21918
            //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("ldap/condecouserlookup.asp?") || e.Url.ToString().ToLower().Contains("/calendar.aspx") || e.Url.ToString().ToLower().Contains("booking_resourceitem_addnote.asp"))
            {
                IsLoaderHide = true;
            }
            else
            {
                IsLoaderHide = false;
            }
            //

            if (bookingSaved || !IsValidationValid)
            {
                // UtilityManager.MinimizeAppointmentWindow(this.CurrentOutlookWindowHandle);
                bookingSaved = false;
                IsValidationValid = true;
                Collapse(this);
                this.loaderPicture.Visible = false;

                //UtilityManager.CollapseCondecoRegion(this);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);


            }

        }

        private void condecoBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {

            //this.condecoBrowser.Visible = true;
            // MessageBox.Show("Navigated to:" + e.Url);
            condecoBrowser.Tag = "";
            try
            {
                Uri curreURL = e.Url;
                if (curreURL == null) return;
                if (curreURL.Query == null) return;

                //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
                if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("ldap/condecouserlookup.asp?"))
                {
                    IsLoaderHide = true;
                }
                else
                {
                    IsLoaderHide = false;
                }
                //Added by Paritosh to fix #17745. 
                if (e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.asp?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.asp"))
                {
                    UtilityManager.LogMessage("BookingManager -condecoBrowser_Navigated");
                    Outlook.AppointmentItem appItemPay = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemPay.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                    {
                        if (appItemPay != null)
                            appItemPay.Save();
                    }
                    if (appItemPay.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    {
                        //Added by Ravi Goyal to fix CRD-6520- Recurring series and edit an occurence for location only, this will allow appointment to reset on calendar.
                        
                        if (!IsOLK2007(this.AddinModule as AddinModule))
                        {
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1** ");
                            appItemPay.Start = appItemPay.Start.AddMilliseconds(0.0000000001);
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1- added 0.0000000001 milliseconds to make it exception** ");
                        }
                        SetAppointmentLocation();
                        appItemPay.Save();
                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 1- SetAppointmentLocation() done** ");
                    }
                    UtilityManager.FreeCOMObject(appItemPay);
                }
                //Added by Paritosh to solve isssue TP #16913
                if (curreURL.Query.ToLower().Contains("booking_saved"))
                {
                    UtilityManager.LogMessage("BookingManager TP #16913 - booking_saved  condecoBrowser_Navigated");
                    Outlook.AppointmentItem appItemTemp = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemTemp.MeetingStatus != Outlook.OlMeetingStatus.olMeeting && appItemTemp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                    {
                        //Added by Ravi Goyal to fix CRD-6520- Recurring series and edit an occurence for location only, this will allow appointment to reset on calendar.
                        if (!IsOLK2007(this.AddinModule as AddinModule))
                        {
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 2** ");
                            appItemTemp.Start = appItemTemp.Start.AddMilliseconds(0.0000000001);
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 2- added 0.0000000001 milliseconds to make it exception** ");
                        }
                            SetAppointmentLocation();
                            appItemTemp.Save();
                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated -**CRD-6520 Check point 2- SetAppointmentLocation() done** ");
                    }
                    UtilityManager.FreeCOMObject(appItemTemp);
                }
                //end 
                if (curreURL.Query.ToLower().Contains("booking_saved") || !IsValidationValid)
                {
                    //added by Anand on 27_Feb_2013 for TP 11988
                    UtilityManager.LogMessage("BookingManager - booking_saved  condecoBrowser_Navigated");
                    Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    if (appItem != null)
                    {
                        //below lines added by paritosh to fix chevron issue- some instance of recurrance have Day light saving 
                        AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                        if (appItem.IsRecurring)
                        {
                            //List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItem, false);
                            SyncManager syncManager = new SyncManager();
                            //foreach (CondecoMeeting cMeeting in currMeeting)
                            //{
                              //  syncManager.SyncSeriesBookingHasDST(appItem, cMeeting);
                            //}
                            //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                            //Added by Ravi Goyal to fix Recurring series new,edit,delete email issues.
                            if (curreURL.Query.ToLower().Contains("booking_saved"))
                            {
                                AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, false);
                                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !IsSeriesEdited)
                                {
                                    if (appItem.Location.Contains(UtilityManager.GetLocationStartDelimiter()) || appItem.Location.Contains(UtilityManager.GetLocationEndDelimiter()))
                                    {
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - Existing condeco booking identified as locatioin have '" + UtilityManager.GetLocationEndDelimiter() + "' & '" + UtilityManager.GetLocationEndDelimiter() + "'");
                                        //existing booking, set IsSeriesEdited=false.
                                        IsSeriesEdited = true;
                                        CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                                        string MeetingLocation = appHelper.FormatLocationInCondecoStyle(curMeeting.MainLocation);
                                        if (appItem.Location != MeetingLocation && appItem.Location != CondecoResources.Appointment_Initial_Location)
                                        {
                                            AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, true);
                                            UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - Location change identified in recurring series");
                                        }
                                    }
                                    else if (appItem.Location == CondecoResources.Appointment_Initial_Location)
                                    {
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated - New Booking Identified because location is " + CondecoResources.Appointment_Initial_Location);
                                        //new booking, set IsSeriesEdited=false.
                                        IsSeriesEdited = false;
                                    }
                                }
                                //Added by Ravi Goyal to fix Recurring edit issues.
                                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                                {
                                    IsSeriesEdited = true;
                                    if (AppointmentDataInfo.GetIsOnlySeriesLocationUpdated(appItem))
                                    {
                                        AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, false);
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-Along with location Following changes are identified in booking");
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: StartDate:-" + AppointmentDataInfo.GetStartDateChanged(appItem));
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetEndDateChanged(appItem));
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetSubjectChanged(appItem));
                                    }
                                    else
                                    {
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-location is not changed, Following changes are identified in booking");
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: StartDate:-" + AppointmentDataInfo.GetStartDateChanged(appItem));
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetEndDateChanged(appItem));
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated: EndDate:-" + AppointmentDataInfo.GetSubjectChanged(appItem));
                                    }
                                }
                                //Added by Ravi Goyal to fix CRD-6520- Recurring series and edit an occurence for location only, this will allow appointment to reset on calendar.
                                if (!IsSeriesEdited && appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                                {
                                    if (!IsOLK2007(this.AddinModule as AddinModule))
                                    {
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated ---*CRD-6520 Check point 3* ");
                                        appItem.Start = appItem.Start.AddMilliseconds(0.0000000001);
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated ---*CRD-6520 Check point 3*- added 0.0000000001 milliseconds to make it exception** ");
                                    }
                                    if (appItem.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                                    {
                                        appItem.Save();
                                    }
                                    UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated ---*CRD-6520 Check point 3*- SetAppointmentLocation() done** ");
                                }
                                else
                                {
                                    if (AppointmentDataInfo.GetIsItemNonMeeting(appItem) && appItem.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                                    {
                                        IsSeriesEdited = true;
                                        AppointmentDataInfo.SetIsOnlySeriesLocationUpdated(appItem, true);
                                        UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-No Changes in Appointment series, Only switched from NonMeeting to Meeting");
                                    }
                                    UtilityManager.LogMessage("BookingManager.condecoBrowser_Navigated-series edited is set to:" + IsSeriesEdited);
                                    syncManager.SyncIndiviudallyEditedItems(appItem, this.AddinModule as AddinModule, IsSeriesEdited, false);
                                    UtilityManager.LogMessage("**BookingManager.condecoBrowser_Navigated-SyncIndiviudallyEditedItems completed**");
                                    syncManager = null;
                                }
                            }
                            ////--End-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                        }
                        //Added by Ravi Goyal for PRB0040163 (CRD-7585)
                        if (AppointmentDataInfo.GetNoOccurrenceBookingExist(appItem))
                        {
                            IsNoOccurrenceBookingExist = true;
                        }
                        //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
                        //Added by Ravi Goyal for PRB0043359
                        UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.CondecoBookingID, AppointmentDataInfo.GetBookingId(appItem).ToString());
                        //End Added by Ravi Goyal for PRB0043359
                        AppointmentDataInfo.ResetProperties(appItem);
                    }
                    
                    UtilityManager.FreeCOMObject(appItem);
                    //end
                    IsValidationValid = true;
                    AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, false);
                    //UtilityManager.CollapseCondecoRegion(this);
                    Collapse(this);
                    this.loaderPicture.Visible = false;

                }
            }
            catch (System.Exception)
            {
            }
        }

        private void condecoBrowser_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private bool IsOLK2007(AddinModule CurrentAddinModule)
        {
            bool IfOLK2007 = false;
            try
            {
                if (CurrentAddinModule != null)
                {
                    if (CurrentAddinModule.HostMajorVersion == 12)
                    {
                        IfOLK2007 = true;
                        UtilityManager.LogMessage("**BookingManager.IsOLK2007-Outlook Version 2007 detected**");
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("**BookingManager.IsOLK2007-Errror**"+ex.Message);
            }
            return IfOLK2007;
        }



        //private void BookingManager_ADXAfterFormHide(object sender, ADXAfterFormHideEventArgs e)
        //{


        //}

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    UtilityManager.CollapseCondecoRegion(this);
        //   // UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        //}

        //private void btnRefresh_Click(object sender, EventArgs e)
        //{
        //    String loaderPath = "file://" + (new System.IO.FileInfo(Assembly.GetExecutingAssembly().Location)).Directory.ToString().Replace("\\","/");
        //    loaderPath += "/LoaderPage.html";
        //    //loaderPath = @"file:///D:/Projects/RNMDevelopment/CondecoAddin2013_POC/CondecoAddinV2/bin/Debug/LoaderPage.html";


    }
}

