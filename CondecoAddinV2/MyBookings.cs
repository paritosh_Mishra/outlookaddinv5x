using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.App_Resources;

 
namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for MyBookings.
    /// </summary>
    public class MyBookings : AddinExpress.OL.ADXOlForm
    {
        private WebBrowser myBookingBrowser;
        private Button btnClose;
       // private static AppointmentHelper appHelper = new AppointmentHelper();
      //  private static BookingHelper bookingHelper = new BookingHelper();
        private System.ComponentModel.IContainer components = null;

        public MyBookings()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            string iconpath = UtilityManager.IconPath + "\\my_bookings.ico";
            System.Drawing.Icon ico = new System.Drawing.Icon(iconpath);
            this.Icon = ico;

            
            // TODO: Add any initialization after the InitializeComponent call
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyBookings));
            this.myBookingBrowser = new System.Windows.Forms.WebBrowser();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // myBookingBrowser
            // 
            this.myBookingBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myBookingBrowser.Location = new System.Drawing.Point(0, 0);
            this.myBookingBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.myBookingBrowser.Name = "myBookingBrowser";
            this.myBookingBrowser.Size = new System.Drawing.Size(975, 421);
            this.myBookingBrowser.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.ForeColor = System.Drawing.Color.Transparent;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(900, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(17, 18);
            this.btnClose.TabIndex = 2;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MyBookings
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(975, 421);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.myBookingBrowser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MyBookings";
            this.Text = "My Bookings";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Activated += new System.EventHandler(this.MyBookings_Activated);
            this.ADXBeforeFormShow += new AddinExpress.OL.ADXOlForm.BeforeFormShow_EventHandler(this.MyBookings_ADXBeforeFormShow);
            this.ADXSelectionChange += new AddinExpress.OL.ADXOlForm.SelectionChange_EventHandler(this.MyBookings_ADXSelectionChange);
            this.ResumeLayout(false);

        }
        #endregion

        private void MyBookings_Activated(object sender, EventArgs e)
        {
            try
            {
                Uri currentURL = new Uri(UtilityManager.GetOfflinePath());
                if (UtilityManager.ConnectionMode() == 1)
                {
                    //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                    if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.IsCondecoContactable())
                    {
                        if (UtilityManager.GetOutlookVersion(this.OutlookAppObj) == 12)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                        }
                        else
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                        }
                        myBookingBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                    //END-CHANDRA
                    else if (!UtilityManager.IsCurrentUserValid)
                    {
                        UtilityManager.ShowErrorMessage(CondecoResources.User_Not_Authorized_RoomBooking);
                        myBookingBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                }
                if (UtilityManager.IsCondecoContactable())
                    currentURL = new Uri(UtilityManager.MyRequestURL);

                this.myBookingBrowser.Url = currentURL;
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in MyBookings_Activated :" + ex.Message + " " + ex.StackTrace);
            }
           
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            UtilityManager.CollapseCondecoRegion(this);
            UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        }

        private void MyBookings_ADXSelectionChange()
        {
            try
            {
                if (!Active)
                {
                }
                else
                {

                    Uri currentURL = new Uri(UtilityManager.GetOfflinePath());
                    if (UtilityManager.IsCondecoContactable())
                        currentURL = new Uri(UtilityManager.MyRequestURL);

                    this.myBookingBrowser.Url = currentURL;
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error ocurredin in MyBookings_ADXSelectionChange :" + ex.Message + " " + ex.StackTrace);
            }
        }

        private void MyBookings_ADXBeforeFormShow()
        {
            //this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);
           // this.Visible = bookingHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);
        }
    }
}

