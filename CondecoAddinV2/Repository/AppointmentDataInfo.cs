﻿using System;
using System.Collections.Generic;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;

/*
Only Condeco appointment will be consider. Appointment time will be added when event register and keep the original dates 
 */

namespace CondecoAddinV2.Repository
{
    public static class AppointmentDataInfo
    {
        class AppointmentInfo
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public bool SkipWriteCheck { get; set; }
            public string EntryId { get; set; }
            public bool HavingPermission { get; set; }
            public int BookingId { get; set; }
            public bool Prop_StartDateChanged { get; set; }
            public bool Prop_EndDateChanged { get; set; }
            public bool Prop_SubjectChanged { get; set; }
            public bool Prop_IsOnlySeriesLocationUpdated { get; set; }
            public bool Prop_IsAppointmentNonMeeting { get; set; }
            public bool Prop_SetIsItemNonMeeting { get; set; }
            //Added by Ravi Goyal for PRB0040163 (CRD-7585)
            public bool Prop_NoOccurrenceBookingExist { get; set; }
            //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
            public bool Prop_IsAppItemCondecoBookingOpen { get; set; }
        }

        private static List<AppointmentInfo> AppointmentDataCollection = new List<AppointmentInfo>();

        public static void AddAppointmentInCollection(Outlook.AppointmentItem appItem, bool havingPermission, int bookingId)
        {
            if (!string.IsNullOrEmpty(appItem.EntryID))
            {
                string entryId = appItem.EntryID;
                AppointmentDataCollection.RemoveAll(x => x.EntryId == entryId);
                AppointmentDataCollection.Add(new AppointmentInfo
                {
                    EntryId = entryId,
                    StartDate = appItem.Start,
                    EndDate = appItem.End,
                    HavingPermission = havingPermission,
                    BookingId = bookingId
                });
            }
        }

        public static bool GetHavingPermission(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalData = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalData != null)
                return originalData.HavingPermission;
            return true;
        }

        public static int GetBookingId(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalData = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalData != null)
                return originalData.BookingId;
            return 0;
        }

        public static DateTime? GetOriginalStartDate(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.StartDate;
            return null;
        }

        public static DateTime? GetOriginalEndDate(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.EndDate;
            return null;
        }

        public static void SetSkipWriteCheck(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.SkipWriteCheck = value;
        }

        public static bool GetSkipWriteCheck(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.SkipWriteCheck;
            return false;
        }

        public static void RemoveAppointmentFromCollection(Outlook.AppointmentItem appItem)
        {
            if (!string.IsNullOrEmpty(appItem.EntryID))
            {
                string entryId = appItem.EntryID;
                AppointmentDataCollection.RemoveAll(x => x.EntryId == entryId);
            }
        }

        public static void SetStartDateChanged(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_StartDateChanged = value;
        }
        public static void SetEndDateChanged(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_EndDateChanged = value;
        }
        public static void SetSubjectChanged(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_SubjectChanged = value;
        }
        public static bool GetStartDateChanged(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_StartDateChanged;
            return false;
        }
        public static bool GetEndDateChanged(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_EndDateChanged;
            return false;
        }
        public static bool GetSubjectChanged(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_SubjectChanged;
            return false;
        }
        //Added by Ravi Goyal to Capture if only Series Location is updated, incase if true, the edited appointment email will be sent by outlook only
        public static void SetIsOnlySeriesLocationUpdated(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_IsOnlySeriesLocationUpdated = value;
        }
        public static bool GetIsOnlySeriesLocationUpdated(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_IsOnlySeriesLocationUpdated;
            return false;
        }
        public static void SetIsItemNonMeeting(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_SetIsItemNonMeeting = value;
        }
        //Added by Ravi Goyal for PRB0040163 (CRD-7585)
        public static bool GetIsItemNonMeeting(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_SetIsItemNonMeeting;
            return false;
        }
        public static void SetNoOccurrenceBookingExist(Outlook.AppointmentItem appItem, bool value)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                originalDate.Prop_NoOccurrenceBookingExist = value;
        }
        //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
        public static bool GetNoOccurrenceBookingExist(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
                return originalDate.Prop_NoOccurrenceBookingExist;
            return false;
        }
        //End by Ravi Goyal
        public static void ResetProperties(Outlook.AppointmentItem appItem)
        {
            AppointmentInfo originalDate = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (originalDate != null)
            {
                originalDate.Prop_StartDateChanged = false;
                originalDate.Prop_EndDateChanged = false;
                originalDate.Prop_SubjectChanged = false;
                originalDate.Prop_IsAppointmentNonMeeting = false;
                originalDate.Prop_NoOccurrenceBookingExist = false;
            }
        }
        public static bool GetAppItemCondecoBookingOpen(Outlook.AppointmentItem appItem)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                return appinfo.Prop_IsAppItemCondecoBookingOpen;
            return false;
        }

        public static void SetAppItemCondecoBookingOpen(Outlook.AppointmentItem appItem, bool value)
        {
            var appinfo = AppointmentDataCollection.Find(x => x.EntryId == appItem.EntryID);
            if (appinfo != null)
                appinfo.Prop_IsAppItemCondecoBookingOpen = value;
        }

    }
}
