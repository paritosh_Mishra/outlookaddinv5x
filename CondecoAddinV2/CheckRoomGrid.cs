using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using CondecoAddinV2.App_Resources;
using System.Reflection;
using CondecoAddinV2.Constants;
using CondecoAddinV2.Repository;
using System.Collections.Generic;

namespace CondecoAddinV2
{
    /// <summary>
    /// Summary description for CheckRoomGrid.
    /// </summary>
    public class CheckRoomGrid : Form
    {

        public delegate void CollapseHandler(object sender);
        public event CollapseHandler Collapse;

        private WebBrowser gridBrowser;
        private System.ComponentModel.IContainer components = null;
        private static AppointmentHelper appHelper = new AppointmentHelper();
        private static BookingHelper bookingHelper = new BookingHelper();
        private static SyncManager syncManager = new SyncManager();
        //private Button btnClose;
        bool IsValidationValid = true;
        bool bookingSaved = false;
        private string currentBookingData = string.Empty;
        object OutlookAppObj = null;
        object AddinModule = null;
        //object InspectorObj = null;
        PictureBox loaderPicture = null;
        //Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
        private static bool IsLoaderHide = false;
        //Added by Ravi Goyal for PRB0040163 (CRD-7585)
        private bool IsNoOccurrenceBookingExist = false;
        //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
        public CheckRoomGrid(WebBrowser browserObject, object OutlookAppObj, object AddinModule, object InspectorObj, PictureBox loaderPicture)
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            gridBrowser = browserObject;
            gridBrowser.Visible = true;
            this.gridBrowser.Navigated += new WebBrowserNavigatedEventHandler(gridBrowser_Navigated);
            this.gridBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(gridBrowser_DocumentCompleted);
            this.gridBrowser.ProgressChanged += new WebBrowserProgressChangedEventHandler(gridBrowser_ProgressChanged);

            this.OutlookAppObj = OutlookAppObj;
            this.AddinModule = AddinModule;
            this.loaderPicture = loaderPicture;

            string iconpath = UtilityManager.IconPath + "\\availability.ico";
            System.Drawing.Icon ico = new System.Drawing.Icon(iconpath);
            this.Icon = ico;

            // TODO: Add any initialization after the InitializeComponent call

        }



        void gridBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {

            // !IsLoaderHide  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected

            if (e.CurrentProgress != e.MaximumProgress && e.MaximumProgress >= 0 && e.CurrentProgress >= 0 && !IsLoaderHide)
            {
                UtilityManager.LogMessage("True --gridBrowser.Url " + gridBrowser.Url);
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2), (gridBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = true;
            }
            else
            {
                //Tp# 21514 fixed by paritosh loader not visible 
                if (gridBrowser.ReadyState == WebBrowserReadyState.Complete)
                {
                    UtilityManager.LogMessage("false --gridBrowser.Url " + gridBrowser.Url);
                    this.loaderPicture.Visible = false;
                }
                //this.condecoBrowser.Visible = true;
            }
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckRoomGrid));
            this.gridBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // gridBrowser
            // 
            this.gridBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridBrowser.Location = new System.Drawing.Point(0, 0);
            this.gridBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.gridBrowser.Name = "gridBrowser";
            this.gridBrowser.Size = new System.Drawing.Size(975, 400);
            this.gridBrowser.TabIndex = 0;
            this.gridBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.gridBrowser_DocumentCompleted);
            this.gridBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.gridBrowser_Navigated);
            // 
            // CheckRoomGrid
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(975, 400);
            this.Controls.Add(this.gridBrowser);
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CheckRoomGrid";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = App_Resources.CondecoResources.Check_Room;
            this.ResumeLayout(false);

        }
        #endregion

        public void CheckRoomGrid_Activated(object sender, EventArgs e)
        {


            //bool activated1 = true;

            Uri currentUri = new Uri(UtilityManager.GetOfflinePath());
            if (!UtilityManager.IsCondecoContactable())
            {
                //Added by Paritosh .Fix issue #16519 of TP ,added multilingual msg 
                string strHtmlBuffer = global::CondecoAddinV2.App_Resources.CondecoResources.Offline;
                strHtmlBuffer = strHtmlBuffer.Replace("#INSTALLDIR#", Assembly.GetExecutingAssembly().CodeBase.Substring(0, Assembly.GetExecutingAssembly().CodeBase.LastIndexOf("/")) + "//");
                strHtmlBuffer = strHtmlBuffer.Replace("#OfflineMsg#", global::CondecoAddinV2.App_Resources.CondecoResources.Condeco_Offline_Message);
                // 
                this.gridBrowser.DocumentText = strHtmlBuffer;
                //Below line is commneted by paritosh for solving #16519 of TP
                // this.gridBrowser.Url = new Uri(UtilityManager.GetOfflinePath());
            }
            else
            {
                this.loaderPicture.Size = new Size(64, 64);
                this.loaderPicture.Location = new Point((gridBrowser.Width / 2) - (loaderPicture.Width / 2), (gridBrowser.Height / 2) - (loaderPicture.Height / 2));
                this.loaderPicture.Visible = true;

                if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                {
                    if (!UtilityManager.IsSSOTimeOutMsgShow)
                    {

                        UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                    }
                    //UtilityManager.IsSSOTimeOutMsgShow = false;
                    if (!gridBrowser.IsDisposed)
                    {
                        this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                    }
                    return;
                }
                if (UtilityManager.OutlookGridType() == 1)
                {

                    string result = this.PostBookingData();
                }
                else
                {
                    currentUri = this.GetOutlookRoomBookingGridPath();

                    if (currentUri != null)
                        this.gridBrowser.Url = currentUri;
                }

                //UtilityManager.SetForegroundWindow(this.CurrentOutlookWindowHandle);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);


            }


        }

        //private void CheckRoomGrid_ADXSelectionChange()
        //{

        //}

        private Uri GetOutlookRoomBookingGridPath()
        {
            // iLoginPath & "?thisU=" & thisUserName & "&outlookpostID=&outlook=1&edit=&OutlookEditType=&x=1
            //&OutlookGrid=1&MeetingDate=" & iFrom & "&GridBookingID=" & iGridBookingID & "&RoomID=" & iResourceID)    

            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            Outlook.AppointmentItem appItem = null;
            try
            {
                string host = UtilityManager.HostName;
                string loginPath = UtilityManager.GetLoginPath();

                // StringBuilder newpath = new StringBuilder();
                // string currentUser = UtilityManager.GetCurrentUserName();
                appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return new Uri("");
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                string port = "";
                string hostCheck = host;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = host.Split(':');
                    host = hostData[0];
                    port = hostData[1];
                }

                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //string timeFormat = "HH:mm";
                uriBuilder.Scheme = UtilityManager.HostScheme;
                uriBuilder.Host = host;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /*newpath.Append("thisU=" + currentUser);
                if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }
                newpath.Append("&outlookPostID=" + "");
                newpath.Append("&outlook=1&edit=&x=1&OutlookEditType=&OutlookGrid=1");
                newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion().ToString());
                */

                /*
                 * 
                 * Need to populate the booking and Room ID
                 * 
                 */
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);

                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToString(dateFormat);
                    }
                    else
                    {
                        if (appItem != null)
                            meetingDate = appItem.Start.ToString(dateFormat);
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetOutlookRoomBookingGridPath MeetingDate:" + ex.Message + " " + ex.StackTrace);
                }
                /* newpath.Append("&MeetingDate=" + meetingDate);
                 newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                 if (currMeeting.BookingID > 0)
                     newpath.Append("&GridBookingID=" + currMeeting.BookingID);
                 // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID

                 newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/

                uriBuilder.Query = UtilityManager.GetQueryStringParameters("", meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "1", "", "", "", "", "", "");// newpath.ToString();
                cURI = uriBuilder.Uri;
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred GetOutlookRoomBookingGridPath:" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }

            return cURI;
        }

        private Uri GetOutlookAdvancedBookingGridPath()
        {
            // iLoginPath & "?thisU=" & thisUserName & "&outlookpostID=&outlook=1&edit=&OutlookEditType=&x=1
            //&OutlookGrid=1&MeetingDate=" & iFrom & "&GridBookingID=" & iGridBookingID & "&RoomID=" & iResourceID)    

            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            Outlook.AppointmentItem appItem = null;
            try
            {
                string host = UtilityManager.HostName;
                string loginPath = UtilityManager.GetLoginPath();

                // StringBuilder newpath = new StringBuilder();
                string currentUser = UtilityManager.GetCurrentUserName();
                appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return new Uri("");
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                string port = "";
                string hostCheck = host;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = host.Split(':');
                    host = hostData[0];
                    port = hostData[1];
                }

                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //string timeFormat = "HH:mm";
                uriBuilder.Scheme = UtilityManager.HostScheme;
                uriBuilder.Host = host;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;



                /*  newpath.Append("thisU=" + currentUser);
                  if (UtilityManager.ConnectionMode() == 1)
                  {
                      newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                  }
                  newpath.Append("&outlookPostID=" + "");
                  // Set OutlooGrid to 2 so that it is the request for advanced Grid
                  newpath.Append("&outlook=1&edit=&x=1&OutlookEditType=&OutlookGrid=2");
                  newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion().ToString());
                  */

                /*
                 * 
                 * Need to populate the booking and Room ID
                 * 
                 */
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        if (appItem != null)
                        {

                            meetingDate = appItem.Start.ToUniversalTime().ToString();

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred Advance Grid Meeting Start date:" + ex.Message + " " + ex.StackTrace);
                }
                string meetingEndDate = "";
                try
                {
                    if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                        meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        meetingEndDate = appItem.End.ToUniversalTime().ToString();

                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred AdvanceGrid Meeting End date:" + ex.Message + " " + ex.StackTrace);
                }

                /* newpath.Append("&MeetingDate=" + meetingDate);
                 newpath.Append("&MeetingEndDate=" + meetingEndDate);

                 newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                 if (currMeeting.BookingID > 0)
                     newpath.Append("&GridBookingID=" + currMeeting.BookingID);

                 newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());*/
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters("", meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, "", "", "", "", ""); //newpath.ToString();
                cURI = uriBuilder.Uri;
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred Advance Grid Meeting Start date:" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }

            return cURI;
        }

        private void gridBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
            //Added calender.aspx to hide loader to fix #21918
            if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx") || e.Url.ToString().ToLower().Contains("ldap/condecouserlookup.asp?") || e.Url.ToString().ToLower().Contains("/calendar.aspx") || e.Url.ToString().ToLower().Contains("booking_resourceitem_addnote.asp"))
            {
                IsLoaderHide = true;
            }
            else
            {
                IsLoaderHide = false;
            }
            if (bookingSaved || !IsValidationValid)
            {
                // UtilityManager.MinimizeAppointmentWindow(this.CurrentOutlookWindowHandle);
                bookingSaved = false;
                IsValidationValid = true;
                Collapse(this);
                this.loaderPicture.Visible = false;
                //UtilityManager.CollapseCondecoRegion(this);
                //UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
            }
        }



        //private void btnClose_Click(object sender, EventArgs e)
        //{
        //    UtilityManager.CollapseCondecoRegion(this);
        //    UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
        //}

        ~CheckRoomGrid()
        {
            //  Deactivate();
        }

        public new void Deactivate()
        {
            //Added by Ravi Goyal for PRB0040163 (CRD-7585)
            if (!IsNoOccurrenceBookingExist)
            {
                SetAppointmentLocation();

            }
            //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
            this.gridBrowser.Navigated -= gridBrowser_Navigated;
            this.gridBrowser.DocumentCompleted -= gridBrowser_DocumentCompleted;
            this.gridBrowser.ProgressChanged -= gridBrowser_ProgressChanged;
            // this.gridBrowser.Navigating -= gridBrowser_Navigating;
        }

        private void SetAppointmentLocation()
        {
            Outlook.AppointmentItem appItem = null;
            try
            {
                appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                if (appItem == null) return;
                string newLocSimple = bookingHelper.GetBookingLocation("", appItem);
                string newLoc = appHelper.FormatLocationInCondecoStyle(newLocSimple);
                bool updateLoctaion = true;
                if (newLoc.ToLower().Contains("cancelled"))
                {
                    //UserProperties uProps = appItem.UserProperties;
                    //UserProperty prop = uProps.Find("AppointmentLocationSynced", true);
                    //if (prop != null)
                    //{
                    //    updateLoctaion = false;


                    //}
                    //UtilityManager.FreeCOMObject(prop);
                    //UtilityManager.FreeCOMObject(uProps);
                    string namedValue = "";
                    namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                    if (!String.IsNullOrEmpty(namedValue))
                    {
                        updateLoctaion = false;
                    }
                }

                if (appItem.Location == null)
                    appItem.Location = " ";
                if (!String.IsNullOrEmpty(newLoc) && updateLoctaion)
                {
                    if (!appItem.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        //appItem.Location = newLoc;
                        appHelper.SetAppointmentLocation(appItem, newLocSimple);
                        string namedValue = "";
                        namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        if (!String.IsNullOrEmpty(namedValue))
                        {
                            UserPropertiesExtension.DeleteNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                        }
                    }
                }
                //--start-Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster && !bookingHelper.isCrossDayRecurranceBooking(appItem))
                {
                    UtilityManager.LogMessage("CheckRoomGrid:SetAppointmentLocation- checking for Recurrence booking Exclusion rooms");
                    SyncManager sync = new SyncManager();
                    string postID = appHelper.GetAppointmentPostID(appItem);
                    bool IsSeriesEdited = false;
                    int bookingId = bookingHelper.GetBookingID(postID, appItem);
                    if (bookingId > 0 && (AppointmentDataInfo.GetStartDateChanged(appItem) || AppointmentDataInfo.GetEndDateChanged(appItem) || AppointmentDataInfo.GetSubjectChanged(appItem)))
                    {
                        IsSeriesEdited = true;
                    }
                    sync.SyncIndiviudallyEditedItems(appItem, this.AddinModule as AddinModule, IsSeriesEdited, false);
                    sync = null;
                }
                //End added by Ravi Goyal
                ///////UtilityManager.SetFocus(this.CurrentOutlookWindowHandle);
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in CheckRoomGrid_Deactivate" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(appItem);
            }
        }
        //private void CheckRoomGrid_ADXBeforeFormShow()
        //{
        //    this.Visible = appHelper.IsCondecoRegionVisible(this.InspectorObj as Outlook.Inspector);
        //}

        private string PostBookingData()
        {
            string postID = "";
            int bookingId = 0;
            Outlook.AppointmentItem appItem = null;
            try
            {
                appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);

                if (appItem == null) return string.Empty;
                if (UtilityManager.ConnectionMode() == 1)
                {
                    //ADDED BY CHANDRA FOR CASE WHERE USER IS NOT ENTERED IN TO CONDECO PART IN fORM aUTHENTICATION
                    if (UtilityManager.UserName.Trim().Equals("") && UtilityManager.IsCondecoContactable())
                    {
                        if (UtilityManager.GetOutlookVersion(this.OutlookAppObj) == 12)
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2007);
                        }
                        else
                        {
                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.UserLogin_FormUserNotMapped_2010_2013);
                        }
                        gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());
                        return "-1";
                    }
                    //END cHANDRA
                    else if (!UtilityManager.IsCurrentUserValid)
                    {
                        UtilityManager.ShowErrorMessage(CondecoResources.User_Not_Authorized_RoomBooking);
                        gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());

                        return "-1";
                    }
                }

                if (!ValidationManager.ValidateAppointment(appItem, (this.AddinModule as AddinModule).isItemNotInSync))
                {
                    IsValidationValid = false;
                    this.gridBrowser.Url = new Uri(UtilityManager.GetSplashPath());

                    return "-1";
                }
                (this.AddinModule as AddinModule).isItemNotInSync = false;
                ////if (string.IsNullOrEmpty(currentBookingData))
                ////    currentBookingData = bookingHelper.GetCurrentBookingData(appItem);
                bookingId = AppointmentDataInfo.GetBookingId(appItem);
                if (bookingId != 0)
                {
                    AppointmentDataInfo.SetAppItemCondecoBookingOpen(appItem, true);
                }

                if (appHelper.IsNewAppointment(appItem))
                {
                    postID = bookingHelper.HandelNewBookingRequest(appItem);
                    this.gridBrowser.Url = this.GetNewBookingPath(postID, appItem);

                    if (string.IsNullOrEmpty(appItem.Location))
                        appItem.Location = CondecoResources.Appointment_Initial_Location;

                }
                else
                {

                    postID = bookingHelper.HandleExistingBookingRquest(appItem);

                    //string currentURL = string.Empty;
                    //bool pageExist = false;
                    //bool reloadCondecoBooking = true;



                    //if (reloadCondecoBooking)
                    //{
                    this.gridBrowser.Url = this.GetExistingBookingPath(postID, appItem);

                    //}

                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in PostBookingData" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {

                UtilityManager.FreeCOMObject(appItem);
            }
            return postID;
        }

        private Uri GetNewBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            UriBuilder uriBuilder = new UriBuilder();
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();

                StringBuilder newpath = new StringBuilder();
                //string currentUser = UtilityManager.GetCurrentUserName();
                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;
                string port = "";
                string currentHost = UtilityManager.HostName; ;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                // newpath.Append("thisU=" + currentUser);
                /*if (UtilityManager.ConnectionMode() == 1)
                {
                    newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                }*/
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        if (appItem != null)
                        {

                            meetingDate = appItem.Start.ToUniversalTime().ToString();

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetNewBookingPath Meeting date:" + ex.Message + " " + ex.StackTrace);
                }
                string meetingEndDate = "";
                try
                {
                    if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                        meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        meetingEndDate = appItem.End.ToUniversalTime().ToString();

                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetNewBookingPath Meeting End date:" + ex.Message + " " + ex.StackTrace);
                }

                /*newpath.Append("&MeetingDate=" + meetingDate);
                newpath.Append("&MeetingEndDate=" + meetingEndDate);
                newpath.Append("&outlookPostID=" + postID);
                newpath.Append("&outlook=1&edit=");
                newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                newpath.Append("&x=1&OutlookGrid=2&BookingID=");*/

                // newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, -1, -1, "2", meetingEndDate, appHelper.GetAppointmentBookingType(appItem), "", "", "", "");   // newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetNewBookingPath" + ex.Message + " " + ex.StackTrace);
            }

            Uri cURI = uriBuilder.Uri;
            return cURI;
        }

        private Uri GetExistingBookingPath(string postID, Outlook.AppointmentItem appItem)
        {
            // iLoginPath & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "
            //&OutlookEditType="&iOutlookEditType&"&From="&iFrom&"&To="&iTo&"&BookingID="&iBookingID)    
            UriBuilder uriBuilder = new UriBuilder();
            Uri cURI = null;
            try
            {
                string host = UtilityManager.HostURL;
                string loginPath = UtilityManager.GetLoginPath();
                DateTime startDate = appItem.Start;
                DateTime endDate = appItem.End;
                //StringBuilder newpath = new StringBuilder();
                // check if Single Occcurrence Got a Condeco Reference or not

                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, false);
                if (currMeeting.BookingID == 0)
                {
                    cURI = GetNewBookingPath(postID, appItem);
                    return cURI;
                }
                string dateFormat = "ddd " + UtilityManager.CondecoDateFormat + " HH:mm";
                //newpath.Append(host);
                //newpath.Append(loginPath);
                uriBuilder.Scheme = UtilityManager.HostScheme;

                string port = "";
                string currentHost = UtilityManager.HostName;
                string hostCheck = currentHost;
                if (hostCheck.Contains(":"))
                {
                    string[] hostData = hostCheck.Split(':');
                    currentHost = hostData[0];
                    port = hostData[1];
                }
                uriBuilder.Host = currentHost;
                if (!string.IsNullOrEmpty(port))
                {
                    int currentPort;
                    Int32.TryParse(port, out currentPort);
                    uriBuilder.Port = currentPort;
                }
                uriBuilder.Path = loginPath;
                /* newpath.Append("thisU=" + UtilityManager.GetCurrentUserName());
                 if (UtilityManager.ConnectionMode() == 1)
                 {
                     newpath.Append("&thisP=" + UtilityManager.EncryptedPassword);
                 }*/
                string meetingDate = "";
                try
                {
                    if (currMeeting.DateFrom.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
                        meetingDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        if (appItem != null)
                        {

                            meetingDate = appItem.Start.ToUniversalTime().ToString();

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetExistingBookingPath Meeting date:" + ex.Message + " " + ex.StackTrace);
                }
                string meetingEndDate = "";
                try
                {
                    if (currMeeting.DateTo.Date != DateTime.MinValue.Date)
                    {
                        DateTime newDate = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);

                        meetingEndDate = UtilityManager.ConvertDateToTZ(newDate, currMeeting.LocationID).ToUniversalTime().ToString();

                    }
                    else
                    {
                        meetingEndDate = appItem.End.ToUniversalTime().ToString();

                    }
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred GetExistingBookingPath Meeting End date:" + ex.Message + " " + ex.StackTrace);
                }
                string dtFrm = UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat);
                string dtTo = UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat);
                // newpath.Append("&MeetingDate=" + meetingDate);
                //newpath.Append("&MeetingEndDate=" + meetingEndDate);
                //newpath.Append("&outlookPostID=" + postID);
                //newpath.Append("&outlook=1&edit=1&x=1&OutlookGrid=2");
                //newpath.Append("&OutlookEditType=" + appHelper.GetAppointmentBookingType(appItem));
                // newpath.Append("&From=" + UtilityManager.ConvertDateToTZ(startDate, currMeeting.LocationID).ToString(dateFormat));
                //  newpath.Append("&To=" + UtilityManager.ConvertDateToTZ(endDate, currMeeting.LocationID).ToString(dateFormat));
                // newpath.Append("&BookingID=" + currMeeting.BookingID);
                //newpath.Append("&RoomID=" + currMeeting.ResourceItemID);
                //if (currMeeting.BookingID > 0)
                //  newpath.Append("&GridBookingID=" + currMeeting.BookingID);
                //newpath.Append("&gridVersion=" + UtilityManager.OutlookGridVersion.ToString());

                //newpath.Append("&currrentTZ=" + UtilityManager.GetLocalTZName().Replace(" ", "|"));
                //newpath.Append("&thisDomain=" + UtilityManager.GetUserDomainName());
                // string path = host + loginPa  & "?thisU=" & thisEmail & "&outlookpostID=" & iID & "&outlook=1&edit=" & edit & "&OutlookEditType="&iOutlookEditType&"&x=1&OutlookGrid=&BookingID="&iBookingID
                uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, appHelper.GetAppointmentBookingType(appItem), "1", "", "", "");  //newpath.ToString();
                // uriBuilder.Query = UtilityManager.GetQueryStringParameters(postID, meetingDate, currMeeting.ResourceItemID, currMeeting.BookingID, "2", meetingEndDate, appHelper.GetAppointmentBookingType(appItem), "1", Convert.ToString(dtFrm), Convert.ToString(dtTo), "");  //newpath.ToString();
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in GetExistingBookingPath" + ex.Message + " " + ex.StackTrace);
            }
            cURI = uriBuilder.Uri;
            return cURI;
        }

        private void gridBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            // MessageBox.Show("Navigated to:" + e.Url);
            try
            {
                Uri curreURL = e.Url;
                if (curreURL == null) return;
                if (curreURL.Query == null) return;


                //  Added by Paritosh to fix #17779. Outlook Addin - Loading spinning animation displays unnecessarily if intelligent recurring booking selected
                if (e.Url.ToString().ToLower().Contains("condecogridcache/advancedrecurrenceroomselection.aspx"))
                {
                    IsLoaderHide = true;
                }
                else
                {
                    IsLoaderHide = false;
                }
                //Added by Paritosh to fix #17745. 
                if (e.Url.ToString().ToLower().Contains("/epdq/processmpipayment.asp?") || e.Url.ToString().ToLower().Contains("/epdq/processpayment.asp"))
                {
                    SetAppointmentLocation();
                    Outlook.AppointmentItem appItemPay = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemPay.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                    {
                        if (appItemPay != null)
                            appItemPay.Save();
                    }

                    UtilityManager.FreeCOMObject(appItemPay);

                }
                //
                //Added by Paritosh to solve isssue TP #16913
                if (curreURL.Query.ToLower().Contains("booking_saved"))
                {
                    Outlook.AppointmentItem appItemTemp = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    //Added by Paritosh to solve isssue TP #17571. 
                    if (appItemTemp.MeetingStatus != Outlook.OlMeetingStatus.olMeeting)
                    {
                        if (appItemTemp != null)
                            appItemTemp.Save();
                    }
                    UtilityManager.FreeCOMObject(appItemTemp);
                }
                //end 
                if (curreURL.Query.ToLower().Contains("booking_saved") || !IsValidationValid)
                {
                    UtilityManager.LogMessage("CheckRoomGrid - booking_saved  gridBrowser_Navigated");
                    Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                    if (appItem != null)
                    {
                        //added by paritosh 
                        if (appItem.IsRecurring)
                        {
                            List<CondecoMeeting> currMeeting = bookingHelper.GetRecurranceBookingHasDST(appItem, false);
                            foreach (CondecoMeeting cMeeting in currMeeting)
                            {
                                syncManager.SyncSeriesBookingHasDST(appItem, cMeeting); //, dsOccData, DataFilledInDS
                            }
                        }
                    }
                    //Added by Ravi Goyal for PRB0040163 (CRD-7585)
                    if (AppointmentDataInfo.GetNoOccurrenceBookingExist(appItem))
                    {
                        IsNoOccurrenceBookingExist = true;
                    }
                    //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
                   
                    //Added by Ravi Goyal for PRB0043359
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.CondecoBookingID, AppointmentDataInfo.GetBookingId(appItem).ToString());
                    //End Added by Ravi Goyal for PRB0043359
                    AppointmentDataInfo.ResetProperties(appItem);
                    UtilityManager.FreeCOMObject(appItem);
                    //end
                    IsValidationValid = true;
                    //this.GetType().InvokeMember("HideFloating", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, this, new object[] { });
                    Collapse(this);
                    this.loaderPicture.Visible = false;



                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error has occurred in gridBrowser_Navigated" + ex.Message + " " + ex.StackTrace);
            }
        }

    }
}

