﻿namespace CondecoAddinV2.Constants
{
    public static class CustomProperty
    {
       // public static string CondecoAppointmentInformation = "CondecoAppInfo";

        //For XML Named Property
        public static string CondecoAppointmentInformation = "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/CondecoAppInfo";

        //String Named Property
        public static string CondecoBookingID = "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/CondecoBookingID";

        // Boolean Value Named Property
        public static string BookRoomClicked = "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/BookRoomClicked";

        // Boolean Value Named Property
        public static string AppointmentLocationSynced = "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/AppointmentLocationSynced";

        // Below Named property is used only in OLK 2007.
        public static string NoOfPropCondecoAppInfo = "http://schemas.microsoft.com/mapi/string/{3F567CC8-72F9-4087-80E9-71DCAF3575F9}/NoOfProperty";

       // public static string RecurrDateHasDST = "http://schemas.microsoft.com/mapi/string/{3F567CC8-72F9-4086-80E9-71DCAF3575F9}/RecurrDateHasDST" ;
    
    }
}
