namespace CondecoAddinV2.DeskBooking
{
    partial class NavigationPaneDesks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
  
  
        /// <summary>
        /// Clean uppreparation[n++] = " any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
  
        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNavigationPaneDesks = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNavigationPaneDesks
            // 
            this.btnNavigationPaneDesks.BackgroundImage = global::CondecoAddinV2.Properties.Resources.ol_menu_bg;
            this.btnNavigationPaneDesks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNavigationPaneDesks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNavigationPaneDesks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNavigationPaneDesks.FlatAppearance.BorderSize = 0;
            this.btnNavigationPaneDesks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnNavigationPaneDesks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavigationPaneDesks.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNavigationPaneDesks.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnNavigationPaneDesks.Image = global::CondecoAddinV2.Properties.Resources.Desk_Booking;
            this.btnNavigationPaneDesks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNavigationPaneDesks.Location = new System.Drawing.Point(0, 0);
            this.btnNavigationPaneDesks.Name = "btnNavigationPaneDesks";
            this.btnNavigationPaneDesks.Size = new System.Drawing.Size(167, 36);
            this.btnNavigationPaneDesks.TabIndex = 1;
            this.btnNavigationPaneDesks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNavigationPaneDesks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNavigationPaneDesks.UseVisualStyleBackColor = false;
            this.btnNavigationPaneDesks.Click += new System.EventHandler(this.btnNevigationPaneDesks_Click);
            this.btnNavigationPaneDesks.MouseLeave += new System.EventHandler(this.btnNevigationPaneDesks_MouseLeave);
            this.btnNavigationPaneDesks.MouseHover += new System.EventHandler(this.btnNevigationPaneDesks_MouseHover);
            // 
            // NavigationPaneDesks
            // 
            this.ClientSize = new System.Drawing.Size(167, 36);
            this.Controls.Add(this.btnNavigationPaneDesks);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "NavigationPaneDesks";
            this.Text = "NevigationPaneDesks";
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Button btnNavigationPaneDesks;
    }
}
