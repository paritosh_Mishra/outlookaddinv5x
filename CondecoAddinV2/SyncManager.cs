using System;
using System.Collections.Generic;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Globalization;
using CondecoAddinV2.App_Resources;
using CondecoAddinV2.Constants;
using System.Data;
using CondecoAddinV2.Repository;
using System.Linq;
namespace CondecoAddinV2
{
    public sealed class SyncManager
    {
        private static AppointmentHelper appHelper = new AppointmentHelper();
        private static BookingHelper bookingHelper = new BookingHelper();
        
        public SyncManager()
        {

        }
        public void SyncAppointmentWithCondeco(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:Syncing item with Subject" + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);
            try
            {
                if (appItem == null) return;
                if (appHelper.IsNewAppointment(appItem)) return;
                if (!bookingHelper.IsCondecoBookingV2(appItem)) return;

                //if (UtilityManager.IsForceCultureENGB())
                //{
                //    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                //}

                CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
                if (!IsSyncingRequired(currMeeting, appItem)) return;

                switch (appItem.RecurrenceState)
                {
                    case Outlook.OlRecurrenceState.olApptNotRecurring:
                        SyncNormalBooking(currMeeting, appItem);
                        break;
                    case Outlook.OlRecurrenceState.olApptMaster:
                        SyncSeriesBooking(appItem, true);
                        break;
                    case Outlook.OlRecurrenceState.olApptOccurrence:
                    case Outlook.OlRecurrenceState.olApptException:
                        if (bookingHelper.IsNonCondecoOccurrence(appItem)) return;
                        bool seriesUpdated = false;
                        Outlook.AppointmentItem parentItem = appItem.Parent as Outlook.AppointmentItem;
                        if (parentItem != null)
                        {
                            //Changed by Anand : Checking return value and if series master changed then open instance is invalid and no further action possible
                            seriesUpdated = SyncSeriesBooking(parentItem, false);
                        }
                        if (seriesUpdated)
                        {
                            UtilityManager.ShowWarningMessage(CondecoResources.Series_Synced);
                            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco: ShowWarningMessage: " + CondecoResources.Series_Synced); 
                            try
                            {
                                //Added by Ravi Goyal (13-Oct-2015) to Fix PRB0041450- Outlook Crashed when open the manage room booking in single instance after series approved by Admin through web
                                if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence)
                                {
                                    if (parentItem != null)
                                    {
                                        appItem.Location = parentItem.Location;
                                        appItem.Start = parentItem.Start;
                                        appItem.End = parentItem.End;
                                        appItem.Subject = parentItem.Subject;
                                        UtilityManager.FreeCOMObject(parentItem);
                                        UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco-Series sync completed successfully after opening occurence of meeting"); 
                                    }
                                }
                                else
                                {
                                    UtilityManager.FreeCOMObject(parentItem);
                                }
                                //End Added by Ravi Goyal (13-Oct-2015)
                            }
                                //Following commented by Ravi Goyal (13-Oct-2015) for PRB0041450
                                //added by Anand on 6-Mar-2013 for issue 12179
                                //Outlook.Inspector ins = appItem.GetInspector;
                                //((Outlook._Inspector)ins).Close(Outlook.OlInspectorClose.olDiscard);
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error displayed is " + ex.Message);
                                UtilityManager.LogMessage("Error StackTrace " + ex.StackTrace);
                            }
                        }
                        //End Change
                        else
                            UtilityManager.FreeCOMObject(parentItem);
                            SyncOccurrenceBooking(currMeeting, appItem);
                        break;
                    default:
                        //SyncOccurrenceBooking(appItem); 
                        break;
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred while syning appontment:" + ex.Message);
                UtilityManager.LogMessage("Error stacked trace while syning appontment:" + ex.StackTrace);
            }

            UtilityManager.LogMessage("SyncManager.SyncAppointmentWithCondeco:*********Finished********");
        }

        private void SyncNormalBooking(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:Syncing item with Subject: " + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);

            bool itemSynced = false;

            if (currMeeting.BookingID == 0) return;
            itemSynced = SyncItem(currMeeting, appItem);
            ClearAppointmentDataIfRequired(currMeeting, appItem);
            if (itemSynced)
            {

                UtilityManager.LogMessage("SyncManager.SyncNormalBooking: Appointment has been synced");

                UtilityManager.ShowWarningMessage(CondecoResources.Appointment_Synced);
            }

            UtilityManager.LogMessage("SyncManager.SyncNormalBooking:*********Finished********");
        }

        //Changed by Anand : Added bool return type to identify that series master had been modified.
        public bool SyncSeriesBooking(Outlook.AppointmentItem appItem, bool showMessage)
        {
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:*********Started********");
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item with Subject: " + appItem.Subject + ":Start Date:" + appItem.Start + "End Date" + appItem.End);
            bool itemUpdated = false;
            // if (appItem.RecurrenceState == OlRecurrenceState.olApptException || appItem.RecurrenceState == OlRecurrenceState.olApptOccurrence)
            CondecoMeeting currMeeting = bookingHelper.GetCondecoSeriesMeeting(appItem, false);
            // need to implement rejected bookings


            //Sync the Subject
            //Changed by Anand : Added trim in comparison as most of the time its invalidating the object due to of space
            string MeetingTitle =UtilityManager.CheckHtmlDecode(currMeeting.MeetingTitle.Trim());
            if (!string.IsNullOrEmpty(currMeeting.MeetingTitle) && !appItem.Subject.Trim().ToLower().Equals(MeetingTitle.Trim().ToLower()))
            {
                appItem.Subject = MeetingTitle;
                itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item Subject to " + currMeeting.MeetingTitle);
            }//End Change
            if (appItem.Location == null)
                appItem.Location = " ";

            //Sync the Location
            string newLoc = appHelper.FormatLocationInCondecoStyle(currMeeting.MainLocation);
            if (!string.IsNullOrEmpty(newLoc) && !appItem.Location.ToLower().Contains(newLoc.ToLower()))
            {
                //appItem.Location = currMeeting.MainLocation;
                bool locationUpdated = appHelper.SetAppointmentLocation(appItem, currMeeting.MainLocation);
                if (locationUpdated) itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item location to " + currMeeting.MainLocation);
            }
            

            List<CondecoMeeting> currMeetinglist = bookingHelper.GetRecurranceBookingHasDST(appItem,true);
            if (currMeetinglist.Count > 0)
            {

                foreach (CondecoMeeting cMeeting in currMeetinglist)
                {
                    bool bln = UpdateSeriesBookingHasDST(appItem, cMeeting);
                    if (bln)
                    {
                        itemUpdated = true;
                    }
                }
                //if (UpdateStartDateTimeInSeries(rpItem, currMeeting))
                //    itemUpdated = true;
                //if (DSTUpdateEndDateTimeInSeries(rpItem, currMeeting))
                //    itemUpdated = true;
                //foreach (CondecoMeeting cMeeting in currMeetinglist)
                //{
                //   bool bln= UpdateSeriesBookingHasDST(appItem, cMeeting);
                //   if (bln)
                //   {
                //       itemUpdated = true;
                //   }
                //}
            }
            else
            {
                Outlook.RecurrencePattern rpItem = appItem.GetRecurrencePattern();
                if (UpdateStartDateTimeInSeries(rpItem, currMeeting))
                    itemUpdated = true;
                if (UpdateEndDateTimeInSeries(rpItem, currMeeting))
                    itemUpdated = true;

                UtilityManager.FreeCOMObject(rpItem);
            }



            if (itemUpdated)
            {
                try
                {
                    appItem.Save();
                    UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Series Item changes saved sucessfully");
                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while SyncSeriesBooking appontment:" + ex.Message);
                    UtilityManager.LogMessage("Error stacked trace while SyncSeriesBooking appontment:" + ex.StackTrace);
                }

                ClearAppointmentDataIfRequired(currMeeting, appItem);

                if (showMessage)
                    UtilityManager.ShowWarningMessage(CondecoResources.Series_Synced);
            }
            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:*********Finished********");
            return itemUpdated;
        }

        private static bool UpdateEndDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            //long meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            long meetingTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            DateTime currMeetingItemSeriesEnd = new DateTime(meetingTicks);
            DateTime condecoSeriesEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesEnd,
                                                                                         currMeeting.LocationID,
                                                                                         currMeeting.OriginalTZ);
            if (condecoSeriesEndTimeInLocalTz.TimeOfDay != rpItem.EndTime.TimeOfDay)
            {
                rpItem.EndTime = condecoSeriesEndTimeInLocalTz;
                itemUpdated = true;
            }
            //if (condecoSeriesEndTimeInLocalTz.Date != rpItem.PatternEndDate.Date)
            //{
            //    rpItem.PatternEndDate = condecoSeriesEndTimeInLocalTz.Date;
            //    itemUpdated = true;
            //}
            return itemUpdated;
        }

        private static bool DSTUpdateEndDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            long currentTicks = rpItem.PatternEndDate.Date.Ticks + rpItem.EndTime.TimeOfDay.Ticks;
            DateTime itemSeriesEnd = new DateTime(currentTicks);
            itemSeriesEnd = UtilityManager.ConvertDateToTZ(itemSeriesEnd, currMeeting.LocationID, currMeeting.OriginalTZ);
            currentTicks = rpItem.PatternEndDate.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            DateTime condecoSeriesEnd = new DateTime(currentTicks);

            if (itemSeriesEnd.CompareTo(condecoSeriesEnd) != 0)
            {
                DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID, currMeeting.OriginalTZ);
                if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                {
                    rpItem.EndTime = newDt;
                    itemUpdated = true;
                }
            }
            return itemUpdated;
        }

        private static bool UpdateStartDateTimeInSeries(Outlook.RecurrencePattern rpItem, CondecoMeeting currMeeting)
        {
            bool itemUpdated = false;
            //long meetingTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            long meetingTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            DateTime currMeetingItemSeriesStart = new DateTime(meetingTicks);
            DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                                                                                         currMeeting.LocationID,
                                                                                         currMeeting.OriginalTZ);
            if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
            {
                rpItem.StartTime = condecoSeriesStartTimeInLocalTz;
                itemUpdated = true;
            }
            //if (condecoSeriesStartTimeInLocalTz.Date != rpItem.PatternStartDate.Date)
            //{
            //    rpItem.PatternStartDate = condecoSeriesStartTimeInLocalTz.Date;
            //    itemUpdated = true;
            //}
            return itemUpdated;
        }

        //private static bool UpdateStartDateTimeInSeries(RecurrencePattern rpItem, CondecoMeeting currMeeting)
        //{
        //    bool itemUpdated = false;
        //    long currentTicks = rpItem.PatternStartDate.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
        //    DateTime itemSeriesStart = new DateTime(currentTicks);
        //    itemSeriesStart = UtilityManager.ConvertDateToTZ(itemSeriesStart, currMeeting.LocationID);
        //    currentTicks = rpItem.PatternStartDate.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
        //    DateTime condecoSeriesStart = new DateTime(currentTicks);

        //    if (itemSeriesStart.CompareTo(condecoSeriesStart) != 0)
        //    {
        //        DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeFrom, currMeeting.LocationID);
        //        if (newDt.TimeOfDay.CompareTo(rpItem.StartTime.TimeOfDay) != 0)
        //        {
        //            rpItem.StartTime = newDt;
        //            itemUpdated = true;
        //            UtilityManager.LogMessage("SyncManager.SyncSeriesBooking:Syncing item series start time to " + newDt);
        //        }
        //    }
        //    return itemUpdated;
        //}

        private void SyncOccurrenceBooking(CondecoMeeting curMeeting, Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:*********Started********");
            CondecoMeeting currMeeting = bookingHelper.GetCurrentCondecoMeeting(appItem, true);
            bool itemSynced = false;

            bool result = false;
            if (currMeeting.DeleteBooking || currMeeting.Edited || currMeeting.Approved || currMeeting.BookingRejected)
            {
                result = true;
            }
            if (!result) return;
            //Sync the Subject
            itemSynced = this.SyncItem(currMeeting, appItem);
            if (itemSynced)
            {
                UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:Single Occurrence Synced sucessfully");
                ClearAppointmentDataIfRequired(currMeeting, appItem);
                UtilityManager.ShowWarningMessage(CondecoResources.Appointment_Synced);
            }
            UtilityManager.LogMessage("SyncManager.SyncOccurrenceBooking:*********Finished********");
        }
        public bool SyncItem(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            //bool doSyncing = false;
            bool itemUpdated = false;
            bool itemlocationUpdated = false;
            bool isCrossDayBooking = false;
            if (bookingHelper.isCrossDayRecurranceBooking(appItem))
            {
                isCrossDayBooking = true;
            }
            UtilityManager.LogMessage("SyncManager.SyncItem:*********Started********");
            //Added by Anand On 1-Feb-2013 if setting subject empty its coming null and giving error below.
            if (appItem.Subject == null) appItem.Subject = " ";
            string MeetingTitle = UtilityManager.CheckHtmlDecode(currMeeting.MeetingTitle.Trim());
            if (!string.IsNullOrEmpty(currMeeting.MeetingTitle) && !appItem.Subject.Trim().ToLower().Equals(MeetingTitle.Trim().ToLower()))
            {
                appItem.Subject = MeetingTitle;
                //appItem.Subject = currMeeting.MeetingTitle.Replace("&#39;", "'").Replace("&#38;", "&");
                itemUpdated = true;
            }
            if (appItem.Location == null)
                appItem.Location = " ";
            string newLoc = appHelper.FormatLocationInCondecoStyle(currMeeting.MainLocation);
            //Sync the Location
            UtilityManager.LogMessage("SyncManager.SyncItem:Going to Compare Appointment Location: " + appItem.Location + "With condeco Meeting Lcoation:" + currMeeting.MainLocation);
            if ((!string.IsNullOrEmpty(newLoc) && !appItem.Location.ToLower().Contains(newLoc.ToLower())) || appItem.IsRecurring)
            {
                //Make the item updated to false if location are same but need to change in the UI
                if (appItem.Location.ToLower().Contains(newLoc.ToLower()) && !itemUpdated)
                {
                    itemlocationUpdated = false;
                }
                if (!itemlocationUpdated)
                {
                    //appItem.Location = currMeeting.MainLocation;
                    bool locationUpdated = appHelper.SetAppointmentLocation(appItem, currMeeting.MainLocation);
                    //Changed by Anand 10_Jan_2013 for issue as converting occurance as exception when opening occurance TP 10476 issue.
                    if(locationUpdated) itemlocationUpdated = true;
                    UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item location to " + currMeeting.MainLocation);
                }
            }
            DateTime DatetoDBDateFrom;
            DateTime DatetoDBTimeFrom;
            //Below code changed for TP issue 9198
            if (isCrossDayBooking)
            {
              //  DatetoDBDateFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
              //  DatetoDBTimeFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeFrom, currMeeting.LocationID);
                DatetoDBDateFrom = currMeeting.DateFrom;
                DatetoDBTimeFrom = currMeeting.TimeFrom;
            }
            else
            {
                DatetoDBDateFrom = currMeeting.DateFrom;
                DatetoDBTimeFrom = currMeeting.TimeFrom;
            }
           // DateTime meetingStart = new DateTime(currMeeting.DateFrom.Year, currMeeting.DateFrom.Month, currMeeting.DateFrom.Day, currMeeting.TimeFrom.Hour, currMeeting.TimeFrom.Minute, currMeeting.TimeFrom.Second);
            DateTime meetingStart = new DateTime(DatetoDBDateFrom.Year, DatetoDBDateFrom.Month, DatetoDBDateFrom.Day, DatetoDBTimeFrom.Hour, DatetoDBTimeFrom.Minute, DatetoDBTimeFrom.Second);
            DateTime appStart = UtilityManager.ConvertDateToTZ(appItem.Start,currMeeting.LocationID);
            if (appStart.CompareTo(meetingStart) != 0)
            {
                //appItem.Start = meetingStartTimeInLocalTz;
                appItem.Start = UtilityManager.ConvertDateToLocalTZ(meetingStart, currMeeting.LocationID);
                itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item Start Time");
            }
            DateTime DatetoDBDateTo;
            DateTime DatetoDBTimeTO;
            if (isCrossDayBooking)
            {
               // DatetoDBDateTo = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateTo, currMeeting.LocationID);
                //DatetoDBTimeFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID);
                DatetoDBDateTo = currMeeting.DateTo;
                DatetoDBTimeTO = currMeeting.TimeTo;
            }
            else
            {
                DatetoDBDateTo = currMeeting.DateTo;
                DatetoDBTimeTO = currMeeting.TimeTo;
            }
           // DateTime meetingEnd = new DateTime(currMeeting.DateTo.Year, currMeeting.DateTo.Month, currMeeting.DateTo.Day, currMeeting.TimeTo.Hour, currMeeting.TimeTo.Minute, currMeeting.TimeTo.Second);
            DateTime meetingEnd = new DateTime(DatetoDBDateTo.Year, DatetoDBDateTo.Month, DatetoDBDateTo.Day, DatetoDBTimeTO.Hour, DatetoDBTimeTO.Minute, DatetoDBTimeTO.Second);
            DateTime appEnd = UtilityManager.ConvertDateToTZ(appItem.End, currMeeting.LocationID);
            if (appEnd.CompareTo(meetingEnd) != 0)
            {
                appItem.End = UtilityManager.ConvertDateToLocalTZ(meetingEnd, currMeeting.LocationID);
                itemUpdated = true;
                UtilityManager.LogMessage("SyncManager.SyncItem:Syncing item End Time");
            }

            if (itemUpdated || itemlocationUpdated)
            {
                try
                {
                    if(! (AppointmentDataInfo.GetAppItemCondecoBookingOpen(appItem)))
                    {
                        appItem.Save();
                    }
                    UtilityManager.LogMessage("Item changes Synced sucessfully");

                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while SyncItem appontment:" + ex.Message);
                    UtilityManager.LogMessage("Error stacked trace while SyncItem appontment:" + ex.StackTrace);
                }
            }
            UtilityManager.LogMessage("SyncManager.SyncItem:*********Finished********");
            return itemUpdated;
        }

        public void SyncIndiviudallyEditedItems(Outlook.AppointmentItem appItem, AddinModule CurrentAddinModule = null, bool IsSeriesEdited = false, bool IsSeriesDeleted = false)
        {
           
            UtilityManager.LogMessage("SyncManager-> Inside SyncIndiviudallyEditedItems");
            //bool result = true;
            //if (!bookingHelper.IsCondecoBookingV2(appItem)) return;
            UtilityManager.LogMessage("SyncManager-> Current Item is a condeco Booking");
            if (appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            {
                // List<CondecoMeeting> editedMeetings = bookingHelper.GetIndividuallyEditedItems(appItem);
                List<DateTime> missingDates = new List<DateTime>();
                int locationID = 0;
                UtilityManager.LogMessage("SyncManager-> Going to Get IndividualyEdited and Missing Items");
                List<CondecoMeeting> editedMeetings = bookingHelper.GetIndividuallyEditedAndMissingItems(appItem, ref missingDates, ref locationID);
                int count = missingDates.Count;

                if (editedMeetings.Count == 0 && missingDates.Count == 0) return;
                UtilityManager.LogMessage("SyncManager-> Number of Missing Dates Found" + count + "::Number of Edited Items " + editedMeetings.Count);
                Outlook.RecurrencePattern recPattern = appItem.GetRecurrencePattern();

                //changed by Anand 22-Jan-13 for TP issue 9870/10220  
                if (missingDates.Count > 0)
                    CurrentAddinModule.MissingAppointmentlist = new List<Outlook.AppointmentItem>();
                HanldeMissingItems(recPattern, missingDates, locationID,IsSeriesDeleted,CurrentAddinModule);
                //end changed by anand

                if (editedMeetings.Count > 0)
                {
                    //if (!bookingHelper.isCrossDayRecurranceBooking(appItem))
                    //{
                    HanldeIndiviudalyEditedItems(appItem, recPattern, editedMeetings, CurrentAddinModule, IsSeriesEdited, IsSeriesDeleted);
                    // }

                }
                //changed by Anand 22-Jan-13 for TP issue 9870/10220  
                //if (missingDates.Count > 0)
                //    HanldeMissingItems(appItem, recPattern, missingDates, locationID);
                //end changed by anand
                UtilityManager.FreeCOMObject(recPattern);
                
            }
        }
        private void HanldeIndiviudalyEditedItems(Outlook.AppointmentItem appItem, Outlook.RecurrencePattern recPattern, List<CondecoMeeting> editedMeetings, AddinModule CurrentAddinModule = null, bool IsSeriesEdited = false, bool IsSeriesDeleted = false)
        {
            bool isCrossDayBooking = false;
         
            if(bookingHelper.isCrossDayRecurranceBooking(appItem))
            {
                isCrossDayBooking = true;
            }

            if (CurrentAddinModule != null && CurrentAddinModule.HostMajorVersion>= 14)
            {
               CurrentAddinModule.exclusionlist = new List<Outlook.AppointmentItem>();
               CurrentAddinModule.EditedOccurenceDate = new List<DateTime>();
               CurrentAddinModule.AppMasterLoc=new List<string>();
            }
                foreach (CondecoMeeting cMeeting in editedMeetings)
                {
                    DateTime occurrenceData = DateTime.MinValue;
                    DateTime DatetoDB;
                    Outlook.AppointmentItem currAppointment = null;
                    long currentTicks;
                    // case 2 when room is updated
                    try
                    {
                        try
                        {
                            if (isCrossDayBooking)
                            {
                                DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.OriginalDateFrom, cMeeting.LocationID);
                            }
                            else
                            {
                                DatetoDB = cMeeting.OriginalDateFrom;
                            }
                            // currentTicks = cMeeting.OriginalDateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                            occurrenceData = new DateTime(currentTicks);
                            //occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                            currAppointment = recPattern.GetOccurrence(occurrenceData);
                        }
                        catch (System.Exception ex)
                        {
                            UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                            UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                        }

                        //case 1 When Series Time is changed
                        if (currAppointment == null)
                        {
                            try
                            {
                                if (isCrossDayBooking)
                                {
                                    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                                }
                                else
                                {
                                    DatetoDB = cMeeting.DateFrom;
                                }
                                // currentTicks = cMeeting.DateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                                currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                                occurrenceData = new DateTime(currentTicks);
                                occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                                currAppointment = recPattern.GetOccurrence(occurrenceData);
                                //currAppointment = GetAppointmentFromRecPatternBasedOnDateAndTime(recPattern, currentTicks, cMeeting.LocationID);
                            }
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                                UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                            }
                        }
                        // case 4 When room and times are changed acroos countries booking
                        if (currAppointment == null)
                        {
                            try
                            {
                                if (isCrossDayBooking)
                                {
                                    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                                }
                                else
                                {
                                    DatetoDB = cMeeting.DateFrom;
                                }
                                //currentTicks = cMeeting.DateFrom.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                                currentTicks = DatetoDB.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                                occurrenceData = new DateTime(currentTicks);
                                // occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                                currAppointment = recPattern.GetOccurrence(occurrenceData);
                            }
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                                UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                            }
                        }

                        // case 2 when room is updated
                        if (currAppointment == null)
                        {
                            try
                            {
                                if (isCrossDayBooking)
                                {
                                    DatetoDB = UtilityManager.ConvertDateToLocalTZ(cMeeting.DateFrom, cMeeting.LocationID);
                                }
                                else
                                {
                                    DatetoDB = cMeeting.DateFrom;
                                }
                                //currentTicks = cMeeting.DateFrom.Date.Ticks + cMeeting.TimeFrom.TimeOfDay.Ticks;
                                currentTicks = DatetoDB.Date.Ticks + cMeeting.TimeFrom.TimeOfDay.Ticks;
                                occurrenceData = new DateTime(currentTicks);
                                occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, cMeeting.LocationID);
                                currAppointment = recPattern.GetOccurrence(occurrenceData);
                            }
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                                UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                            }

                        }

                        if (currAppointment != null)
                        {
                            try
                            {
                                //Added by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402 (Only for >outlook 2007, not for outlook 2007 or lesser versions.
                                bool IsException = false;
                                if (currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
                                {
                                    IsException = true;
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit/delete mode,existing RecurrenceState state is olApptException");
                                }
                                if (IsSeriesEdited)
                                {
                                    //Added by Ravi Goyal- creating exception forcefully so that item should be exclude from update during entire series edit
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit mode, RecurrenceState going to change olApptException forcefully");
                                    IsException = true;
                                    currAppointment.Start = currAppointment.Start.AddMinutes(1.0);
                                    currAppointment.End = currAppointment.End.AddMinutes(1.0);
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in Edit mode, RecurrenceState state is changed to olApptException");
                                }
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-SyncItem Started");
                                if (IsSeriesDeleted && !IsException)
                                {
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in delete mode, RecurrenceState state is olApptOccurence");
                                }
                                else
                                {
                                    SyncItem(cMeeting, currAppointment);
                                }
                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-SyncItem completed");
                                if (CurrentAddinModule != null)
                                {
                                    if (currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                                    {
                                        if (CurrentAddinModule.HostMajorVersion < 14)
                                        {
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Outlook version is" + CurrentAddinModule.HostMajorVersion);
                                            UtilityManager.FreeCOMObject(currAppointment);
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-UtilityManager.FreeCOMObject() completed,object released successfully");
                                        }
                                        else if (CurrentAddinModule.HostMajorVersion >= 14 && !IsSeriesEdited && !IsSeriesDeleted)
                                        {
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Outlook version is" + CurrentAddinModule.HostMajorVersion);
                                            CurrentAddinModule.exclusionlist.Add(currAppointment);
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Items added to exclusion list (not sent yet)");
                                        }
                                        else if (IsSeriesDeleted && !IsException)
                                        {
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series deleted and Item in not exception");
                                            CurrentAddinModule.AppMasterLoc.Add(cMeeting.MainLocation);
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-AppMasterLoc set as" + cMeeting.MainLocation);
                                        }
                                        //Added by Ravi Goyal to fix Recurring series edit issue, so incase if not only location is updated, addin will send seperate emails for each edited meeting occurence
                                        else if (IsSeriesEdited && (IsException || currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptException || currAppointment.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence))
                                        {
                                            UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode and Item is Exception");
                                            //.. so if not only location updated, send seperate email for each edited meeting to sync the Attendees and host calendar.
                                            if (!AppointmentDataInfo.GetIsOnlySeriesLocationUpdated(appItem) && !cMeeting.DeleteBooking)
                                            {
                                                CurrentAddinModule.EditedOccurenceDate.Add(currAppointment.Start);
                                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode and Item added to List<> EditedOccurenceDate ");
                                            }
                                            //..if only location updated do nothing, outlook will send seperate email.
                                            else
                                            {
                                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode ,only location updated so do nothing, outlook will send seperate email.");
                                                UtilityManager.FreeCOMObject(currAppointment);
                                                UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Series in edit mode ,UtilityManager.FreeCOMObject() completed,object released successfully");
                                            }
                                        }
                                        else
                                        {
                                            UtilityManager.FreeCOMObject(currAppointment);
                                        }
                                    }
                                    else
                                    {
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-Appointment is not meeting (no attendee), releasing the com Object");
                                        UtilityManager.FreeCOMObject(currAppointment);
                                        UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-object released successfully");
                                    }
                                }
                                else
                                {
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-CurrentAddinModule is Null, releasing the com Object");
                                    UtilityManager.FreeCOMObject(currAppointment);
                                    UtilityManager.LogMessage("SyncManager.HandleIndividuallyEditedItems()-object released successfully");
                                }
                            }
                            catch (System.Exception ex)
                            {
                                UtilityManager.LogMessage("Error Occurred while HanldeIndiviudalyEditedItems appontment:" + ex.Message);
                                UtilityManager.LogMessage("Error stacked trace while HanldeIndiviudalyEditedItems appontment:" + ex.StackTrace);
                            }
                        }
                    }
                    finally
                    {
                        //UtilityManager.FreeCOMObject(currAppointment);
                    }
                }
        }
        
        //Below method Edited by Ravi Goyal for PRB0040163 (CRD-7585)
        private void HanldeMissingItems(Outlook.RecurrencePattern recPattern, List<DateTime> missingAppointments, int locationID,bool IsDeleted=false, AddinModule CurrentAddinModule = null)
        {
            UtilityManager.LogMessage("HanldeMissingItems-> Inside Missing Items for total items:"+missingAppointments.Count + "for location"+locationID);
            
            foreach (var missingDate in missingAppointments)
            {
                DateTime occurrenceData = DateTime.MinValue;
                Outlook.AppointmentItem currAppointment = null;
                UtilityManager.LogMessage("Going to check appointment for" + missingDate.ToString());
                int CompareValue = 0;
                //case 1 When Series Time is changed
                long currentTicks;
                try
                {
                    currentTicks = missingDate.Date.Ticks + recPattern.StartTime.TimeOfDay.Ticks;
                    occurrenceData = new DateTime(currentTicks);
                    //commented below by Anand 22-Jan-13 for TP issue 9870/10220 
                    //as its using recpattern so should check current appointmeht
                    //occurrenceData = UtilityManager.ConvertDateToLocalTZ(occurrenceData, locationID);
                    //also check for passed bookings conditions added for issue TP 10699
                    //if (DateTime.Compare(occurrenceData, DateTime.Now) == -1)
                    //    currAppointment = recPattern.GetOccurrence(occurrenceData);

                    CompareValue = DateTime.Compare(occurrenceData, DateTime.Now);
                    currAppointment = recPattern.GetOccurrence(occurrenceData);

                    //end comment by anand

                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("Error Occurred while HanldeMissingItems appontment:" + ex.Message);
                    UtilityManager.LogMessage("Error stacked trace while HanldeMissingItems appontment:" + ex.StackTrace);
                }


                if (currAppointment != null)
                {
                    try
                    {
                        UtilityManager.LogMessage("Going to Set location for appointment:" + missingDate.ToString());
                       // this.SyncItem(cMeeting, currAppointment);
                       //Changed below as its making location to blank Anand 22-Jan-13 for TP issue 9870/10220  
                       // old code currAppointment.Location = string.Empty;
                        if (CompareValue < 0) //condition for passed booking
                            currAppointment.Location = currAppointment.Location;
                        else if (CompareValue >= 0) //condition for skipped booking
                            currAppointment.Location = " ";
                        //End Added by Anand 22-Jan-13 for TP issue 9870/10220
                        currAppointment.Save();
                        //Edited by Ravi Goyal for PRB0040163 (CRD-7585)
                        if (IsDeleted)
                        {
                            UtilityManager.FreeCOMObject(currAppointment);
                        }
                        else if (CurrentAddinModule != null && currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olMeeting)
                        {
                            if (currAppointment.Location.ToLower().Contains("removed"))
                            {
                                UtilityManager.FreeCOMObject(currAppointment);
                            }
                            else
                            {
                                CurrentAddinModule.MissingAppointmentlist.Add(currAppointment);
                            }
                        }
                        else
                        {
                            UtilityManager.FreeCOMObject(currAppointment);
                        }
                        //End Edited by Ravi Goyal for PRB0040163 (CRD-7585)
                        UtilityManager.LogMessage("location set for appointment:" + missingDate.ToString());
                        //End Added by Ravi Goyal for PRB0040163 (CRD-7585)
                        UtilityManager.LogMessage("location set for appointment:" + missingDate.ToString());
                    }
                    catch (System.Exception ex)
                    {
                        UtilityManager.LogMessage("Error Occurred while HanldeMissingItems appontment:" + ex.Message);
                        UtilityManager.LogMessage("Error stacked trace while HanldeMissingItems appontment:" + ex.StackTrace);
                    }
                }

            }
            UtilityManager.LogMessage("HanldeMissingItems-> Finish Missing Items" );
        }
        public bool IsSyncingRequired(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            bool result = true;
            bool doSyncing = false;
            if (currMeeting.DeleteBooking || currMeeting.BookingRejected)
            {
                if (currMeeting.BookingRejected)
                {
                    if (!appItem.Location.ToLower().Contains(CondecoResources.Appointment_Rejected_Title.ToLower()))
                    {
                        doSyncing = true;

                    }
                    //return itemUpdated;
                }
                if (currMeeting.DeleteBooking)
                {
                    if (!appItem.Location.ToLower().Contains(CondecoResources.Appointment_Cancelled_Title.ToLower()))
                    {
                        doSyncing = true;
                    }
                    // return itemUpdated;
                }

                string namedValue = "";
                namedValue = UserPropertiesExtension.GetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced);
                //Below Commented by Paritosh to fix #20475
                if (String.IsNullOrEmpty(namedValue))
                {
                    UserPropertiesExtension.SetNamedPropertyRecord(appItem, CustomProperty.AppointmentLocationSynced, Convert.ToString("True"));
                 // appItem.Save();
                }
                //else
                //{
                //    doSyncing = false;
                //}
                if (doSyncing)
                {

                    result = true;
                }
                else
                {
                    result = false;
                }

                // return itemUpdated;
            }
            if (result)
            {
                if ((appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptException || appItem.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence) && DateTime.Now >= appItem.Start)
                {
                    result = false;
                }
            }
            return result;
        }
        public bool ClearAppointmentDataIfRequired(CondecoMeeting currMeeting, Outlook.AppointmentItem appItem)
        {
            bool result = false;
            if (currMeeting.BookingRejected || currMeeting.DeleteBooking)
            {
                appHelper.ClearAppointmentData(appItem);
                result = true;
            }
            return result;
        }
        public void SyncSeriesBookingHasDST(Outlook._AppointmentItem appitem, CondecoMeeting currMeeting)
        {
           // ,DataSet occdata,bool IsEdit
            DateTime occurrenceData = DateTime.MinValue;
            DateTime condecoSeriesStartTimeInLocalTz = DateTime.MinValue;
            Outlook.AppointmentItem currAppointment = null;
            Outlook.RecurrencePattern rpItem = null;
            try
            {
                UtilityManager.LogMessage("SyncManager.SyncSeriesBookingHasDST:*********Started********");
                rpItem = appitem.GetRecurrencePattern();
                long currentTicks;
                try
                {
                    currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                    DateTime currMeetingItemSeriesStart = new DateTime(currentTicks);
                    condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                                                                                          currMeeting.LocationID,
                                                                                          currMeeting.OriginalTZ);
                    //  if (IsEdit)
                    //  {

                    //for (int i = 0; i < occdata.Tables[0].Rows.Count; i++)
                    //{
                    //if (Convert.ToInt32(occdata.Tables[0].Rows[i]["BookingID"]) == currMeeting.BookingID)
                    //{
                    // DateTime dtime = Convert.ToDateTime(occdata.Tables[0].Rows[i]["OccuranceDate"]);
                    currentTicks = currMeeting.DateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                    DateTime Instancedate = new DateTime(currentTicks);
                    currAppointment = rpItem.GetOccurrence(Instancedate);
                    currAppointment.Start = condecoSeriesStartTimeInLocalTz;
                    //  break;
                    //}
                    //}
                    //}
                    //else
                    //{



                    //    currentTicks = currMeeting.DateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                    //    occurrenceData = new DateTime(currentTicks);
                    //    currAppointment = rpItem.GetOccurrence(occurrenceData);
                    //    currAppointment.Start = condecoSeriesStartTimeInLocalTz;

                    //}
                }
                catch
                {

                    //currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                    //occurrenceData = new DateTime(currentTicks);
                    //DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(occurrenceData,currMeeting.LocationID,currMeeting.OriginalTZ);
                    //currAppointment = rpItem.GetOccurrence(occurrenceData);
                    //currAppointment.Start = condecoSeriesStartTimeInLocalTz;
                }
                try
                {
                    currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                    DateTime currMeetingEnddate = new DateTime(currentTicks);
                    DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeetingEnddate, currMeeting.LocationID, currMeeting.OriginalTZ);
                    currAppointment.End = newDt;
                }
                catch
                {
                }
                currAppointment.Save();
            }
            catch
            {
            }
            finally
            {
                UtilityManager.FreeCOMObject(rpItem);
                UtilityManager.FreeCOMObject(currAppointment);
            }
           // return condecoSeriesStartTimeInLocalTz;
        }
        private static bool UpdateSeriesBookingHasDST(Outlook.AppointmentItem appitem, CondecoMeeting currMeeting)
        {
            bool isCrossDayBooking = false;
            if (bookingHelper.isCrossDayRecurranceBooking(appitem))
            {
                isCrossDayBooking = true;
            }
            
            bool itemUpdated = false;
            UtilityManager.LogMessage("SyncManager.UpdateSeriesBookingHasDST:*********Started********");
            Outlook.AppointmentItem currAppointment = null;
            Outlook.RecurrencePattern rpItem = appitem.GetRecurrencePattern();
            DateTime occurrenceData = DateTime.MinValue;
            long currentTicks;
            try
            {
                DateTime CrossdateFrom;
                if (isCrossDayBooking)
                {
                    CrossdateFrom = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
                }
                else
                {
                    CrossdateFrom = currMeeting.DateFrom;
                }
                //currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                currentTicks = CrossdateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
                DateTime currMeetingItemSeriesStart = new DateTime(currentTicks);
                DateTime condecoSeriesStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(currMeetingItemSeriesStart,
                                                                                        currMeeting.LocationID,
                                                                                        currMeeting.OriginalTZ);


               // currentTicks = currMeeting.DateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                currentTicks = CrossdateFrom.Date.Ticks + rpItem.StartTime.TimeOfDay.Ticks;
                occurrenceData = new DateTime(currentTicks);
                currAppointment = rpItem.GetOccurrence(occurrenceData);
                if (condecoSeriesStartTimeInLocalTz.TimeOfDay != rpItem.StartTime.TimeOfDay)
                {
                    currAppointment.Start = condecoSeriesStartTimeInLocalTz;
                    itemUpdated = true;
                }

                try
                {

                    DateTime CrossdateTo;
                    if (isCrossDayBooking)
                    {
                        CrossdateTo = UtilityManager.ConvertDateToLocalTZ(currMeeting.DateFrom, currMeeting.LocationID);
                    }
                    else
                    {
                        CrossdateTo = currMeeting.DateFrom;
                    }
                    //currentTicks = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                    currentTicks = CrossdateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
                    DateTime currMeetingEnddate = new DateTime(currentTicks);
                    DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeetingEnddate, currMeeting.LocationID, currMeeting.OriginalTZ);
                    //if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                    //{
                    //    currAppointment.End = newDt;
                    //    itemUpdated = true;
                    //}
                    if (newDt.TimeOfDay.CompareTo(currAppointment.End.TimeOfDay) != 0)
                    {
                        currAppointment.End = newDt;
                        itemUpdated = true;
                    }

                    //if (itemSeriesEnd.CompareTo(condecoSeriesEnd) != 0)
                    //{
                    //    DateTime newDt = UtilityManager.ConvertDateToLocalTZ(currMeeting.TimeTo, currMeeting.LocationID, currMeeting.OriginalTZ);
                    //    if (newDt.TimeOfDay.CompareTo(rpItem.EndTime.TimeOfDay) != 0)
                    //    {
                    //        rpItem.EndTime = newDt;
                    //        itemUpdated = true;
                    //    }
                    //}
                    if (itemUpdated)
                    {
                        currAppointment.Save();
                    }
                    

                }
                catch
                {
                }
            }
            catch
            {
            }
            finally
            {
                UtilityManager.FreeCOMObject(rpItem);
                UtilityManager.FreeCOMObject(currAppointment);
            }
            return itemUpdated; 
        }
        
       
       
    }
}
