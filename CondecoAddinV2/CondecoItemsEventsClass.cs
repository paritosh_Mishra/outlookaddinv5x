using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CondecoAddinV2.App_Resources;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CondecoAddinV2
{
    /// <summary>
    /// Add-in Express Outlook Items Events Class
    /// </summary>
    public class CondecoItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents
    {
        private AddinModule CurrentModule = null;
        static AppointmentHelper appHelper = new AppointmentHelper();
        static BookingHelper bookingHelper = new BookingHelper();
        private DateTime origStartDate = DateTime.MinValue;
        private DateTime origEndDate = DateTime.MaxValue;
        private string itemToMove = string.Empty;

        private string origSubject = string.Empty;

        public bool isAppointmentMoved = false;
        public CondecoItemsEventsClass(AddinExpress.MSO.ADXAddinModule module)
            : base(module)
        {
            if (CurrentModule == null)
                CurrentModule = module as AddinModule;
        }

        public override void ProcessItemAdd(object item)
        {
            UtilityManager.LogMessage("Calendar Item Add called");
        }
        //Added by Ravi Goyal for PRB0040856
        private bool GetCurrentView(object item)
        {
            UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView():*********Started********");
            bool viewrequired = false;
            try
            {
                object WindowType = this.CurrentModule.OutlookApp.ActiveWindow();
                if (WindowType is Outlook.Explorer)
                {
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Window type is Outlook explorer");
                    Outlook.View currentview = this.CurrentModule.OutlookApp.ActiveExplorer().CurrentView as Outlook.View;
                    if (currentview.Name == "Calendar")
                    {
                        UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: current view on explorer is calendar");
                        viewrequired = true;
                    }
                    UtilityManager.FreeCOMObject(currentview);
                }
                else if (WindowType is Outlook.Inspector)
                {
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Window type is Outlook Inspector");
                    if (item == null) viewrequired = false;
                    if (item is Outlook.AppointmentItem || item is Outlook.MeetingItem) viewrequired = true;
                    UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView: Inspector item is Appointment");
                }
                UtilityManager.FreeCOMObject(WindowType);
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("CondecoItemsEventsClass.GetCurrentView():*********Exception :-********"+ex.Message);
            }
            return viewrequired;
        }
        //End Added by Ravi Goyal for PRB0040856
        public override void ProcessItemChange(object item)
        {
            //Added by Ravi Goyal for PRB0040856
            bool Goahead= GetCurrentView(item);
            if (Goahead)
            {
                UtilityManager.LogMessage("**CondecoItemsEventsClass.ProcessItemChange:*Goahead method returned true**");
            }
            else
            {
                UtilityManager.LogMessage("**CondecoItemsEventsClass.ProcessItemChange:*Goahead method returned false**");
                return;
            }
            //End Added by Ravi Goyal for PRB0040856
            UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessItemChange:*********Started********");
            // UtilityManager.LogMessage("2A-Calendar Item changed called"+item.GetType().FullName);
            if (item == null) return;
            if (!(item is Outlook.AppointmentItem)) return;
            Outlook.AppointmentItem currAppointment = item as Outlook.AppointmentItem;
            if (currAppointment == null) return;
                try
                {
                    // if appointment is move on the calendar
                    string cPostID = appHelper.GetAppointmentPostID(currAppointment);
                    //  UtilityManager.LogMessage("2B- Item going to move for postid:" + cPostID);
                    if (string.IsNullOrEmpty(cPostID)) return;
                    if (isAppointmentMoved)
                    {
                        string currentID = itemToMove;
                        itemToMove = "";
                        //   UtilityManager.LogMessage("2C- Item moved called for postid:" + cPostID);
                        if (!bookingHelper.IsCondecoBooking(currAppointment) || !currAppointment.EntryID.Equals(currentID))
                        {
                            isAppointmentMoved = false;

                            //  UtilityManager.LogMessage("2CC- Item is not a condeco booking for postid:" + cPostID);
                            // UtilityManager.FreeCOMObject(currAppointment);
                            return;
                        }


                        // UtilityManager.LogMessage("2D- Item is going to moved in condeco  with Subject:" + currAppointment.Subject + "StartDate:" + currAppointment.Start + ":End Date:" + currAppointment.End);
                        this.HandleAppointmentMove(currAppointment);
                        //  UtilityManager.LogMessage("2E- Item moved in condeco  with Subject:" + currAppointment.Subject + "StartDate:" + currAppointment.Start + ":End Date:" + currAppointment.End);

                        //isCondecoBookingChanged = false;
                        // UtilityManager.FreeCOMObject(currAppointment);
                        isAppointmentMoved = false;
                        return;
                    }
                    // Try to delete single occurrence


                    if (currAppointment.IsRecurring && (currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting || currAppointment.MeetingStatus == Outlook.OlMeetingStatus.olMeeting))
                    {

                        if (string.IsNullOrEmpty(appHelper.GetAppointmentPostID(currAppointment))) return;
                        this.HandleOccurrenceDelete(currAppointment);


                    }
                }
                catch (Exception ex)
                {
                    UtilityManager.LogMessage("CondecoItemsEvents.ProcessItemChange Catch " + ex.Message);
                }
                finally
                {

                    // GC.Collect();
                    // GC.WaitForPendingFinalizers();
                    //  UtilityManager.FreeCOMObject(currAppointment);
                }
            UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessItemChange:*********Finished********");
        }

        public override void ProcessItemRemove()
        {

        }

        public override void ProcessBeforeFolderMove(object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeItemMove(object item, object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {

            try
            {
                if (item is Outlook.AppointmentItem)
                {
                    Outlook.AppointmentItem cItem = item as Outlook.AppointmentItem;

                    if (!bookingHelper.IsCondecoBooking(cItem))
                    {
                        UtilityManager.LogMessage("1BB- Item is not a condeco appointment");

                        return;
                    }
                    if (UtilityManager.ConnectionMode() == 2 && !UtilityManager.IsSSOAuthentication())
                    {
                        if (!UtilityManager.IsSSOTimeOutMsgShow)
                        {

                            UtilityManager.ShowErrorMessage(App_Resources.CondecoResources.User_Not_Authorized);
                        }
                        //UtilityManager.IsSSOTimeOutMsgShow = false;
                        e.Cancel = true;
                        return;
                    }
                    Outlook.MAPIFolder currFolder = null;
                    if (moveTo == null)
                    {
                        HandleDeleteConecoBooking(cItem);
                    }

                    if (moveTo is Outlook.MAPIFolder)
                    {
                        currFolder = moveTo as Outlook.MAPIFolder;

                        if (currFolder.Name.ToLower().StartsWith("calendar") || currFolder.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                        {

                            string currentPostID = appHelper.GetAppointmentPostID(cItem);
                            //&& cItem.MeetingStatus != Outlook.OlMeetingStatus.olMeeting
                            if (!appHelper.IsNewAppointment(cItem) && !String.IsNullOrEmpty(currentPostID))
                            {
                                bool dragnDropAllowed = false;
                                if ((AppointmentHelper.DragnDrop == 2) || ((AppointmentHelper.DragnDrop == 1) && !cItem.IsRecurring && (cItem.MeetingStatus == Outlook.OlMeetingStatus.olNonMeeting)))
                                {
                                    dragnDropAllowed = true; ;
                                }
                                if (AppointmentHelper.DragnDrop == 0 || !dragnDropAllowed)
                                {
                                    string ddMessage = string.Format(CondecoResources.Appointment_DragnDrop_Locked, Environment.NewLine);
                                    MessageBox.Show(ddMessage, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    e.Cancel = true;
                                }

                                isAppointmentMoved = true;
                                itemToMove = cItem.EntryID;
                                origSubject = cItem.Subject;
                                origStartDate = cItem.Start;
                                origEndDate = cItem.End;

                            }

                        }

                        if (currFolder.Name.ToLower().Equals("deleted items") || currFolder.DefaultItemType == Outlook.OlItemType.olMailItem)
                        {
                            HandleDeleteConecoBooking(cItem);

                            if (bookingHelper.IsVCWithin1hour)
                            {
                                e.Cancel = true;
                            }
                        }
                    }
                    // ProcessItemChange(cItem);
                    //UtilityManager.FreeCOMObject(currFolder);
                    //UtilityManager.FreeCOMObject(cItem);
                }
            }
            catch (System.Exception ex)
            {
                UtilityManager.LogMessage("ProcessBeforeItemMove " + ex.Message + " " + ex.StackTrace);
            }
            //  UtilityManager.LogMessage("CondecoItemsEventsClass.ProcessBeforeItemMove:*********Finished********");
        }
        private void HandleDeleteConecoBooking(Outlook.AppointmentItem cItem)
        {
            if (!appHelper.IsNewAppointment(cItem))
            {
                // Dont delete if its a dummy item deleted by microsoft live meeting,
                bool deleteCondecoBooking = true;
                bool wasItemOpened = (this.Module as AddinModule).isItemOpen;
                //cItem.MessageClass.ToLower().Equals("ipm.appointment.live meeting request") && !wasLiveMeeting
                Outlook.Inspector olInsp = null;
                string currentCaption = string.Empty;
                try
                {
                    olInsp = cItem.GetInspector;
                    if (olInsp != null)
                    {
                        currentCaption = olInsp.Caption;
                    }

                }
                catch (System.Exception ex)
                {
                    UtilityManager.LogMessage("ProcessBeforeItemMove GettingInspector" + ex.Message);
                }
                finally
                {
                    UtilityManager.FreeCOMObject(olInsp);
                }
                //  string itemCaption = cItem.Subject + " - Conferencing Request";
                if (wasItemOpened && cItem.MeetingStatus != Outlook.OlMeetingStatus.olMeetingCanceled && currentCaption.ToLower().Contains("conferencing request"))
                    deleteCondecoBooking = false;

                if (deleteCondecoBooking)
                    //Edited by Ravi Goyal-PRB0040146,PRB0040402,PRB0040402
                    bookingHelper.DeleteCondecoBooking(cItem, CurrentModule);
                CondecoMeeting curMeeting = bookingHelper.GetCurrentCondecoMeeting(cItem, true);
            }

        }
        private void HandleAppointmentMove(Outlook.AppointmentItem curApp)
        {

            // UtilityManager.LogMessage("2D1- Inside handle appointment move with Subject:" + curApp.Subject + "StartDate:" + curApp.Start + ":End Date:" + curApp.End);
            Outlook.AppointmentItem singleAppointment = null;
            // MessageBox.Show("Going to Move Appointment with Start:" + origStartDate + "to :" + curApp.Start);
            // UtilityManager.LogMessage("2D2- checking if appontment is recurring series");
            if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
            {
                Outlook.RecurrencePattern masterRecPattern = curApp.GetRecurrencePattern();
                Outlook.Exceptions oExps = masterRecPattern.Exceptions;
                if (oExps.Count > 0)
                {
                    for (int ex = 0; ex < oExps.Count; ex++)
                    {
                        Outlook.Exception currExp = oExps[ex + 1];
                        if (currExp != null)
                        {
                            if (!currExp.Deleted && currExp.OriginalDate.Date.CompareTo(origStartDate.Date) == 0)
                            {
                                // UtilityManager.LogMessage("2D2- single occurrent deleted exceptionfound for date:" + origStartDate.Date);
                                singleAppointment = currExp.AppointmentItem;
                                UtilityManager.FreeCOMObject(currExp);
                                break;
                            }
                            UtilityManager.FreeCOMObject(currExp);
                        }
                    }
                }
                UtilityManager.FreeCOMObject(oExps);
            }
            CondecoMeeting currMeeting = new CondecoMeeting();
            if (singleAppointment != null)
            {
                string postID = appHelper.GetAppointmentPostID(curApp);
                currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, origStartDate);
                curApp = singleAppointment;
                // UtilityManager.LogMessage("2D3A- Getting condeco appointment for deleted single occurrence meeting for date:" + origStartDate);
            }
            else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptOccurrence || curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptException)
            {
                string postID = appHelper.GetAppointmentPostID(curApp);
                currMeeting = bookingHelper.GetCondecoMeetingSingleOccurrence(postID, curApp, false, curApp.Start);

                //curApp = singleAppointment;
            }
            else if (curApp.RecurrenceState == Outlook.OlRecurrenceState.olApptNotRecurring)
            {
                currMeeting = bookingHelper.GetCurrentCondecoMeeting(curApp, false);

            }
            // UtilityManager.LogMessage("2D4- Boking Found in condeco:" + currMeeting.BookingID);
            if (currMeeting.BookingID == 0) return;
            int result = 0;
            // UtilityManager.LogMessage("2D5- checking for past booking:" + currMeeting.BookingID);
            // Below appItem.End check to fix the CRRI 6550 ,TP-19968 by Paritosh 
            long meetingTicks1 = currMeeting.DateFrom.Date.Ticks + currMeeting.TimeFrom.TimeOfDay.Ticks;
            DateTime meetingStartDateTime1 = new DateTime(meetingTicks1);


            DateTime condecoStartTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingStartDateTime1,
                                                                                                currMeeting.LocationID,
                                                                                                currMeeting.OriginalTZ);

            long meetingTicks = currMeeting.DateTo.Date.Ticks + currMeeting.TimeTo.TimeOfDay.Ticks;
            DateTime meetingEndDateTime = new DateTime(meetingTicks);




            DateTime condecoEndTimeInLocalTz = UtilityManager.ConvertDateToLocalTZ(meetingEndDateTime,
                                                                                                   currMeeting.LocationID,
                                                                                                   currMeeting.OriginalTZ);
            //24/jan/2004
            //if (meetingStartDateTime1.CompareTo(DateTime.Now) < 0 && meetingStartDateTime1.CompareTo(curApp.Start) != 0)
            //{
            //    result = -4;
            //}
            if (condecoStartTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && curApp.Start.CompareTo(condecoStartTimeInLocalTz) != 0 && (condecoStartTimeInLocalTz.CompareTo(DateTime.Now) < 0) && (curApp.Start.CompareTo(condecoStartTimeInLocalTz) < 0))
            {
                // MessageBox.Show("date change to past");
                result = -4;
            }
            else if (condecoEndTimeInLocalTz.CompareTo(DateTime.MinValue) != 0 && (condecoEndTimeInLocalTz.CompareTo(DateTime.Now) < 0))
            {
                result = -6;
            }

            else if (DateTime.Now.CompareTo(curApp.Start) >= 0 && DateTime.Now.CompareTo(curApp.End) > 0)
            {
                // UtilityManager.LogMessage("2D5A- booking is in past can not be modified:" + DateTime.Now);
                result = -200;
            }
            else if (curApp.AllDayEvent)
            {

                result = -400;
            }
            else
            {

                result = bookingHelper.UpdateCondecoBooking(currMeeting, curApp);

            }
            string message = CondecoResources.AppointmentMove_Services_NotAvailable;
            if (result == -1 || result == -999 || result == -100 || result == -200 || result == -300 || result == -400 ||
                result == -2 || result == -3 || result == -4 || result == -5 || result == -100 || result == -99 ||
                result == -97 || result == -81 || result == -82 || result == -83 || result == -84 || result == -85 || result == -86 || result == -6)
            {

                if (result == -1)
                    message = CondecoResources.AppointmentMove_Room_NotAvailable;
                else if (result == -81)
                    message = CondecoResources.Group_SingleDayBookingOnly;
                else if (result == -82)
                    message = CondecoResources.Group_BusinessHourOverLap;
                else if (result == -83)
                    message = CondecoResources.Group_AdvancedBookingPeriod;
                else if (result == -84)
                    message = CondecoResources.Group_NoticeRequired;
                else if (result == -97)
                    message = CondecoResources.Condeco_Update_Failed_VCBooking;
                else if (result == -99)
                    message = CondecoResources.Appointment_Cannot_Moved;
                else if (result == -200)
                    message = CondecoResources.Booking_Moved_Past;
                else if (result == -400)
                    message = CondecoResources.AllDayEvent_NotSupported;
                else if (result == -999) //added by Anand for permission verification
                    message = CondecoResources.Check_Booking_Permission;

                else if (result == -4)
                {
                    //  message = CondecoResources.BookingStartDateUneditable;
                    message = CondecoResources.Booking_Moved_Past;
                }
                else if (result == -85)
                {
                    //MessageBox.Show("Meeting in progress cant edit");
                    message = CondecoResources.BookingIsInProgress;
                }
                else if (result == -86)
                {
                    message = CondecoResources.AppointmentIsAlreadyClosed;
                }
                else if (result == -6 || result == -3)
                {
                    message = CondecoResources.Booking_Update_Past;
                }
                MessageBox.Show(message, CondecoResources.Condeco_Error_Caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //Changed by Anand on 2-Feb-2013 for TP10719 Since the origStartDate taken directly from  
                //appointment should not convert to local time zone as it already in local time
                //curApp.Start = UtilityManager.ConvertDateToLocalTZ(origStartDate, currMeeting.LocationID);
                //curApp.End = UtilityManager.ConvertDateToLocalTZ(origEndDate, currMeeting.LocationID); ;
                curApp.Start = origStartDate;
                curApp.End = origEndDate;
                //End change
                curApp.Save();


            }
            else if (result == 1)
            {
                //Outlook.AppointmentItem appItem = appHelper.GetCurrentAppointment(this.OutlookAppObj);
                UtilityManager.LogMessage("CondecoItemsEventsClass.cs- HandleAppointmentMove(Outlook.AppointmentItem curApp)  (result == 1)");
                string newLocSimple = bookingHelper.GetBookingLocation("", curApp);
                string newLoc = appHelper.FormatLocationInCondecoStyle(newLocSimple);
                if (curApp.Location == null)
                    curApp.Location = " ";
                if (!String.IsNullOrEmpty(newLoc) && !String.IsNullOrEmpty(curApp.Location))
                {
                    if (!curApp.Location.ToLower().Contains(newLoc.ToLower()))
                    {
                        appHelper.SetAppointmentLocation(curApp, newLocSimple);
                        // curApp.Location = newLoc;

                        curApp.Save();
                    }
                }
                //  message = CondecoResources.Appointment_Moved;

                //MessageBox.Show(message, "Condeco Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }



        private void HandleOccurrenceDelete(Outlook.AppointmentItem appItem)
        {
            UtilityManager.LogMessage("CondecoItemsEventsClass.HandleOccurrenceDelete:*********Started********");
            Outlook.RecurrencePattern rPattern = null;
            Outlook.Exceptions recExps = null;
            // Outlook.Exception mExp = null;
            Outlook.AppointmentItem newItem = null;
            Outlook.NameSpace ns = null;
            try
            {
                //Commented and changed By anand for TP10181 issue

                string currentId = appItem.EntryID;

                //Note: Commented by anand need to discuss with Waqas
                Marshal.ReleaseComObject(appItem);
                appItem = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();


                ns = CurrentModule.OutlookApp.Session;
                object currentItem = ns.GetItemFromID(currentId, null);
                newItem = currentItem as Outlook.AppointmentItem;
                rPattern = newItem.GetRecurrencePattern();
                recExps = rPattern.Exceptions;

                if (recExps.Count == 0)
                {
                    //Commented and changed By anand for TP10181 issue as they are already in finally

                    UtilityManager.FreeCOMObject(rPattern);
                    UtilityManager.FreeCOMObject(recExps);
                    UtilityManager.FreeCOMObject(newItem);
                    UtilityManager.FreeCOMObject(ns);
                    //  UtilityManager.FreeCOMObject(mExp);
                    return;
                }
                string postID = appHelper.GetAppointmentPostID(newItem);
                string getBookingData = "iID=" + postID + "&iSwitch=7";
                UtilityManager.LogMessage("6a-Getting user booking Data with parm: " + getBookingData);
                string bookingData = UtilityManager.PostDataToServer(getBookingData);
                //  for (int i = 0; i < recExps.Count; i++)
                foreach (Outlook.Exception curexp in recExps)
                {
                    //mExp = recExps.Item(i + 1);
                    // mExp = recExps[i + 1];
                    try
                    {
                        if (curexp.Deleted)
                        {

                            bookingHelper.DeleteSingleOccurrenceFromCondeco(newItem, curexp.OriginalDate, postID, bookingData);
                        }
                    }
                    catch (Exception ex)
                    {
                        UtilityManager.LogMessage("Going to Delete exp for" + curexp.OriginalDate + "::" + ex.Message + " " + ex.StackTrace);
                        bookingHelper.DeleteSingleOccurrenceFromCondeco(newItem, curexp.OriginalDate, postID, bookingData);
                    }
                    finally
                    {

                        // UtilityManager.FreeCOMObject(mExp);
                        Marshal.ReleaseComObject(curexp);
                    }
                }
            }
            catch (Exception ex)
            {
                UtilityManager.LogMessage("Error Occurred in Occurrence Delete" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                UtilityManager.FreeCOMObject(recExps);
                UtilityManager.FreeCOMObject(rPattern);
                UtilityManager.FreeCOMObject(newItem);
                UtilityManager.FreeCOMObject(ns);
                //UtilityManager.FreeCOMObject(mExp);
            }
            UtilityManager.LogMessage("CondecoItemsEventsClass.HandleOccurrenceDelete:*********Finished********");
        }


    }
}

